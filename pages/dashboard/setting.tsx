import { NextPage } from 'next'
import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicInput from '../../components/shared/BasicInput'
import DashboardLayout from '../../components/shared/DashboardLayout'
import LoadingScreen from '../../components/shared/LoadingScreen'
import { api } from '../../helper/api'
import { useCurrentMember } from '../../helper/useCurrentMember'

type Props = {}

const Setting: NextPage<Props> = (props: Props) => {
  const [show_modal, setShowModal] = useState(false)

  const { member } = useCurrentMember()

  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm()

  if (!member) {
    return <LoadingScreen />
  }

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan data...')
    try {
      const form_data: any = {}

      // for (const [key, value] of Object.entries(data)) {
      //   form_data.append(key, value)
      // }

      const { name, phone, address, email } = data

      form_data.name = name
      form_data.phone = phone
      form_data.address = address
      form_data.email = email

      await api.post('/api/members/update-my-profile', form_data)

      toast.success('Berhasil menyimpan data!')
      setTimeout(() => {
        window.location.reload()
      }, 2000)
    } catch (error) {
      console.log(error)
      toast.error('Error happened!')
    }
  })

  return (
    <>
      <DashboardLayout>
        <form onSubmit={onSubmit}>
          <div className="mb-10 rounded-lg border py-7 px-16">
            <h3 className="mb-7 font-semibold">Identitas Akun</h3>
            <div className="mb-2 grid grid-cols-4 items-center">
              <div className="col-span-1">
                <p className="mb-4">Nama Lengkap</p>
              </div>
              <div className="col-span-2">
                <BasicInput
                  error={errors.name}
                  field_name="name"
                  label=""
                  register={register}
                  default_value={member.name}
                />
              </div>
            </div>
            <div className="mb-2 grid grid-cols-4 items-center">
              <div className="col-span-1">
                <p className="mb-4">No HP Aktif</p>
              </div>
              <div className="col-span-2">
                <BasicInput
                  error={errors.phone}
                  field_name="phone"
                  label=""
                  register={register}
                  default_value={member.phone}
                />
              </div>
            </div>
            <div className="mb-2 grid grid-cols-4 items-center">
              <div className="col-span-1">
                <p className="mb-4">Alamat E-mail</p>
              </div>
              <div className="col-span-2">
                <BasicInput
                  error={errors.email}
                  field_name="email"
                  label=""
                  type="email"
                  register={register}
                  default_value={member.email}
                />
              </div>
            </div>
            <hr className="my-8" />
            <h3 className="mb-7 font-semibold">Data Pribadi</h3>
            <div className="mb-2 grid grid-cols-4 items-center">
              <div className="col-span-1">
                <p className="mb-4">Nama Bank</p>
              </div>
              <div className="col-span-2">
                <BasicInput
                  error={errors.bank_name}
                  field_name="bank_name"
                  label=""
                  register={register}
                  default_value={member.member_bank_account.bank_name}
                  disabled={true}
                />
              </div>
            </div>
            <div className="mb-2 grid grid-cols-4 items-center">
              <div className="col-span-1">
                <p className="mb-4">No Rekening</p>
              </div>
              <div className="col-span-2">
                <BasicInput
                  error={errors.bank_account_number}
                  field_name="bank_account_number"
                  label=""
                  register={register}
                  default_value={member.member_bank_account.bank_account_number}
                  disabled={true}
                />
              </div>
            </div>
            <div className="mb-2 grid grid-cols-4 items-center">
              <div className="col-span-1">
                <p className="mb-4">Nama Pemilik Rekening</p>
              </div>
              <div className="col-span-2">
                <BasicInput
                  error={errors.bank_account_name}
                  field_name="bank_account_name"
                  label=""
                  register={register}
                  default_value={member.member_bank_account.bank_account_name}
                  disabled={true}
                />
              </div>
            </div>
            <div className="mb-4 grid grid-cols-4 items-center">
              <div className="col-span-1"></div>
              <div className="col-span-2">
                <button
                  type="button"
                  className="ml-auto rounded-lg bg-jankos-pink-300 py-2 px-7 text-sm font-semibold text-white"
                  onClick={setShowModal.bind(this, true)}
                >
                  Ubah Data Rekening
                </button>
              </div>
            </div>
            <div className="mb-2 grid grid-cols-4 items-center">
              <div className="col-span-1">
                <p className="mb-4">Alamat Sesuai KTP</p>
              </div>
              <div className="col-span-2">
                <BasicInput
                  error={errors.address}
                  field_name="address"
                  label=""
                  register={register}
                  textarea={true}
                  default_value={member.address}
                />
              </div>
            </div>
            {/* <div className="mb-2 grid grid-cols-4 items-center">
              <div className="col-span-1">
                <p className="mb-4">Foto KTP</p>
              </div>
              <div className="col-span-2">
                <BasicInput
                  error={errors.image_id_card}
                  field_name="image_id_card"
                  label=""
                  register={register}
                  type="file"
                  validation={{ required: false }}
                />
              </div>
            </div> */}
          </div>
          <div className="text-right">
            <button
              type="submit"
              className="ml-auto rounded-lg bg-jankos-pink-300 py-2 px-7 text-sm font-semibold text-white"
            >
              Simpan Perubahan
            </button>
          </div>
        </form>
      </DashboardLayout>
      {show_modal && (
        <div className="fixed top-0 left-0 z-10 flex h-screen w-screen items-center justify-center">
          <div
            className="absolute top-0 left-0 h-full w-full cursor-pointer bg-black bg-opacity-50"
            onClick={setShowModal.bind(this, false)}
          />
          <div className="z-10 block h-auto w-1/2 rounded bg-white p-12">
            <p className="mx-auto mb-6 w-2/3 text-center text-sm">
              Untuk mengganti No. Rekening Anda, harap menghubungi Call Center
              Jankos Glow di nomor:
            </p>
            <p className="text-center text-xl font-bold text-jankos-pink-300">
              (021) - 8761234
            </p>
          </div>
        </div>
      )}
    </>
  )
}

export default Setting
