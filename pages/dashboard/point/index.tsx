import { NextPage } from 'next'
import React, { useEffect, useState } from 'react'
import DashboardLayout from '../../../components/shared/DashboardLayout'
import { useCurrentMember } from '../../../helper/useCurrentMember'
import DataTable from 'react-data-table-component'
import { api } from '../../../helper/api'
import Select from 'react-select/'
import { toast } from 'react-toastify'
import LoadingScreen from '../../../components/shared/LoadingScreen'
import { format } from 'date-fns'
import Image from 'next/image'

type Props = {}

const Points: NextPage<Props> = (props: Props) => {
  const { member } = useCurrentMember()

  const [selected_product, setSelectedProduct] = useState<any>(null)
  const [selected_address, setSelectedAddress] = useState<any>(null)
  const [products, setProducts] = useState<any[]>([])
  const [addresses, setAddresses] = useState<any[]>([])
  const [current_point, setCurrentPoint] = useState(0)

  useEffect(() => {
    const fetchProducts = async () => {
      const {
        data: { data },
      } = await api.get(`/api/products`)
      setProducts(data)
    }
    fetchProducts()
  }, [])

  useEffect(() => {
    if (member) {
      setCurrentPoint(
        member.member_point_ledgers
          .map((ledger: any) => ledger.amount)
          .reduce(
            (current_total_amount: number, current_amount: number) =>
              current_total_amount + current_amount,
            0
          )
      )
    }
  }, [member, setCurrentPoint])

  useEffect(() => {
    const fetchAddress = async () => {
      if (member) {
        const { data } = await api.get('/api/flow/my-address')
        setAddresses(data)
      }
    }
    fetchAddress()
  }, [member])

  if (!member) {
    return <LoadingScreen />
  }

  const onRedeem = async () => {
    toast.info('Redeeming...')
    if (!selected_product || !selected_address) {
      toast.error('Request Invalid!')
    }

    try {
      const response = await api.post('/api/flow/redeem-point', {
        product_id: selected_product.id,
        shipping_address_id: selected_address.id,
      })
      console.log(response.data)
      toast.success('Redeem Success!')
      setTimeout(() => {
        window.location.reload()
      }, 3000)
    } catch (error: any) {
      toast.error('Error!')
      console.log(error.response)
    }
  }

  return (
    <>
      <DashboardLayout>
        <div className="mb-9 w-min rounded-lg border bg-white py-5 pl-7 pr-20 shadow-md">
          <p className="mb-2 whitespace-nowrap text-sm font-medium">
            Poin Anda
          </p>
          <div className="mb-3 flex items-center">
            <div className="mr-4 flex h-12 w-12 items-center justify-center rounded-full border-4 border-jankos-pink-300 text-jankos-pink-300">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth={2}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M12 8v13m0-13V6a2 2 0 112 2h-2zm0 0V5.5A2.5 2.5 0 109.5 8H12zm-7 4h14M5 12a2 2 0 110-4h14a2 2 0 110 4M5 12v7a2 2 0 002 2h10a2 2 0 002-2v-7"
                />
              </svg>
            </div>
            <p className="pt-1 text-4xl font-bold text-jankos-pink-300">
              {current_point.toLocaleString()}
            </p>
          </div>
          <p className="whitespace-nowrap text-sm font-medium">
            Berlaku hingga{' '}
            <span className="font-semibold text-jankos-pink-300">
              12 Maret 2023
            </span>
          </p>
          {/* <p className="mt-4 text-xs">Kode anda : {member.referral_code}</p> */}
        </div>
        {member.member_point_ledgers.length > 0 && (
          <div className="rounded-lg shadow-md">
            <DataTable
              data={member.member_point_ledgers}
              columns={[
                {
                  name: 'Tanggal',
                  selector: (row: any) => row.createdAt,
                  format: (row: any) =>
                    format(new Date(row.createdAt), 'yyyy-MM-dd'),
                },
                {
                  name: 'Jumlah',
                  selector: (row: any) => row.amount,
                  format: (row: any) => row.amount.toLocaleString(),
                },
              ]}
            />
          </div>
        )}
        <div className="mb-9"></div>
        <p className="mb-4 font-semibold">
          Tukarkan poin yang kamu miliki dengan hadiah dibawah ini!
        </p>
        <div className="grid grid-cols-2 gap-8 lg:grid-cols-4">
          {products.map((product) => (
            <div className="hover:bg-slateborder-slate-200 group relative h-auto w-full transform rounded border border-slate-200 bg-white shadow-md transition hover:-translate-y-1 hover:border-jankos-pink-300 hover:shadow-lg">
              <div className="relative h-72 w-full border-b">
                <Image
                  src={product.thumbnail}
                  layout="fill"
                  objectFit="cover"
                  objectPosition={'center center'}
                />
              </div>
              {/* <img
                src={product.thumbnail}
                className="h-auto w-full rounded-t border-b"
                alt=""
              /> */}
              <div className="flex flex-col justify-between p-4">
                <div className="">
                  <p className="mb-2 text-sm font-semibold">{product.name}</p>
                  <p className="mb-1.5 text-sm font-semibold text-jankos-pink-300">
                    {product.price.toLocaleString()} Poin
                  </p>
                </div>
                <button
                  className={`w-full rounded py-1.5 text-xs font-semibold text-white ${
                    current_point >= product.price
                      ? 'bg-jankos-pink-300'
                      : 'bg-slate-400'
                  }`}
                  onClick={setSelectedProduct.bind(this, product)}
                  disabled={current_point < product.price}
                >
                  Tukarkan Poin
                </button>
              </div>
            </div>
          ))}
        </div>
      </DashboardLayout>
      {selected_product && (
        <div className="fixed top-0 left-0 z-10 flex h-screen w-screen items-center justify-center">
          <div
            className="absolute top-0 left-0 h-full w-full cursor-pointer bg-black bg-opacity-50"
            onClick={setSelectedProduct.bind(this, null)}
          />
          <div className="z-10 block h-1/2 w-1/2 rounded bg-white p-9">
            <p className="mb-9 text-center text-xl font-bold">Pilih Alamat</p>
            <Select
              options={addresses.map((address: any) => {
                return {
                  label: `${address.contact_name}, ${address.contact_address}, ${address.subdistrict_name}, ${address.city_name}, ${address.postal_code}`,
                  value: address,
                }
              })}
              onChange={({ value }: any) => setSelectedAddress(value)}
            />
            <div className="text-center">
              <button
                className={`mx-auto mt-6 w-min rounded bg-jankos-pink-300 py-1.5 px-6 text-xs font-semibold text-white`}
                onClick={onRedeem}
              >
                Redeem
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  )
}

export default Points
