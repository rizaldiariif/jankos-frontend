import { NextPage } from 'next'
import React, { useEffect, useState } from 'react'
import DashboardLayout from '../../../components/shared/DashboardLayout'
import Link from 'next/link'
import { useCurrentMember } from '../../../helper/useCurrentMember'
import { fetcher } from '../../../helper/fetcher'
import LoadingScreen from '../../../components/shared/LoadingScreen'
import Image from 'next/image'

type Props = {}

const Products: NextPage<Props> = (props: Props) => {
  const { member } = useCurrentMember()

  const [products, setProducts] = useState<any[]>([])

  useEffect(() => {
    const fetchProducts = async () => {
      const { data } = await fetcher('/api/products')
      setProducts(data)
    }
    fetchProducts()
  }, [])

  if (!member) {
    return <LoadingScreen />
  }

  return (
    <DashboardLayout>
      <div className="grid grid-cols-2 gap-8 lg:grid-cols-4">
        {products.map((product) => (
          <Link href={`/dashboard/product/${product.id}`}>
            <a className="hover:bg-slateborder-slate-200 group relative h-auto w-full transform rounded border border-slate-200 bg-white shadow-md transition hover:-translate-y-1 hover:border-jankos-pink-300 hover:shadow-lg">
              <div className="relative h-72 w-full border-b">
                <Image
                  src={product.thumbnail}
                  layout="fill"
                  objectFit="cover"
                  objectPosition={'center center'}
                />
              </div>
              <div className="p-4">
                <p className="mb-1.5 text-sm font-medium">{product.name}</p>
                <p className="mb-1 text-xs">Harga {member.member_level.name}</p>
                <p className="mb-1.5 text-sm font-semibold text-jankos-green-200">
                  Rp
                  {product.product_level_prices
                    .find(
                      (price: any) =>
                        price.member_level_id === member.member_level_id
                    )
                    .price.toLocaleString()}
                </p>
              </div>
            </a>
          </Link>
        ))}
      </div>
    </DashboardLayout>
  )
}

export default Products
