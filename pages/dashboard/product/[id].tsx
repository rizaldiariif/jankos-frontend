import { NextPage } from 'next'
import React, { useEffect, useState } from 'react'
import DashboardLayout from '../../../components/shared/DashboardLayout'
import Link from 'next/link'
import { useCurrentMember } from '../../../helper/useCurrentMember'
import { fetcher } from '../../../helper/fetcher'
import { useRouter } from 'next/router'
import { useCart } from '../../../helper/useCart'
import { toast } from 'react-toastify'
import LoadingScreen from '../../../components/shared/LoadingScreen'
import { useForm } from 'react-hook-form'
import Image from 'next/image'
import { Splide, SplideSlide } from '@splidejs/react-splide'

type Props = {}

const ProductDetail: NextPage<Props> = (props: Props) => {
  const { member } = useCurrentMember()
  const { addItem, cart } = useCart()
  const {
    query: { id },
  } = useRouter()
  const [active_image, setActiveImage] = useState<string | null>('')

  const { register, setValue, getValues } = useForm()

  const [product, setProduct] = useState<any>(null)
  const [products, setProducts] = useState<any[]>([])

  useEffect(() => {
    const fetchProduct = async () => {
      const { data } = await fetcher(`/api/products`)
      const selected_product = data.find((d: any) => d.id === id)
      setProduct(selected_product)
      setProducts(data)
    }
    fetchProduct()
  }, [id])

  if (!product || !member) {
    return <LoadingScreen />
  }

  return (
    <DashboardLayout>
      <div className="mb-10 flex items-center justify-start">
        <p className="text-sm">Beranda</p>
        <p className="ml-5 text-sm">/</p>
        <p className="ml-5 text-sm">Produk</p>
      </div>
      <div className="mb-14 grid grid-cols-3 gap-8">
        <div className="relative mb-16 h-auto w-full">
          <div className="relative mb-4 h-96 w-full">
            <img
              src={active_image || product?.thumbnail}
              className="mb-4 h-full w-full object-cover object-center"
              alt=""
            />
          </div>
          <Splide
            aria-label="My Favorite Images"
            options={{
              // type: 'loop',
              height: '64px',
              autoHeight: true,
              pagination: false,
              rewind: false,
              perPage: 4,
              gap: '1rem',
              perMove: 1,
            }}
            onActive={(_, slide) => {
              setActiveImage(slide.slide.ariaAtomic)
            }}
          >
            <SplideSlide
              aria-atomic={product?.thumbnail}
              onClick={() => {
                setActiveImage(product?.thumbnail)
              }}
            >
              <img
                src={product?.thumbnail}
                alt="Image 1"
                style={{ height: '64px', width: '64px' }}
              />
            </SplideSlide>
            {product?.product_images.map((image: any) => (
              <SplideSlide
                aria-atomic={image.url}
                onClick={() => {
                  setActiveImage(image.url)
                }}
              >
                <img
                  src={image.url}
                  alt={image.name}
                  style={{ height: '64px', width: '64px' }}
                />
              </SplideSlide>
            ))}
          </Splide>
        </div>
        <div className="col-span-2">
          <p className="mb-1.5 text-xl font-bold">{product.name}</p>
          <p className="mb-1 text-xs">Harga Jual</p>
          <p className="mb-1.5 text-xs font-semibold">
            Rp{product.price.toLocaleString()}
          </p>
          <p className="mb-1 text-xs">Harga {member.member_level.name}</p>
          <p className="mb-6 text-sm font-semibold text-jankos-green-200">
            Rp
            {product.product_level_prices
              .find(
                (price: any) => price.member_level_id === member.member_level_id
              )
              .price.toLocaleString()}
          </p>
          <p className="mb-1 text-sm font-medium">Quantity</p>
          <div className="mb-4 flex h-10 items-center justify-start">
            <button
              className="flex h-full items-center justify-center rounded-l border border-slate-300 bg-white px-6 text-xl font-semibold transition-colors active:bg-slate-200"
              onClick={() => {
                if (getValues('quantity') > 0) {
                  setValue('quantity', getValues('quantity') - 1)
                }
              }}
            >
              -
            </button>
            <input
              type="number"
              className="flex h-full w-24 items-center justify-center border border-slate-300 bg-white text-center font-semibold"
              defaultValue={0}
              min={1}
              max={10000}
              {...register('quantity')}
              onChange={(e) => {
                if (
                  parseInt(e.target.value) > 0 &&
                  parseInt(e.target.value) < 10000
                ) {
                  setValue('quantity', parseInt(e.target.value))
                }

                if (parseInt(e.target.value) < 1) {
                  setValue('quantity', 1)
                }

                if (parseInt(e.target.value) >= 10000) {
                  setValue('quantity', 10000)
                }
              }}
            />
            <button
              className="flex h-full items-center justify-center rounded-r border border-slate-300 bg-white px-6 text-xl font-semibold transition-colors active:bg-slate-200"
              onClick={() => {
                if (parseInt(getValues('quantity')) < 10000) {
                  setValue('quantity', parseInt(getValues('quantity')) + 1)
                }
              }}
            >
              +
            </button>
          </div>
          <button
            className="flex items-center justify-start rounded bg-jankos-pink-300 py-2 px-4 font-semibold text-white"
            onClick={() => {
              addItem({
                product_id: product.id,
                quantity: getValues('quantity'),
              })
              toast.success('Added to cart!')
              setTimeout(() => {
                window.location.href = '/dashboard/product'
              }, 2000)
            }}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="mr-3 h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
              />
            </svg>
            Tambah ke Keranjang
          </button>
        </div>
      </div>
      <div className="mb-16 w-full">
        <p className="mb-5 font-semibold">Deskripsi Produk</p>
        <p className="mb-2 text-sm font-bold">Berat: {product.weight}gr</p>
        <div
          className="mb-8 w-2/3 text-sm font-medium"
          dangerouslySetInnerHTML={{ __html: product.description }}
        />
        {/* <p className="">{product.description}</p> */}
        {/* <p className="mb-5 font-semibold">Cocok digunakan untuk</p> */}
        {/* <div className="grid grid-cols-4">
          {product.suitables.map((suitable: any) => (
            <div className="flex flex-col items-center justify-center text-center">
              <div className="mb-4 flex h-20 w-20 items-center justify-center rounded bg-jankos-pink-100">
                <img src={suitable.icon} alt="" className="h-12 w-auto" />
              </div>
              <p className="text-sm font-medium">{suitable.label}</p>
            </div>
          ))}
        </div> */}
      </div>
      <div className="w-full">
        <p className="mb-4 font-semibold">Produk Favorit Semua Orang</p>
        <div className="grid grid-cols-2 gap-8 lg:grid-cols-4">
          {products.map((product) => (
            <Link href={`/dashboard/product/${product.id}`}>
              <a className="group relative h-auto w-full transform rounded border border-slate-200 bg-white shadow-md transition hover:-translate-y-1 hover:border-jankos-pink-300 hover:bg-jankos-pink-100 hover:shadow-lg">
                <div className="relative h-72 w-full border-b">
                  <Image
                    src={product.thumbnail}
                    layout="fill"
                    objectFit="cover"
                    objectPosition={'center center'}
                  />
                </div>
                <div className="p-4">
                  <p className="mb-1.5 text-sm font-medium">{product.name}</p>
                  <p className="mb-1 text-xs">
                    Harga {member.member_level.name}
                  </p>
                  <p className="mb-1.5 text-sm font-semibold text-jankos-green-200">
                    Rp
                    {product.product_level_prices
                      .find(
                        (price: any) =>
                          price.member_level_id === member.member_level_id
                      )
                      .price.toLocaleString()}
                  </p>
                </div>
              </a>
            </Link>
          ))}
        </div>
      </div>
    </DashboardLayout>
  )
}

export default ProductDetail
