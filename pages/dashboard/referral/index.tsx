import { NextPage } from 'next'
import DashboardLayout from '../../../components/shared/DashboardLayout'
import { useCurrentMember } from '../../../helper/useCurrentMember'
import DataTable from 'react-data-table-component'
import LoadingScreen from '../../../components/shared/LoadingScreen'
import { toast } from 'react-toastify'
import { format } from 'date-fns'

type Props = {}

const Referrals: NextPage<Props> = (props: Props) => {
  const { member } = useCurrentMember()

  if (!member) {
    return <LoadingScreen />
  }

  const onCopyCode = () => {
    navigator.clipboard.writeText(member.referral_code)
    toast.success('Kode referral berhasil disalin!')
  }

  return (
    <DashboardLayout>
      <div className="mb-14 flex items-center justify-between">
        <div className="flex">
          <div className="mr-6 rounded-lg border bg-white py-4 px-6 shadow-md">
            <p className="mb-3 text-sm font-medium">Referral Aktif</p>
            <p className="text-4xl font-bold text-jankos-pink-300">
              {
                member.member_referrals.filter(
                  (mr: any) =>
                    member.member_level.hierarchy_number >=
                    mr.referred_member.member_level.hierarchy_number
                ).length
              }
            </p>
          </div>
          <div className="rounded-lg border bg-white py-4 px-6 shadow-md">
            <p className="mb-3 text-sm font-medium">Total Referral</p>
            <p className="text-4xl font-bold text-jankos-pink-300">
              {member.member_referrals.length}
            </p>
          </div>
        </div>
        <div className="">
          <p className="mb-4 text-sm font-medium">Kode Referral Kamu</p>
          <div className="flex items-center">
            <p className="mr-2 font-semibold">{member.referral_code}</p>
            <button
              className="rounded bg-jankos-pink-100 py-1 px-2 text-sm font-semibold text-jankos-pink-300"
              onClick={onCopyCode}
            >
              Salin Kode
            </button>
          </div>
        </div>
      </div>
      <p className="mb-6 font-semibold">Daftar Referral</p>
      <DataTable
        data={member.member_referrals}
        columns={[
          {
            name: 'No',
            selector: (_row, index) => (index ? index + 1 : 1),
          },
          {
            name: 'Nama',
            selector: (row: any) => row.referred_member.name,
          },
          {
            name: 'Member Level',
            selector: (row: any) => row.referred_member.member_level.name,
          },
          {
            name: 'Tanggal Bergabung',
            selector: (row: any) => row.createdAt,
            format: (row: any) => format(new Date(row.createdAt), 'dd-MM-yyyy'),
          },
          {
            name: 'Status',
            selector: (row: any) =>
              member.member_level.hierarchy_number >=
              row.referred_member.member_level.hierarchy_number
                ? 'Aktif'
                : 'Non-Aktif',
          },
        ]}
      />
    </DashboardLayout>
  )
}

export default Referrals
