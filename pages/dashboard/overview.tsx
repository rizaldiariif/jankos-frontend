import { NextPage } from 'next'
import React, { useEffect, useState } from 'react'
import DashboardLayout from '../../components/shared/DashboardLayout'
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js'
import { Line } from 'react-chartjs-2'
import { api } from '../../helper/api'
import { useCurrentMember } from '../../helper/useCurrentMember'

type Props = {}

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
)

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' as const,
    },
    title: {
      display: true,
      text: 'Transaksi Member',
    },
  },
  scales: {
    y: {
      min: 0,
      max: 1000,
      stepSize: 100,
    },
  },
}

const Overview: NextPage<Props> = (props: Props) => {
  const { member } = useCurrentMember()

  const [transaction_monthly, setTransactionMonthly] = useState([])
  // const [current_point, setCurrentPoint] = useState(0)
  const [year, setYear] = useState(new Date().getFullYear())

  // useEffect(() => {
  //   if (member) {
  //     setCurrentPoint(
  //       member.member_point_ledgers
  //         .map((ledger: any) => ledger.amount)
  //         .reduce(
  //           (current_total_amount: number, current_amount: number) =>
  //             current_total_amount + current_amount,
  //           0
  //         )
  //     )
  //   }
  // }, [member, setCurrentPoint])

  const labels = [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember',
  ]

  const data = {
    labels,
    datasets: [
      {
        label: 'Jumlah Transaksi',
        data: transaction_monthly,
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
    ],
  }

  useEffect(() => {
    const fetchMyTransactions = async () => {
      const { data } = await api.get(
        `/api/flow/my-transaction-monthly?year=${year}`
      )
      setTransactionMonthly(data)
    }
    fetchMyTransactions()
  }, [year])

  return (
    <DashboardLayout>
      {/* <div className="mb-9 w-min rounded-lg border bg-white py-5 pl-7 pr-20 shadow-md">
        <p className="mb-2 whitespace-nowrap text-sm font-medium">Poin Anda</p>
        <div className="mb-3 flex items-center">
          <div className="mr-4 flex h-12 w-12 items-center justify-center rounded-full border-4 border-jankos-pink-300 text-jankos-pink-300">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M12 8v13m0-13V6a2 2 0 112 2h-2zm0 0V5.5A2.5 2.5 0 109.5 8H12zm-7 4h14M5 12a2 2 0 110-4h14a2 2 0 110 4M5 12v7a2 2 0 002 2h10a2 2 0 002-2v-7"
              />
            </svg>
          </div>
          <p className="pt-1 text-4xl font-bold text-jankos-pink-300">
            {current_point.toLocaleString()}
          </p>
        </div>
        <p className="whitespace-nowrap text-sm font-medium">
          Berlaku hingga{' '}
          <span className="font-semibold text-jankos-pink-300">
            12 Maret 2023
          </span>
        </p>
      </div> */}
      <div className="rounded bg-white p-6 shadow-lg">
        <div className="text-right">
          <select
            onChange={(e) => setYear(parseInt(e.target.value))}
            className="rounded border bg-jankos-pink-100 py-2 px-6 text-xs font-semibold"
          >
            <option value={2020} selected={year === 2020}>
              2020
            </option>
            <option value={2021} selected={year === 2021}>
              2021
            </option>
            <option value={2022} selected={year === 2022}>
              2022
            </option>
            <option value={2023} selected={year === 2023}>
              2023
            </option>
            <option value={2024} selected={year === 2024}>
              2024
            </option>
            <option value={2025} selected={year === 2025}>
              2025
            </option>
          </select>
        </div>
        <Line options={options} data={data} />
        <p className="mt-4 text-center text-xs font-medium">{year}</p>
      </div>
    </DashboardLayout>
  )
}

export default Overview
