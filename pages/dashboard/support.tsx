import { NextPage } from 'next'
import React from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicInput from '../../components/shared/BasicInput'
import DashboardLayout from '../../components/shared/DashboardLayout'
import { api } from '../../helper/api'

type Props = {}

const Support: NextPage<Props> = (props: Props) => {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan pesan...')
    try {
      const { subject, body } = data

      const form_data = {
        subject,
        body,
      }

      await api.post('/api/flow/support-email', form_data)

      toast.success('Berhasil mengirim pesan!')
      setTimeout(() => {
        window.location.reload()
      }, 2000)
    } catch (error) {
      console.log(error)
      toast.error('Error happened!')
    }
  })

  return (
    <DashboardLayout>
      <form onSubmit={onSubmit}>
        <div className="mb-10 rounded-lg border py-7 px-16">
          <h3 className="mb-4 font-semibold">Bantuan</h3>
          <p className="mb-7 text-sm">
            Anda mengalami kendala mengenai Jankos Glow? Silahkan kirim pesan
            kepada kami. Kami akan membantu Anda dengan senang hati!
          </p>
          <div className="mb-2 grid grid-cols-4 items-center">
            <div className="col-span-1">
              <p className="mb-4">Subjek</p>
            </div>
            <div className="col-span-2">
              <BasicInput
                error={errors.subject}
                field_name="subject"
                label=""
                placeholder="Bantuan apa yang Anda butuhkan saat ini?"
                register={register}
              />
            </div>
          </div>
          <div className="mb-2 grid grid-cols-4 items-start">
            <div className="col-span-1">
              <p className="mb-4">Pesan</p>
            </div>
            <div className="col-span-2">
              <BasicInput
                error={errors.body}
                field_name="body"
                label=""
                placeholder="Jelaskan pada kami detail dari permasalahan Anda saat ini!"
                register={register}
                textarea={true}
              />
            </div>
          </div>
        </div>
        <div className="text-right">
          <button
            type="submit"
            className="ml-auto rounded-lg bg-jankos-pink-300 py-2 px-7 text-sm font-semibold text-white"
          >
            Kirim Pesan
          </button>
        </div>
      </form>
    </DashboardLayout>
  )
}

export default Support
