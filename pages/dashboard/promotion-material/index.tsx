import { format } from 'date-fns'
import { NextPage } from 'next'
import React, { useEffect, useState } from 'react'
import ReactDatePicker from 'react-datepicker'
import { toast } from 'react-toastify'
import DashboardLayout from '../../../components/shared/DashboardLayout'
import { downloader } from '../../../helper/downloader'
import { fetcher } from '../../../helper/fetcher'

type Props = {}

const PromotionMaterial: NextPage<Props> = (props: Props) => {
  const [promotion_materials, setPromotionMaterials] = useState<any[]>([])
  const [code, setCode] = useState('promotion')
  const [keyword, setKeyword] = useState('')
  const [codes] = useState([
    {
      label: 'Gambar Promosi',
      value: 'promotion',
    },
    {
      label: 'Gambar Produk',
      value: 'product',
    },
  ])
  const [start_date, setStartDate] = useState(
    new Date(
      new Date().getFullYear(),
      new Date().getMonth() - 1,
      new Date().getDate()
    )
  )
  const [end_date, setEndDate] = useState(new Date())

  const onChangeDate = (dates: any) => {
    const [start, end] = dates
    setStartDate(start)
    setEndDate(end)
  }

  useEffect(() => {
    const fetchMarketingMaterial = async () => {
      let url = '/api/marketing-materials?page=1'
      if (keyword != '') {
        url += `&title.iLike=%${keyword}%`
      }
      if (start_date) {
        url += `&suggestion_date.gte=${start_date}`
      }
      if (end_date) {
        url += `&suggestion_date.lte=${end_date}`
      }
      const { data } = await fetcher(url)
      setPromotionMaterials(data)
    }
    fetchMarketingMaterial()
  }, [code, keyword, start_date, end_date])

  return (
    <DashboardLayout>
      {/* <div className="rounded bg-white p-6 shadow-lg">My Orders</div> */}
      <div className="mb-9 flex w-full items-center justify-between">
        <input
          type="text"
          className="w-72 rounded border-slate-300 py-3 px-4 text-xs shadow-md"
          placeholder="Cari berdasarkan kata kunci"
          onChange={(e) => setKeyword(e.target.value)}
          value={keyword}
        />
        <div className="w-52">
          <ReactDatePicker
            selected={start_date}
            onChange={onChangeDate}
            startDate={start_date}
            endDate={end_date}
            selectsRange
            className="w-full rounded-lg border-slate-300 py-3 px-4 text-xs shadow-md"
            dateFormatCalendar="dd/MM/yyyy"
            // inline
          />
        </div>
        {/* <input
          type="text"
          className="w-72 rounded border-slate-300 py-3 px-4 text-xs shadow-md"
          placeholder="Pilih tanggal transaksi"
        /> */}
      </div>
      <div className="mb-12 grid grid-cols-2 gap-6 rounded-lg bg-white shadow-md">
        {codes.map((c) => (
          <span
            id={c.value}
            onClick={() => {
              setCode(c.value)
              setKeyword('')
              setStartDate(
                new Date(
                  new Date().getFullYear(),
                  new Date().getMonth() - 1,
                  new Date().getDate()
                )
              )
              setEndDate(new Date())
            }}
            className={`mx-auto w-min whitespace-nowrap p-3 text-sm transition-colors ${
              c.value === code
                ? 'border-b-4 border-b-jankos-pink-300 font-semibold text-black'
                : 'cursor-pointer font-medium text-slate-600'
            }`}
          >
            {c.label}
          </span>
        ))}
      </div>
      {promotion_materials
        .filter((promotion_material) => promotion_material.code === code)
        .map((promotion_material) => (
          <div className="mb-5 grid gap-y-4 rounded-lg border border-jankos-pink-100 bg-white p-5 shadow-md lg:grid-cols-5">
            <img
              src={promotion_material.thumbnail}
              alt=""
              className="h-32 w-auto rounded border"
            />
            <div className="col-span-3">
              <p className="mb-2 font-semibold">{promotion_material.title}</p>
              <p className="mb-8 text-sm">{promotion_material.text}</p>
              <p className="text-sm font-semibold">
                Saran tanggal post:{' '}
                {format(
                  new Date(promotion_material.suggestion_date),
                  'dd-MM-yyyy'
                )}
              </p>
            </div>
            <div className="flex items-center justify-center">
              <div className="mr-10">
                <button
                  className="mb-2 flex h-12 w-12 items-center justify-center rounded-full bg-jankos-pink-100 text-jankos-pink-300"
                  onClick={() => {
                    navigator.clipboard.writeText(promotion_material.text)
                    toast.success('Text copied!')
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M8 16H6a2 2 0 01-2-2V6a2 2 0 012-2h8a2 2 0 012 2v2m-6 12h8a2 2 0 002-2v-8a2 2 0 00-2-2h-8a2 2 0 00-2 2v8a2 2 0 002 2z"
                    />
                  </svg>
                </button>
                <p className="mx-auto w-min text-center text-xs font-semibold">
                  Salin Teks
                </p>
              </div>
              <div className="mr-10">
                <div
                  className="mb-2 flex h-12 w-12 cursor-pointer items-center justify-center rounded-full bg-jankos-pink-100 text-jankos-pink-300"
                  // href={promotion_material.thumbnail}
                  // download
                  // target={'_blank'}
                  onClick={() =>
                    downloader(
                      promotion_material.thumbnail,
                      promotion_material.title
                    )
                  }
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"
                    />
                  </svg>
                </div>
                <p className="mx-auto w-min text-center text-xs font-semibold">
                  Unduh Gambar
                </p>
              </div>
            </div>
          </div>
        ))}
    </DashboardLayout>
  )
}

export default PromotionMaterial
