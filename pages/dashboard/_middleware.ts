import { NextFetchEvent, NextRequest, NextResponse } from 'next/server'

export async function middleware(req: NextRequest, ev: NextFetchEvent) {
  if (!process.env.NEXT_PUBLIC_APP_COOKIE_NAME) {
    throw new Error('ENV VARIABLE FOR COOKIE NAME IS NOT SET!')
  }

  const jwt = req.cookies[process.env.NEXT_PUBLIC_APP_COOKIE_NAME]

  if (typeof jwt !== 'string') {
    const url = req.nextUrl.clone()
    url.pathname = '/auth/signin'
    return NextResponse.redirect(url)
  }

  const response = NextResponse.next()

  response.cookie(process.env.NEXT_PUBLIC_APP_COOKIE_NAME, jwt)

  return response
}
