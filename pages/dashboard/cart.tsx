import { NextPage } from 'next'
import React, { useEffect, useMemo, useState } from 'react'
import DashboardLayout from '../../components/shared/DashboardLayout'
import { useCurrentMember } from '../../helper/useCurrentMember'
import Select from 'react-select/'
import { useCart } from '../../helper/useCart'
import { fetcher } from '../../helper/fetcher'
import { useThrottle } from 'react-use'
import { useForm } from 'react-hook-form'
import { api } from '../../helper/api'
import { toast } from 'react-toastify'
import LoadingScreen from '../../components/shared/LoadingScreen'
import { useRouter } from 'next/router'

type Props = {}

const Cart: NextPage<Props> = (props: Props) => {
  const { member } = useCurrentMember()
  const { cart, updateItem, deleteItem } = useCart()
  const { reload } = useRouter()

  const { register, watch, setValue, getValues } = useForm()

  const [products, setProducts] = useState<any[]>([])
  const [product_options, setProductOptions] = useState<any[]>([])
  const [member_shipping_address, setMemberShippingAddress] = useState<any[]>(
    []
  )
  const [selected_shipping_address, setSelectedShippingAddress] =
    useState<any>(null)
  const [loading_products, setLoadingProducts] = useState(true)
  const [modal_new_address, setModalNewAddress] = useState(false)
  const [member_levels, setMemberLevels] = useState<any>([])

  const [subdistrict_options, setSubdistrictOptions] = useState([])
  const [postal_code_options, setPostalCodeOptions] = useState([])
  const [subdistrict, setSubdistrict] = useState<string | null>(null)
  const [loading_subdistrict_option, setLoadingSubdistrictOption] =
    useState(false)
  const throttled_subdistrict = useThrottle(subdistrict, 2000)

  const watch_subdistrict = watch('subdistrict')

  const [courier_options, setCourierOptions] = useState([])
  const [selected_courier, setSelectedCourier] = useState<any>(null)

  useEffect(() => {
    const fetchSubdistrict = async () => {
      if (throttled_subdistrict) {
        setLoadingSubdistrictOption(true)
        const { data } = await fetcher(
          `/api/flow/listing-locations?type=subdistrict&name=${throttled_subdistrict}`
        )
        setSubdistrictOptions(data)
        setTimeout(() => {
          setLoadingSubdistrictOption(false)
        }, 2000)
      }
    }
    fetchSubdistrict()
  }, [throttled_subdistrict])

  useEffect(() => {
    const fetchPostalCodeOptions = async () => {
      if (watch_subdistrict) {
        const { data } = await fetcher(
          `/api/flow/listing-postal-codes?name=${watch_subdistrict.subdistrict_code}`
        )
        setPostalCodeOptions(data)
      }
    }
    fetchPostalCodeOptions()
  }, [watch_subdistrict])

  useEffect(() => {
    const fetchProductOptions = async () => {
      const { data } = await fetcher('/api/products')
      setProductOptions(data)
    }
    fetchProductOptions()
  }, [])

  useEffect(() => {
    const fetchProducts = async () => {
      if (cart.length > 0 && product_options.length > 0) {
        const cart_product_ids = cart.map((item) => item.product_id)
        const filtered_products = product_options.filter(
          (d: any) => cart_product_ids.indexOf(d.id) >= 0
        )
        const mapped_products = filtered_products.map((fp: any) => {
          return {
            ...fp,
            quantity: cart.find((i) => i.product_id === fp.id)?.quantity,
            checked: false,
          }
        })
        setProducts(mapped_products)
        setLoadingProducts(false)
      } else {
        setLoadingProducts(false)
      }
    }
    fetchProducts()
  }, [cart, product_options])

  useEffect(() => {
    const fetchMemberShippingAddress = async () => {
      if (member) {
        const shipping_addresses = await fetcher('/api/flow/my-address')
        setMemberShippingAddress(shipping_addresses)
      }
    }
    fetchMemberShippingAddress()
  }, [member])

  useEffect(() => {
    const fetchCouriers = async () => {
      if (products.length > 0 && selected_shipping_address) {
        if (products.filter((p) => p.checked).length > 0) {
          setCourierOptions([])
          console.log(selected_shipping_address)
          const response = await api.post('/api/flow/check-shipping-price', {
            items: products
              .filter((p) => p.checked)
              .map((p) => {
                return {
                  product_id: p.id,
                  quantity: p.quantity,
                }
              }),
            member_shipping_address_id: selected_shipping_address.id,
          })

          setCourierOptions(response.data)
        }
      }
    }
    fetchCouriers()
  }, [products, selected_shipping_address])

  useEffect(() => {
    const fetchMemberLevels = async () => {
      const {
        data: { data },
      } = await api.get('/api/member-levels')

      setMemberLevels(data)
    }
    fetchMemberLevels()
  }, [])

  const total_product_quantity = useMemo(() => {
    return products
      .map((p: any) => p.quantity)
      .reduce((prev_q: number, current_q: number) => prev_q + current_q, 0)
  }, [products])

  const next_member_level = useMemo(() => {
    let found = member_levels.find(
      (ml: any) =>
        ml.minimum_order > member.member_level.minimum_order &&
        ml.minimum_order > total_product_quantity
    )

    if (!found) {
      if (member_levels.length > 0) {
        found = member_levels[member_levels.length - 1]
      }
    }

    return found
  }, [total_product_quantity])

  if (
    !member ||
    !cart ||
    member_shipping_address.length === 0 ||
    member_levels.length === 0
  ) {
    return <LoadingScreen />
  }

  const onReduceItem = (product_id: string, quantity: number) => {
    // Update Cookie Data
    if (quantity > 0) {
      updateItem({ product_id, quantity })
    } else {
      toast.error('Jumlah produk tidak boleh 0')
      return
      // deleteItem([product_id])
    }

    // Update UI
    const copy_products = Array.from(products)
    const product_index = copy_products.findIndex(
      (cp: any) => cp.id === product_id
    )
    if (quantity > 0) {
      copy_products.splice(product_index, 1, {
        ...copy_products[product_index],
        quantity,
      })
    } else {
      copy_products.splice(product_index, 1)
    }
    setProducts(copy_products)
  }

  const onAddItem = (product_id: string, quantity: number) => {
    // Update Cookie Data
    updateItem({ product_id, quantity })

    // Update UI
    const copy_products = Array.from(products)
    const product_index = copy_products.findIndex(
      (cp: any) => cp.id === product_id
    )
    copy_products.splice(product_index, 1, {
      ...copy_products[product_index],
      quantity,
    })
    setProducts(copy_products)
  }

  const onChangeItemQuantity = (
    product_id: string,
    previous_quantity: number,
    next_quantity: number
  ) => {
    if (next_quantity <= 0) {
      return
    }
    if (next_quantity > previous_quantity) {
      onAddItem(product_id, next_quantity)
    } else {
      onReduceItem(product_id, next_quantity)
    }
  }

  const onCheckItem = (product_id: string, check_status: boolean) => {
    // Update UI
    const copy_products = Array.from(products)
    const product_index = copy_products.findIndex(
      (cp: any) => cp.id === product_id
    )
    copy_products.splice(product_index, 1, {
      ...copy_products[product_index],
      checked: check_status,
    })
    setProducts(copy_products)
  }

  const onMakeOrder = async () => {
    toast.info('Memproses Pesanan...')

    if (!selected_shipping_address) {
      toast.error('Shipping address invalid!')
      return
    }

    if (products.length === 0) {
      toast.error('Product is invalid!')
      return
    }

    if (!selected_courier) {
      toast.error('Courier is invalid!')
      return
    }

    try {
      const { data } = await api.post('/api/flow/make-order', {
        items: products
          .filter((p) => p.checked)
          .map((p: any) => {
            return {
              product_id: p.id,
              quantity: p.quantity,
            }
          }),
        member_shipping_address_id: selected_shipping_address.id,
        shipping_courier_code: selected_courier.courierCode,
        shipping_courier_service: selected_courier.service,
        shipping_type: 'courier',
      })
      toast.success('Order Created!')
      const products_copy = Array.from(products)
      const products_leftover = products_copy.filter((p) => !p.checked)
      deleteItem(products.filter((p) => p.checked).map((p: any) => p.id))
      setProducts(products_leftover)
      setTimeout(() => {
        window.location.href = '/dashboard/order'
      }, 5000)
    } catch (error) {
      toast.error('Error Happened!')
    }
  }

  const onDeleteItem = (product_id: string) => {
    if (confirm('Apakah anda yakin?')) {
      deleteItem([product_id])
      let products_copy = Array.from(products)
      products_copy = products_copy.filter((p) => p.id !== product_id)
      setProducts(products_copy)
    }
  }

  const onAddNewAddress = async () => {
    const postal_code = getValues('postal_code')
    const shipping_address = getValues('shipping_address')

    if (!postal_code || !shipping_address) {
      toast.error('Informasi Alamat kurang lengkap!')
    }

    try {
      toast.info('Menyimpan alamat...')
      const {
        city_code,
        city_name,
        postal_code: postal_code_value,
        province_code,
        province_name,
        subdistrict_code,
        subdistrict_name,
      } = postal_code
      const new_shipping_address = {
        city_code,
        city_name,
        postal_code: postal_code_value,
        province_code,
        province_name,
        subdistrict_code,
        subdistrict_name,
        contact_address: shipping_address,
      }
      await api.post('/api/flow/add-shipping-address', new_shipping_address)
      toast.success('Berhasil menyimpan alamat!')
      reload()
    } catch (error) {
      toast.info('Terjadi Error!')
      console.log(error)
    }
  }

  const onCheckAll = () => {
    // Update UI
    const copy_products = Array.from(products)
    copy_products.forEach((_p: any, i: number) => {
      copy_products[i].checked = true
    })
    setProducts(copy_products)
  }

  const onUnCheckAll = () => {
    // Update UI
    const copy_products = Array.from(products)
    copy_products.forEach((_p: any, i: number) => {
      copy_products[i].checked = false
    })
    setProducts(copy_products)
  }

  if (!member) {
    return <>Loading</>
  }

  return (
    <DashboardLayout>
      {loading_products
        ? 'Fetching Products...'
        : products.length == 0 && 'Tidak ada Produk.'}
      {products.length > 0 && (
        <div className="flex items-center p-6">
          <input
            type="checkbox"
            className="mr-4 h-4 w-4 rounded bg-jankos-pink-100"
            onChange={(e) => {
              if (e.target.checked) {
                onCheckAll()
              } else {
                onUnCheckAll()
              }
            }}
            checked={products.every((p: any) => p.checked)}
          />
          <p className="text-xs font-semibold">Pilih Semua</p>
        </div>
      )}
      {!loading_products &&
        products.map((product) => (
          <div
            className="relative mb-4 flex items-center justify-between rounded-lg bg-white shadow-md"
            key={product.id}
          >
            <div className="flex items-center p-6">
              <input
                type="checkbox"
                className="mr-4 h-4 w-4 rounded bg-jankos-pink-100"
                onChange={(e) => onCheckItem(product.id, e.target.checked)}
                checked={product.checked}
              />
              <img
                src={product.thumbnail}
                className="mr-4 h-20 w-20 rounded border border-jankos-pink-100"
                alt=""
              />
              <div className="">
                <p className="mb-1.5 text-xs font-semibold">{product.name}</p>
                <p className="mb-1.5 text-xs font-semibold text-jankos-pink-300">
                  Rp
                  {product.product_level_prices
                    .find((plp: any) => {
                      let active_member_level = member.member_level
                      if (
                        next_member_level &&
                        total_product_quantity > next_member_level.minimum_order
                      ) {
                        active_member_level = next_member_level
                      }
                      return plp.member_level_id === active_member_level.id
                    })
                    .price.toLocaleString('id-ID')}
                </p>
                <p className="text-xs">Berat: {product.weight}gr</p>
              </div>
            </div>
            <div className="flex h-full items-center">
              <div className="mx-auto mr-14">
                <div className="mb-4 flex h-10 items-center justify-start">
                  <button
                    className="flex h-full items-center justify-center rounded-l bg-white px-6 text-xl font-semibold transition-colors active:bg-slate-200"
                    onClick={onReduceItem.bind(
                      this,
                      product.id,
                      product.quantity - 1
                    )}
                  >
                    -
                  </button>
                  <input
                    type="number"
                    className="flex h-full w-16 items-center justify-center overflow-hidden rounded-lg border border-slate-300 bg-white text-center font-semibold"
                    value={product.quantity}
                    onChange={(e) =>
                      onChangeItemQuantity(
                        product.id,
                        product.quantity,
                        parseInt(e.target.value)
                      )
                    }
                    min={0}
                  />
                  {/* <span className="">
                    {product.quantity}
                  </span> */}
                  <button
                    className="flex h-full items-center justify-center rounded-r bg-white px-6 text-xl font-semibold transition-colors active:bg-slate-200"
                    onClick={onAddItem.bind(
                      this,
                      product.id,
                      product.quantity + 1
                    )}
                  >
                    +
                  </button>
                </div>
                {next_member_level &&
                  (next_member_level.minimum_order - total_product_quantity >
                  0 ? (
                    <p className="text-center text-xs">
                      Beli{' '}
                      {next_member_level.minimum_order - total_product_quantity}{' '}
                      paket lagi
                      <br />& capai Jenjang {next_member_level.name}!
                    </p>
                  ) : (
                    <p className="text-center text-xs">
                      Selamat anda akan
                      <br />
                      menjadi member {next_member_level.name}
                    </p>
                  ))}
              </div>
              <button
                className="flex h-full items-center justify-center bg-jankos-pink-100 px-4"
                onClick={onDeleteItem.bind(this, product.id)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6 text-jankos-pink-300"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                  />
                </svg>
              </button>
            </div>
          </div>
        ))}
      <div className="mb-6 mt-12 grid grid-cols-4">
        <p className="text-sm">Alamat Pengiriman</p>
        <div className="col-span-2">
          <Select
            options={member_shipping_address.map((msa: any) => {
              return {
                value: msa,
                label: `${msa.address_name}, ${msa.contact_name}, ${msa.contact_address}, ${msa.subdistrict_name}, ${msa.city_name}, ${msa.postal_code}`,
              }
            })}
            onChange={({ value }: any) => setSelectedShippingAddress(value)}
            className="mb-4"
          />
          {/* <div className="mb-4 rounded border border-slate-300 bg-jankos-pink-100 p-6">
            <p className="text-xs">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt
              iste delectus pariatur omnis maxime hic, eius ipsam qui? Totam,
              quaerat?
            </p>
          </div> */}
          <button
            className="flex items-center justify-start rounded bg-jankos-pink-300 py-2 px-4 text-xs font-semibold text-white"
            onClick={setModalNewAddress.bind(this, true)}
          >
            Tambah Alamat Baru
          </button>
        </div>
      </div>
      <div className="mb-6 grid grid-cols-4">
        <p className="text-sm">Metode Pengiriman</p>
        <Select
          options={courier_options.map((option: any) => {
            return {
              value: option,
              label: `${option.courier} - ${
                option.service
              } - Rp${option.price.toLocaleString()}`,
            }
          })}
          className="col-span-2"
          onChange={(value: any) => setSelectedCourier(value.value)}
        />
      </div>
      <button
        className="ml-auto flex items-center justify-start rounded bg-jankos-pink-300 py-2 px-4 text-sm font-semibold text-white"
        onClick={onMakeOrder}
      >
        Buat Pesanan
      </button>
      {modal_new_address && (
        <div className="fixed top-0 left-0 z-10 flex h-screen w-screen items-center justify-center">
          <div
            className="absolute top-0 left-0 h-full w-full cursor-pointer bg-black bg-opacity-50"
            onClick={setModalNewAddress.bind(this, false)}
          />
          <div className="z-10 block h-auto w-2/3 rounded bg-white p-9">
            <p className="mb-9 text-center text-xl font-bold">
              Tambah Alamat Anda
            </p>
            <div className="col-span-3 mb-4 grid lg:grid-cols-3">
              <div className="flex items-center">
                <label className="col-span-1 text-sm">
                  Alamat Kecamatan Pengiriman
                </label>
              </div>
              <Select
                options={subdistrict_options.map((option: any) => {
                  return {
                    value: option,
                    label: `${option.subdistrict_name}, ${option.city.city_name}, ${option.city.province.province_name}`,
                  }
                })}
                onInputChange={(value) => setSubdistrict(value)}
                onChange={(value: any) => setValue('subdistrict', value.value)}
                isLoading={loading_subdistrict_option}
                className="lg:col-span-2"
              />
            </div>
            <div className="col-span-3 mb-4 grid lg:grid-cols-3">
              <div className="flex items-center">
                <label className="col-span-1 text-sm">Kode Pos</label>
              </div>
              <Select
                options={postal_code_options.map((option: any) => {
                  return {
                    value: option,
                    label: option.postal_code,
                  }
                })}
                onChange={(value: any) => setValue('postal_code', value.value)}
                className="lg:col-span-2"
              />
            </div>
            <div className="col-span-3 mb-4 grid lg:grid-cols-3">
              <div className="flex items-center">
                <label className="col-span-1 text-sm">Alamat Pengiriman</label>
              </div>
              <textarea
                cols={30}
                rows={3}
                className="rounded border border-slate-300 bg-white py-2 px-6 lg:col-span-2"
                {...register('shipping_address', { required: true })}
              ></textarea>
            </div>
            <div className="text-center">
              <button
                className="rounded bg-jankos-pink-300 py-2 px-6 text-xs font-semibold text-white"
                onClick={onAddNewAddress}
              >
                Simpan
              </button>
            </div>
          </div>
        </div>
      )}
    </DashboardLayout>
  )
}

export default Cart
