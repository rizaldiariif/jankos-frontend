import { format } from 'date-fns'
import { NextPage } from 'next'
import { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicInput from '../../../components/shared/BasicInput'
import DashboardLayout from '../../../components/shared/DashboardLayout'
import LoadingScreen from '../../../components/shared/LoadingScreen'
import { api } from '../../../helper/api'
import { useCurrentMember } from '../../../helper/useCurrentMember'

type Props = {}

const Commissions: NextPage<Props> = (props: Props) => {
  const { member } = useCurrentMember()
  const [current_amount, setCurrentAmount] = useState(0)
  const [referral_ledgers, setReferralLedgers] = useState<any[]>([])
  const [show_modal, setShowModal] = useState(false)

  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm()

  useEffect(() => {
    if (member) {
      setCurrentAmount(
        member.member_referrals
          .map((referral: any) =>
            referral.member_referral_ledgers
              .map((ledger: any) => ledger.amount)
              .reduce(
                (total_price: any, current_ledger: any) =>
                  total_price + current_ledger,
                0
              )
          )
          .reduce(
            (total_price: any, current_price: any) =>
              total_price + current_price,
            0
          )
      )
      setReferralLedgers(
        [].concat.apply(
          [],
          member.member_referrals.map((referral: any) => {
            return referral.member_referral_ledgers.map((ledger: any) => {
              return { ...ledger, referred_member: referral.referred_member }
            })
          })
        )
      )
    }
  }, [member, setCurrentAmount])

  if (!member) {
    return <LoadingScreen />
  }

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Memproses data...')
    try {
      const response = await api.post('/api/flow/create-withdrawal', data)
      console.log(response.data)
    } catch (error) {
      console.log(error)
      toast.success('Sukses menarik dana!')
    }
  })

  return (
    <>
      <DashboardLayout>
        <div className="mb-11 w-min rounded-lg border bg-white p-6 pr-20 shadow-md">
          <p className="mb-2 whitespace-nowrap text-sm font-medium">
            Saldo Saya
          </p>
          <p className="mb-4 whitespace-nowrap text-4xl font-bold text-jankos-pink-300">
            Rp
            {current_amount.toLocaleString()}
          </p>
          <button
            className="whitespace-nowrap rounded bg-jankos-pink-300 py-1 px-3 text-sm font-semibold text-white"
            onClick={setShowModal.bind(this, true)}
          >
            Tarik Dana
          </button>
        </div>
        <p className="mb-6 font-semibold">Riwayat Komisi</p>
        {referral_ledgers.map((ledger: any) => (
          <div
            className="mb-4 flex items-start justify-between rounded-lg border bg-white p-7 shadow-md"
            key={ledger.id}
          >
            <div className="flex">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="mr-5 h-11 w-11 text-jankos-pink-300"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path d="M8.433 7.418c.155-.103.346-.196.567-.267v1.698a2.305 2.305 0 01-.567-.267C8.07 8.34 8 8.114 8 8c0-.114.07-.34.433-.582zM11 12.849v-1.698c.22.071.412.164.567.267.364.243.433.468.433.582 0 .114-.07.34-.433.582a2.305 2.305 0 01-.567.267z" />
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-13a1 1 0 10-2 0v.092a4.535 4.535 0 00-1.676.662C6.602 6.234 6 7.009 6 8c0 .99.602 1.765 1.324 2.246.48.32 1.054.545 1.676.662v1.941c-.391-.127-.68-.317-.843-.504a1 1 0 10-1.51 1.31c.562.649 1.413 1.076 2.353 1.253V15a1 1 0 102 0v-.092a4.535 4.535 0 001.676-.662C13.398 13.766 14 12.991 14 12c0-.99-.602-1.765-1.324-2.246A4.535 4.535 0 0011 9.092V7.151c.391.127.68.317.843.504a1 1 0 101.511-1.31c-.563-.649-1.413-1.076-2.354-1.253V5z"
                  clipRule="evenodd"
                />
              </svg>
              <div className="">
                <p className="mb-3 text-xs">
                  {format(new Date(ledger.createdAt), 'yyyy-MM-dd kk:mm')}
                </p>
                <p className="mb-3 text-sm font-semibold">
                  Komisi dari{' '}
                  <span className="text-green-600">
                    {member.member_level.commision_percentage}%
                  </span>{' '}
                  transaksi
                  {/* {ledger.referred_member.member_level.name} */} afiliasi{' '}
                  {ledger.referred_member.name}
                </p>
                <ul className="list-inside list-disc">
                  {ledger.order.order_details.map((detail: any) => (
                    <li className="text-xs">
                      {detail.na} {detail.qu} pcs
                    </li>
                  ))}
                </ul>
              </div>
            </div>
            <p className="text-xl font-bold text-jankos-pink-300">
              Rp{ledger.amount.toLocaleString()}
            </p>
          </div>
        ))}
      </DashboardLayout>
      {show_modal && (
        <div className="fixed top-0 left-0 z-10 flex h-screen w-screen items-center justify-center">
          <div
            className="absolute top-0 left-0 h-full w-full cursor-pointer bg-black bg-opacity-50"
            onClick={setShowModal.bind(this, false)}
          />
          <div className="z-10 block h-auto w-1/2 rounded bg-white p-9">
            <form onSubmit={onSubmit}>
              <p className="mb-9 text-center text-xl font-bold">Tarik Dana</p>
              <div className="mb-2 grid grid-cols-3 items-center">
                <div className="col-span-1">
                  <p className="mb-4 text-xs">Nama Bank</p>
                </div>
                <div className="col-span-2">
                  <BasicInput
                    error={errors.bank_name}
                    field_name="bank_name"
                    label=""
                    register={register}
                    default_value={member.member_bank_account.bank_name}
                    disabled={true}
                  />
                </div>
              </div>
              <div className="mb-2 grid grid-cols-3 items-center">
                <div className="col-span-1">
                  <p className="mb-4 text-xs">No Rekening</p>
                </div>
                <div className="col-span-2">
                  <BasicInput
                    error={errors.bank_account_number}
                    field_name="bank_account_number"
                    label=""
                    register={register}
                    default_value={
                      member.member_bank_account.bank_account_number
                    }
                    disabled={true}
                  />
                </div>
              </div>
              <div className="mb-2 grid grid-cols-3 items-center">
                <div className="col-span-1">
                  <p className="mb-4 text-xs">Nama Pemilik Rekening</p>
                </div>
                <div className="col-span-2">
                  <BasicInput
                    error={errors.bank_account_name}
                    field_name="bank_account_name"
                    label=""
                    register={register}
                    default_value={member.member_bank_account.bank_account_name}
                    disabled={true}
                  />
                </div>
              </div>
              <div className="mb-2 grid grid-cols-3 items-center">
                <div className="col-span-1">
                  <p className="mb-4 text-xs">Nominal</p>
                </div>
                <div className="col-span-2">
                  <BasicInput
                    error={errors.amount}
                    field_name="amount"
                    label=""
                    register={register}
                    default_value={0}
                  />
                </div>
              </div>
              <div className="text-right">
                <button
                  type="submit"
                  className="ml-auto rounded-lg bg-jankos-pink-300 py-2 px-7 text-sm font-semibold text-white"
                >
                  Tarik Sekarang
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  )
}

export default Commissions
