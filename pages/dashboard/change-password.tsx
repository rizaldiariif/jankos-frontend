import { NextPage } from 'next'
import React from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicInput from '../../components/shared/BasicInput'
import DashboardLayout from '../../components/shared/DashboardLayout'
import { api } from '../../helper/api'

type Props = {}

const ChangePassword: NextPage<Props> = (props: Props) => {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan data...')
    try {
      const { password, confirm_password, current_password } = data

      if (password !== confirm_password) {
        toast.error('Password is not match!')
        return
      }

      const form_data = {
        current_password,
        password,
      }

      await api.post('/api/members/update-my-password', form_data)

      toast.success('Berhasil menyimpan data!')
      setTimeout(() => {
        window.location.reload()
      }, 2000)
    } catch (error: any) {
      console.log(error)
      // toast.error('Error happened!')
      error.response.data.errors.map((err: any) => {
        toast.error(err.message)
      })
    }
  })

  return (
    <DashboardLayout>
      <form onSubmit={onSubmit}>
        <div className="mb-10 rounded-lg border py-7 px-16">
          <h3 className="mb-7 font-semibold">Kata Sandi</h3>
          <div className="mb-2 grid grid-cols-4 items-center">
            <div className="col-span-1">
              <p className="mb-4">Kata Sandi Lama</p>
            </div>
            <div className="col-span-2">
              <BasicInput
                error={errors.current_password}
                field_name="current_password"
                label=""
                register={register}
                type="password"
              />
            </div>
          </div>
          <div className="mb-2 grid grid-cols-4 items-center">
            <div className="col-span-1">
              <p className="mb-4">Kata Sandi Baru</p>
            </div>
            <div className="col-span-2">
              <BasicInput
                error={errors.password}
                field_name="password"
                label=""
                register={register}
                type="password"
              />
            </div>
          </div>
          <div className="mb-2 grid grid-cols-4 items-center">
            <div className="col-span-1">
              <p className="mb-4">Ketik Ulang Kata Sandi</p>
            </div>
            <div className="col-span-2">
              <BasicInput
                error={errors.confirm_password}
                field_name="confirm_password"
                label=""
                register={register}
                type="password"
              />
            </div>
          </div>
        </div>
        <div className="text-right">
          <button
            type="submit"
            className="ml-auto rounded-lg bg-jankos-pink-300 py-2 px-7 text-sm font-semibold text-white"
          >
            Simpan Perubahan
          </button>
        </div>
      </form>
    </DashboardLayout>
  )
}

export default ChangePassword
