import { NextPage } from 'next'
import React, { useEffect } from 'react'
import { useCurrentMember } from '../../../helper/useCurrentMember'
import DashboardLayout from '../../../components/shared/DashboardLayout'
import { useRouter } from 'next/router'
import { format } from 'date-fns'
import LoadingScreen from '../../../components/shared/LoadingScreen'
import shortUUID from 'short-uuid'
import { formatIncrement } from '../../../helper/format-increment'

type Props = {}

function reverseArr(input: any[]) {
  var ret = new Array()
  for (var i = input.length - 1; i >= 0; i--) {
    ret.push(input[i])
  }
  return ret
}

const findOrderStatusColor = (order_status_id: string) => {
  switch (order_status_id) {
    case '38d64e47-7b71-47b0-a3f8-7171c64ea073':
      return '#DE1306'
    case '4e07f208-843b-4419-bba4-5a6c18e1e066':
      return '#EBBB46'
    case 'd4b02d1d-3a21-4ba6-a658-7d304065713a':
      return '#77C5E7'
    case 'ce94b478-df89-451d-8b6f-5cd27ea0096a':
      return '#65BF73'
    default:
      return '#CC676A'
  }
}

const MyOrderDetail: NextPage<Props> = (props: Props) => {
  const {
    query: { id },
  } = useRouter()
  const { my_order_detail, fetchMyOrderDetail } = useCurrentMember()

  useEffect(() => {
    if (typeof id === 'string') {
      fetchMyOrderDetail(id)
    }
  }, [id])

  if (!my_order_detail) {
    return <LoadingScreen />
  }

  console.log({ my_order_detail })

  return (
    <DashboardLayout>
      <div className="relative mb-4 grid grid-cols-4 place-items-center rounded-lg border border-slate-200 bg-white p-6 pb-16 shadow-md">
        <div className="relative z-10">
          <div
            className={`z-10 flex h-14 w-14 items-center justify-center rounded-full border-4  bg-white ${
              my_order_detail.order_status_histories.find(
                (h: any) => h.order_status.code === 'menunggu_pembayaran'
              )
                ? 'border-jankos-pink-300 text-jankos-pink-300'
                : 'border-slate-400 text-slate-400'
            }`}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M5 8h14M5 8a2 2 0 110-4h14a2 2 0 110 4M5 8v10a2 2 0 002 2h10a2 2 0 002-2V8m-9 4h4"
              />
            </svg>
          </div>
          <div className="absolute left-1/2 bottom-0 -translate-x-1/2 translate-y-12 transform whitespace-nowrap text-center">
            {my_order_detail.order_status_histories.find(
              (h: any) => h.order_status.code === 'menunggu_pembayaran'
            ) && (
              <>
                <p className="mb-1.5 text-sm font-semibold">
                  Menunggu Pembayaran
                </p>
                <p className="text-xs">
                  {format(
                    new Date(
                      my_order_detail.order_status_histories.find(
                        (h: any) =>
                          h.order_status.code === 'menunggu_pembayaran'
                      ).createdAt
                    ),
                    'dd MMM yyyy - HH:mm'
                  )}
                </p>
              </>
            )}
          </div>
        </div>
        <div className="relative z-10">
          <div
            className={`z-10 flex h-14 w-14 items-center justify-center rounded-full border-4  bg-white ${
              my_order_detail.order_status_histories.find(
                (h: any) => h.order_status.code === 'processing'
              )
                ? 'border-jankos-pink-300 text-jankos-pink-300'
                : 'border-slate-400 text-slate-400'
            }`}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
              />
            </svg>
          </div>
          <div className="absolute left-1/2 bottom-0 -translate-x-1/2 translate-y-12 transform whitespace-nowrap text-center">
            {my_order_detail.order_status_histories.find(
              (h: any) => h.order_status.code === 'processing'
            ) && (
              <>
                <p className="mb-1.5 text-sm font-semibold">Diproses</p>
                <p className="text-xs">
                  {format(
                    new Date(
                      my_order_detail.order_status_histories.find(
                        (h: any) => h.order_status.code === 'processing'
                      ).createdAt
                    ),
                    'dd MMM yyyy - HH:mm'
                  )}
                </p>
              </>
            )}
          </div>
        </div>
        <div className="relative z-10">
          <div
            className={`z-10 flex h-14 w-14 items-center justify-center rounded-full border-4  bg-white ${
              my_order_detail.order_status_histories.find(
                (h: any) => h.order_status.code === 'delivering'
              )
                ? 'border-jankos-pink-300 text-jankos-pink-300'
                : 'border-slate-400 text-slate-400'
            }`}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path d="M9 17a2 2 0 11-4 0 2 2 0 014 0zM19 17a2 2 0 11-4 0 2 2 0 014 0z" />
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M13 16V6a1 1 0 00-1-1H4a1 1 0 00-1 1v10a1 1 0 001 1h1m8-1a1 1 0 01-1 1H9m4-1V8a1 1 0 011-1h2.586a1 1 0 01.707.293l3.414 3.414a1 1 0 01.293.707V16a1 1 0 01-1 1h-1m-6-1a1 1 0 001 1h1M5 17a2 2 0 104 0m-4 0a2 2 0 114 0m6 0a2 2 0 104 0m-4 0a2 2 0 114 0"
              />
            </svg>
          </div>
          <div className="absolute left-1/2 bottom-0 -translate-x-1/2 translate-y-12 transform whitespace-nowrap text-center">
            {my_order_detail.order_status_histories.find(
              (h: any) => h.order_status.code === 'delivering'
            ) && (
              <>
                <p className="mb-1.5 text-sm font-semibold">Dikirim</p>
                <p className="text-xs">
                  {format(
                    new Date(
                      my_order_detail.order_status_histories.find(
                        (h: any) => h.order_status.code === 'delivering'
                      ).createdAt
                    ),
                    'dd MMM yyyy - HH:mm'
                  )}
                </p>
              </>
            )}
          </div>
        </div>
        <div className="relative z-10">
          <div
            className={`z-10 flex h-14 w-14 items-center justify-center rounded-full border-4  bg-white ${
              my_order_detail.order_status_histories.find(
                (h: any) => h.order_status.code === 'received'
              )
                ? 'border-jankos-pink-300 text-jankos-pink-300'
                : 'border-slate-400 text-slate-400'
            }`}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"
              />
            </svg>
          </div>
          <div className="absolute left-1/2 bottom-0 -translate-x-1/2 translate-y-12 transform whitespace-nowrap text-center">
            {my_order_detail.order_status_histories.find(
              (h: any) => h.order_status.code === 'delivered'
            ) && (
              <>
                <p className="mb-1.5 text-sm font-semibold">Diterima</p>
                <p className="text-xs">
                  {format(
                    new Date(
                      my_order_detail.order_status_histories.find(
                        (h: any) => h.order_status.code === 'delivered'
                      ).createdAt
                    ),
                    'dd MMM yyyy - HH:mm'
                  )}
                </p>
              </>
            )}
          </div>
        </div>
        <span
          className={`absolute top-1/2 left-1/4 h-2 w-1/4 -translate-y-6 -translate-x-1/2 transform ${
            my_order_detail.order_status_histories.find(
              (h: any) => h.order_status.code === 'processing'
            )
              ? 'bg-jankos-pink-300'
              : 'bg-slate-400'
          }`}
        ></span>
        <span
          className={`absolute top-1/2 left-1/2 h-2 w-1/4 -translate-y-6 -translate-x-1/2 transform ${
            my_order_detail.order_status_histories.find(
              (h: any) => h.order_status.code === 'delivering'
            )
              ? 'bg-jankos-pink-300'
              : 'bg-slate-400'
          }`}
        ></span>
        <span
          className={`absolute top-1/2 right-1/4 h-2 w-1/4 -translate-y-6 translate-x-1/2 transform ${
            my_order_detail.order_status_histories.find(
              (h: any) => h.order_status.code === 'received'
            )
              ? 'bg-jankos-pink-300'
              : 'bg-slate-400'
          }`}
        ></span>
      </div>
      <div className="mb-4 grid gap-2 rounded-lg border border-jankos-pink-100 bg-white p-6 shadow-md lg:grid-cols-4">
        <div className="col-span-1">
          <p className="mb-4 font-semibold">Alamat Pengiriman</p>
          <p className="text-xs leading-normal">
            {my_order_detail.contact_address},{' '}
            {my_order_detail.subdistrict_name},{my_order_detail.city_name},{' '}
            {my_order_detail.province_name},{my_order_detail.postal_code}
          </p>
        </div>
        <div className="lg:col-span-3 lg:border-l-2 lg:border-slate-200">
          {reverseArr(my_order_detail.order_status_histories)
            .sort((a: any, b: any) => {
              if (a.createdAt > b.createdAt) {
                return -1
              }
              if (a.createdAt < b.createdAt) {
                return 1
              }
              return 0
            })
            .map((history: any, i: number) => (
              <div className="grid grid-cols-6 py-2">
                <div className="relative flex items-center justify-center">
                  <span
                    className={`z-10 h-3 w-3 rounded-full ${
                      i == 0 ? 'bg-jankos-pink-300' : 'bg-slate-400'
                    }`}
                  ></span>
                  {i != my_order_detail.order_status_histories.length - 1 && (
                    <span className="absolute top-0 left-1/2 z-0 h-8 w-0.5 -translate-x-1/2 translate-y-2 transform bg-slate-400"></span>
                  )}
                </div>
                <p className="col-span-2 flex items-center pt-1 text-xs font-medium">
                  {format(new Date(history.createdAt), 'dd MMM yyyy - HH:mm')}
                </p>
                <p
                  className={`col-span-3 flex items-center pt-1 text-xs  ${
                    i == 0 ? 'text-jankos-pink-300' : 'text-black'
                  }`}
                >
                  {history.order_status.name}
                </p>
              </div>
            ))}
        </div>
      </div>
      <div className="mb-4 grid gap-y-4 rounded-lg border border-jankos-pink-100 bg-white p-6 shadow-md lg:grid-cols-4">
        <div className="col-span-3">
          <div className="mb-4 flex flex-col lg:flex-row lg:items-center">
            <p className="mr-6 text-xs font-semibold">
              {format(
                new Date(my_order_detail.createdAt),
                'dd MMM yyyy - HH:mm'
              )}
            </p>
            <p className="mr-6 text-xs">
              INV/{format(new Date(my_order_detail.createdAt), 'yyyy')}/
              {format(new Date(my_order_detail.createdAt), 'MM')}/
              {format(new Date(my_order_detail.createdAt), 'dd')}/
              {formatIncrement(my_order_detail.increment_count)}
            </p>
            <span
              className="mb-1 rounded py-1 px-3 text-xs font-semibold text-white"
              style={{
                backgroundColor: findOrderStatusColor(
                  my_order_detail.order_status_id
                ),
              }}
            >
              {my_order_detail.order_status.name}
            </span>
          </div>
          {my_order_detail.order_details.map((od: any) => (
            <div className="mb-2 flex items-start">
              <div className="relative mr-6 h-24 w-24">
                <img
                  src={od.thumbnail}
                  className="h-full w-auto rounded border border-jankos-pink-100"
                  alt=""
                />
              </div>
              <div className="">
                <p className="mb-2 text-sm font-semibold">{od.name}</p>
                <p className="mb-3 text-sm">{od.quantity} pcs</p>
                <p className="mb-4 text-sm">
                  Rp
                  {(od.quantity * od.final_price).toLocaleString()}
                </p>
              </div>
            </div>
          ))}
        </div>
        <div className="lg:border-l lg:border-l-jankos-pink-100 lg:pl-5">
          <p className="mb-2 text-sm">Biaya Pengiriman</p>
          <p className="mb-4 font-bold">
            Rp
            {my_order_detail.shipping_price.toLocaleString()}
          </p>
          <p className="mb-2 text-sm">Total Pesanan</p>
          <p className="mb-4 text-xl font-bold">
            Rp
            {(
              my_order_detail.order_details
                .map((od: any) => od.final_price * od.quantity)
                .reduce(
                  (total_price: number, current_price: number) =>
                    total_price + current_price
                ) + my_order_detail.shipping_price
            ).toLocaleString()}
          </p>
          {my_order_detail.order_status.code === 'menunggu_pembayaran' && (
            <a
              href={my_order_detail.payment_url}
              target="_blank"
              className="mb-1 rounded bg-jankos-pink-300 py-2 px-3 text-xs font-semibold text-white"
            >
              Pembayaran Klik Di Sini
            </a>
          )}
        </div>
      </div>
    </DashboardLayout>
  )
}

export default MyOrderDetail
