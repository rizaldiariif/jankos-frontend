import { NextPage } from 'next'
import React, { useEffect, useMemo, useState } from 'react'
import { useCurrentMember } from '../../../helper/useCurrentMember'
import DashboardLayout from '../../../components/shared/DashboardLayout'
import Link from 'next/link'
import { format } from 'date-fns'
import shortUUID from 'short-uuid'
import { api } from '../../../helper/api'
import DatePicker from 'react-datepicker'
import { formatIncrement } from '../../../helper/format-increment'

type Props = {}

const findOrderStatusColor = (order_status_id: string) => {
  switch (order_status_id) {
    case '38d64e47-7b71-47b0-a3f8-7171c64ea073':
      return '#DE1306'
    case '4e07f208-843b-4419-bba4-5a6c18e1e066':
      return '#EBBB46'
    case 'd4b02d1d-3a21-4ba6-a658-7d304065713a':
      return '#77C5E7'
    case 'ce94b478-df89-451d-8b6f-5cd27ea0096a':
      return '#65BF73'
    default:
      return '#CC676A'
  }
}

const MyOrders: NextPage<Props> = (props: Props) => {
  const { member } = useCurrentMember()
  const [orders, setOrders] = useState<any[]>([])
  const [loading, setLoading] = useState(true)
  const [page, setPage] = useState(1)
  const [next_page, setNextPage] = useState(false)
  const [prev_page, setPrevPage] = useState(false)

  const [order_status_id, setOrderStatusID] = useState(
    '38d64e47-7b71-47b0-a3f8-7171c64ea073'
  )

  const [start_date, setStartDate] = useState(
    new Date(
      new Date().getFullYear(),
      new Date().getMonth() - 1,
      new Date().getDate()
    )
  )
  const [end_date, setEndDate] = useState(new Date())

  const codes = useMemo(() => {
    return [
      {
        label: 'Menunggu Pembayaran',
        value: '38d64e47-7b71-47b0-a3f8-7171c64ea073',
      },
      {
        label: 'Diproses',
        value: '4e07f208-843b-4419-bba4-5a6c18e1e066',
      },
      {
        label: 'Diambil',
        value: '1e9fa11e-43df-4ebc-9cf7-27d15d85d140',
      },
      {
        label: 'Perjalanan',
        value: 'd4b02d1d-3a21-4ba6-a658-7d304065713a',
      },
      {
        label: 'Diterima',
        value: 'ce94b478-df89-451d-8b6f-5cd27ea0096a',
      },
      {
        label: 'Kadaluwarsa',
        value: '47da5cef-eb0d-4d5d-bd85-6f6fc41ad00c',
      },
    ]
  }, [])

  const fetchOrders = async (page: number, order_status_id: string) => {
    if (member) {
      setOrders([])
      let url = `/api/orders/by-member?page=${page}&limit=4&member_id.eq=${member.id}`
      if (start_date) {
        url += `&createdAt.gte=${start_date}`
      }
      if (end_date) {
        url += `&createdAt.lte=${end_date}`
      }
      if (order_status_id !== '') {
        url += `&order_status_id.eq=${order_status_id}`
      }
      const { data } = await api.get(url)

      setNextPage(data.pagination.next)
      setPrevPage(data.pagination.prev)
      setOrders(data.data)
      setLoading(false)
    }
  }

  useEffect(() => {
    fetchOrders(page, order_status_id)
  }, [page, order_status_id, start_date, end_date, member])

  const onChangeDate = (dates: any) => {
    const [start, end] = dates
    setStartDate(start)
    setEndDate(end)
  }

  return (
    <DashboardLayout>
      {/* <div className="rounded bg-white p-6 shadow-lg">My Orders</div> */}
      <div className="mb-9 flex items-center justify-between">
        <input
          type="text"
          className="hidden w-72 rounded-lg border-slate-300 py-3 px-4 text-xs shadow-md"
          placeholder="Cari berdasarkan kata kunci"
        />
        <DatePicker
          selected={start_date}
          onChange={onChangeDate}
          startDate={start_date}
          endDate={end_date}
          selectsRange
          className="w-52 rounded-lg border-slate-300 py-3 px-4 text-xs shadow-md"
          // inline
        />
        {/* <input
          type="text"
          className="w-72 rounded-lg border-slate-300 py-3 px-4 text-xs shadow-md"
          placeholder="Pilih tanggal transaksi"
        /> */}
      </div>
      <div className="mb-12 grid grid-cols-6 gap-6 rounded-lg border-b border-b-slate-200 bg-white shadow-md">
        {codes.map((c) => (
          <span
            id={c.value}
            onClick={() => {
              setPage(1)
              setOrderStatusID(c.value)
            }}
            className={`mx-auto w-min whitespace-nowrap p-3 text-sm transition-colors ${
              c.value === order_status_id
                ? 'border-b-4 border-b-jankos-pink-300 font-semibold text-black'
                : 'cursor-pointer font-medium text-slate-600'
            }`}
          >
            {c.label}
          </span>
        ))}
      </div>
      {loading
        ? 'Mengambil Data...'
        : orders.length === 0 && 'Tidak Ada Pesanan'}
      {orders.map((order) => (
        <Link href={`/dashboard/order/${order.id}`} key={order.id}>
          <a className="mb-2 grid gap-y-4 rounded-lg border border-slate-200 bg-white py-5 px-8 shadow-md transition-colors hover:border-jankos-pink-200 lg:grid-cols-4">
            <div className="lg:col-span-3">
              <div className="mb-4 flex flex-col lg:flex-row lg:items-center">
                <p className="mr-6 text-xs font-semibold">
                  {format(new Date(order.createdAt), 'dd MMM yyyy')}
                </p>
                <p className="text-xs">
                  {' '}
                  INV/{format(new Date(order.createdAt), 'yyyy')}/
                  {format(new Date(order.createdAt), 'MM')}/
                  {format(new Date(order.createdAt), 'dd')}/
                  {formatIncrement(order.increment_count)}
                </p>
                <span
                  className={`ml-9 rounded py-1 px-2 text-xs font-semibold text-white`}
                  style={{
                    backgroundColor: findOrderStatusColor(
                      order.order_status_id
                    ),
                  }}
                >
                  {order.order_status.name}
                </span>
              </div>
              <div className="flex items-start">
                <div className="relative mr-6 h-24 w-24">
                  <img
                    src={order.order_details[0].thumbnail}
                    className="h-full w-auto rounded border border-slate-200"
                    alt=""
                  />
                </div>
                <div className="">
                  <p className="mb-2 text-sm font-semibold">
                    {order.order_details[0].name}
                  </p>
                  <p className="mb-3 text-sm">
                    {order.order_details[0].quantity} pcs
                  </p>
                  <p className="mb-4 text-sm text-jankos-green-200">
                    Rp
                    {(
                      order.order_details[0].quantity *
                      order.order_details[0].final_price
                    ).toLocaleString()}
                  </p>
                  {order.order_details.length > 1 && (
                    <p className="text-sm text-jankos-pink-300">
                      + {order.order_details.length - 1} produk lainnya
                    </p>
                  )}
                </div>
              </div>
            </div>
            <div className="lg:border-l lg:border-l-slate-200 lg:pl-5">
              <p className="mb-2 text-sm">Total Pesanan</p>
              <p className="text-xl font-bold">
                Rp
                {order.order_details
                  .map((od: any) => od.final_price * od.quantity)
                  .reduce(
                    (total_price: number, current_price: number) =>
                      total_price + current_price
                  )
                  .toLocaleString()}
              </p>
            </div>
          </a>
        </Link>
      ))}
      {!loading && (
        <div className="mt-20 flex items-center justify-center lg:col-span-2">
          {prev_page && (
            <>
              <button
                className="flex h-10 w-10 items-center justify-center rounded-l border bg-jankos-pink-100 text-xs font-semibold"
                onClick={() => {
                  setLoading(true)
                  setPage(page - 1)
                }}
              >
                {'<'}
              </button>
              <button
                className="flex h-10 w-10 items-center justify-center border bg-jankos-pink-100 text-xs font-semibold"
                onClick={() => {
                  setLoading(true)
                  setPage(page - 1)
                }}
              >
                {page - 1}
              </button>
            </>
          )}
          <button
            className="flex h-10 w-10 items-center justify-center border bg-jankos-pink-200 text-xs font-bold"
            disabled
          >
            {page}
          </button>
          {next_page && (
            <>
              <button
                className="flex h-10 w-10 items-center justify-center border bg-jankos-pink-100 text-xs font-semibold"
                onClick={() => {
                  setLoading(true)
                  setPage(page + 1)
                }}
              >
                {page + 1}
              </button>
              <button
                className="flex h-10 w-10 items-center justify-center rounded-r border bg-jankos-pink-100 text-xs font-semibold"
                onClick={() => {
                  setLoading(true)
                  setPage(page + 1)
                }}
              >
                {'>'}
              </button>
            </>
          )}
        </div>
      )}
    </DashboardLayout>
  )
}

export default MyOrders
