import { GetStaticProps, NextPage } from 'next'
import React from 'react'
import useSWR from 'swr'
import Footer from '../components/shared/Footer'
import Header from '../components/shared/Header'
import HeaderMobile from '../components/shared/HeaderMobile'
import { fetcher } from '../helper/fetcher'

type Props = {
  initial_global_contents: any[]
}

const TermAndCondition: NextPage<Props> = (props) => {
  const { initial_global_contents } = props

  const {
    data: { data: global_contents },
  } = useSWR('/api/page-contents?page.eq=global', fetcher, {
    fallbackData: initial_global_contents,
  })

  return (
    <>
      <Header page_contents={global_contents} />
      <HeaderMobile />
      <div className="container mx-auto my-28">
        <h1 className="mb-11 text-center text-3xl font-bold">
          Terms & Condition
        </h1>
        <p className="mb-9 text-sm leading-8">
          <span className="font-semibold">PT. Jankos Karya Indonesia</span>{' '}
          dengan kantor di Janice Tanujaya Alamat Pick Up : Jl. Meruya Ilir Raya
          No.19,RT.8/RW.2, Meruya Utara, Jakarta Barat 11620 selanjutnya disebut
          sebagai Jankos Glow, pemilik dari situs web www.jankos.co.id,
          selanjutnya disebut sebagai SITUS WEB, melalui Syarat dan Ketentuan
          ini menetapkan aturan yang harus dipatuhi oleh Pihak dalam Kontrak,
          yaitu individu atau badan hukum yang mengakses SITUS WEB untuk layanan
          Jankos Glow. Layanan ini ditujukan bagi orang-orang yang ingin
          melakukan pembelian produk kosmetik Jankos Glow melalui SITUS WEB.
          Dengan mengakses SITUS atau melakukan kontrak pembelian produk
          dan/atau layanan, Pihak dalam Kontrak menyetujui semua syarat dan
          ketentuan yang tertera di sini.
        </p>
        <ul className="list-inside list-decimal text-sm leading-8">
          <li className="mb-9 font-semibold">
            Objek
            <ul className="list ml-4 mt-2 list-inside list-decimal font-normal">
              <li>
                Objek dari Syarat dan Ketentuan ini adalah untuk menetapkan
                aturan dan regulasi dalam mengakses SITUS WEB bagi pelanggan
                yang mendaftar untuk melakukan pembelian produk kosmetik Jankos
                Glow melalui SITUS WEB.
              </li>
            </ul>
          </li>
          <li className="mb-9 font-semibold">
            Tentang Pendaftaran Pada Situs
            <ul className="list ml-4 mt-2 list-inside list-decimal font-normal">
              <li>
                Untuk Pihak dalam Kontrak yang mengakses SITUS WEB, Jankos Glow,
                sekaligus beragam informasi yang terdapat dalam SITUS WEB, akan
                menawarkan layanan jual beli kosmetik dengan sistem reseller.
              </li>
              <li>
                SITUS WEB dapat diakses oleh individu atau badan hukum. Bagi
                individu, untuk melakukan jenis layanan kontrak apa pun, Pihak
                dalam Kontrak harus berusia lebih dari 18 (delapan belas) tahun,
                atau, jika kurang, wajib diwakilkan oleh orang tua, pengasuh,
                atau wali mereka.
              </li>
              <li>
                SITUS WEB dapat digunakan oleh individu atau badan hukum, warga
                negara Indonesia maupun asing, dengan memperjelas bahwa, karena
                bisnis akan dilakukan dalam wilayah nasional, undang-undang
                negara tersebut akan berlaku untuk menyelesaikan keraguan atau
                konflik apa pun.
              </li>
              <li>
                Halaman depan SITUS WEB beserta halaman-halaman lainnya dapat
                diakses tanpa membutuhkan pendaftaran dan kata sandi. Akan
                tetapi, untuk pembelian layanan apa pun, Pihak dalam Kontrak
                wajib melakukan PENDAFTARAN dan memasukkan informasi
                pendaftaran.
              </li>
              <li>
                Jankos Glow akan memeriksa data pendaftaran pelanggan secara
                berkala dan mengecualikan pendaftaran yang mengandung informasi
                salah atau tidak benar.
              </li>
              <li>
                Pihak dalam Kontrak, sesuai dengan Syarat dan Ketentuan, setuju
                untuk memberikan informasi yang benar, akurat, terbaru, dan
                lengkap tentang dirinya sendiri sekaligus memelihara dan segera
                memperbarui informasi pendaftaran untuk menjaganya tetap benar,
                akurat, terbaru, dan lengkap.
              </li>
              <li>
                Pihak dalam Kontrak dengan ini menyatakan bahwa semua data yang
                diberikan adalah benar.
              </li>
              <li>
                Pihak dalam Kontrak mengetahui bahwa mengakses SITUS WEB atau
                menggunakan layanannya dengan memberikan alamat email atau nama
                pengguna dan kata sandi secara otomatis menandakan bahwa Pihak
                dalam Kontrak bertanggung jawab sepenuhnya atas tindakannya.
              </li>
              <li>
                Jika informasi apa pun yang diberikan oleh Pihak dalam Kontrak
                tidak benar, tidak akurat, tidak terbaru, atau tidak lengkap,
                atau jika Jankos Glow memiliki alasan dan dasar yang cukup untuk
                mencurigai bahwa informasi tersebut tidak benar, tidak akurat,
                tidak terbaru, atau tidak lengkap, Jankos Glow berhak untuk
                segera menangguhkan atau menghentikan, baik dengan maupun tanpa
                pemberitahuan, permintaan pembelian oleh Pihak dalam Kontrak
                sekaligus penggunaan layanan di masa mendatang.
              </li>
              <li>
                Pihak dalam Kontrak harus menginformasikan alamat elektronik
                (alamat email) yang digunakan di SITUS WEB dan untuk menerima
                pesan-pesan yang timbul akibat pendaftaran di SITUS WEB yang
                disebutkan. Pihak dalam Kontrak akan bertanggung jawab
                sepenuhnya atas aktivitas apa pun yang mungkin terjadi selama
                proses pendaftarannya. Alamat elektronik yang diberikan oleh
                Pihak dalam Kontrak akan digunakan untuk mengidentifikasi Pihak
                dalam Kontrak.
              </li>
              <li>
                Pihak dalam Kontrak setuju untuk segera menginformasikan kepada
                Jankos Glow mengenai penggunaan login apa pun tanpa izin ke
                SITUS WEB (identifikasi akses Pihak dalam Kontrak ke SITUS WEB
                dilakukan dengan mengisi alamat email pribadi atau nama pengguna
                dan kata sandi) atau kebocoran keamanan lainnya yang diketahui.
                Pihak dalam Kontrak juga setuju untuk tidak mengizinkan
                pendaftaran pribadi dilakukan di komputer mana pun setelah masuk
                ke SITUS WEB dengan alamat emailnya, sehingga mencegah
                penggunaan tidak berwenang oleh pihak ketiga.
              </li>
              <li>
                Jankos Glow memiliki kebijakan untuk menghargai privasi Pihak
                dalam Kontrak. Dengan demikian, SITUS WEB tidak akan memantau,
                mengedit, mengakses, atau mengungkap informasi pribadi apa pun
                dari Pihak dalam Kontrak tanpa seizin Pihak dalam Kontrak
                terlebih dahulu, kecuali dalam kasus-kasus yang sebelumnya telah
                dinyatakan dalam Syarat & Ketentuan, atau kecuali Jankos Glow
                diwajibkan oleh pengadilan atau hukum.
              </li>
              <li>
                Pendaftaran Pihak dalam Kontrak pada SITUS WEB bersifat gratis.
              </li>
              <li>
                Pihak dalam Kontrak wajib melakukan pendaftaran sendiri di SITUS
                WEB, sehingga bertanggung jawab secara pribadi atas kebenaran
                dan keakuratan data yang diberikan, dan harus menjaga agar
                lingkungan komputernya tetap aman menggunakan alat-alat yang
                tersedia, seperti antivirus dan firewall yang diperbarui,
                sebagai cara untuk berkontribusi terhadap pencegahan risiko
                elektronik di sisi Pihak dalam Kontrak.
              </li>
            </ul>
          </li>
          <li className="mb-9 font-semibold">
            Tentang Harga dan Pengiriman Produk
            <ul className="list ml-4 mt-2 list-inside list-decimal font-normal">
              <li>
                Produk akan dikirimkan ke alamat yang diberikan oleh Pihak dalam
                Kontrak pada saat mengirimkan permintaan dan akan dikenakan
                biaya pengiriman, bergantung pada informasi di SITUS WEB.
              </li>
              <li>
                Selama konfirmasi pesanan, Pihak dalam Kontrak akan menerima
                informasi tentang total harga produk dan biaya pengiriman, jika
                ada, dan inilah jumlah yang akan dianggap sah untuk kewajiban
                yang dijanjikan dan ditetapkan dalam transaksi pembelian.
              </li>
              <li>
                Selama konfirmasi pesanan, metode pembayaran yang tersedia untuk
                produk-produk yang dibeli oleh Pihak dalam Kontrak akan
                ditampilkan di SITUS WEB.
              </li>
              <li>
                Tenggat waktu pembayaran untuk transfer bank adalah segera dan
                dapat berupa pembayaran langsung atau angsuran (jika berlaku)
                untuk metode pembayaran kartu kredit. Layanan dan pengiriman
                hanya akan dilakukan setelah konfirmasi pembayaran. Jika
                pembayaran tidak dilakukan atau transaksi ditolak karena alasan
                apa pun, pesanan tersebut akan dibatalkan. Jika melakukan
                transfer bank, Pihak dalam Kontrak harus memberikan konfirmasi
                transfer.
              </li>
              <li>
                Tenggat waktu untuk produksi produk yang diminta akan bergantung
                pada konfirmasi pembayaran yang berhasil dan pengiriman berkas
                yang benar oleh Pihak dalam Kontrak. Waktu pengiriman tidak
                termasuk dalam tenggat waktu produksi dan merupakan risiko dari
                Pihak dalam Kontrak.
              </li>
              <li>
                Pengiriman akan dilakukan melalui pos, kargo atau kurir pribadi,
                pada hari kerja dan selama jam kerja, sesuai permintaan Pihak
                dalam Kontrak, dan akan dikirimkan ke Pihak dalam Kontrak atau
                ke orang lain di alamat yang diberikan oleh Pihak dalam Kontrak,
                dengan catatan tidak dibatalkan secara tegas oleh Pihak dalam
                Kontrak. Dengan penandatanganan catatan pengiriman, produk akan
                dianggap dikirim secara sempurna sesuai permintaan. Pihak dalam
                Kontrak harus mengisi data pengiriman dengan tepat untuk
                menghindari penundaan apa pun.
              </li>
            </ul>
          </li>
          <li className="mb-9 font-semibold">
            Hak dan Kewajiban Jankos Glow
            <ul className="list ml-4 mt-2 list-inside list-decimal font-normal">
              <li>
                Jankos Glow berhak untuk, kapan pun, menjalankan promosi
                pemasaran. Promosi akan selalu valid selama periode yang
                ditentukan dan dapat dibatalkan kapan pun sesuai dengan
                kebijakan Jankos Glow.
              </li>
              <li>
                Setelah membayar produk yang dipesan, Pihak dalam Kontrak tidak
                lagi dapat mengundurkan diri dari perjanjian pembelian karena
                produk dibuat khusus sepenuhnya dan dirancang secara khusus
                untuk Pihak dalam Kontrak, sehingga itulah alasan mengapa, jika
                Pihak dalam Kontrak ingin mengundurkan diri dari perjanjian
                pembelian, Pihak dalam Kontrak tidak akan menerima pengembalian
                dana dari jumlah yang sudah dibayarkan. Jika produksi dari
                produk yang dipesan belum dijalankan, voucher dari jumlah yang
                dibayarkan akan dibuat, dengan mengurangkan pengeluaran apa pun
                yang sudah terjadi.
              </li>
              <li>
                Masalah apa pun terkait produk yang diminta akan diselesaikan
                oleh Pihak dalam Kontrak dan Jankos Glow secara langsung. Pihak
                dalam Kontrak mengetahui sepenuhnya bahwa SITUS WEB hanya
                merupakan sarana promosi layanan, di mana informasi untuk
                menjalankan operasi komersial ini ditampilkan.
              </li>
              <li>
                Jankos Glow berhak untuk tidak melakukan layanan apabila berkas
                mengandung konten pornografi, diskriminasi, tidak senonoh, atau
                menyinggung, yang mungkin tidak sesuai dengan moral dan tata
                krama yang baik, sekaligus dapat menyebabkan tuntutan kompensasi
                atau membahayakan pihak ketiga, atau dapat dianggap sebagai
                kejahatan atau tindak pidana ringan.
              </li>
              <li>
                Pihak dalam Kontrak mengetahui dan mengizinkan Jankos Glow untuk
                menyalin berkas yang dikirimkan oleh Pihak dalam Kontrak, jika
                dibutuhkan, untuk menjalankan pekerjaan dan memenuhi kewajiban.
                Jankos Glow akan mengambil langkah-langkah pencegahan normal
                agar berkas digital yang dikirimkan oleh Pihak dalam Kontrak
                tidak diakses atau disebarkan oleh pihak ketiga atau kepada
                pihak ketiga. Akan tetapi, Pihak dalam Kontrak mengetahui bahwa
                saat mengirimkan berkas ini, mungkin terdapat pencegatan oleh
                pihak ketiga atau peretas, di mana Jankos Glow tidak akan
                bertanggung jawab karena ini merupakan situasi yang tidak
                terduga.
              </li>
              <li>
                Kewajiban Jankos Glow terbatas pada penyelesaian layanan,
                produksi dalam tenggat waktu dan ketentuan yang disetujui,
                sekaligus standar kualitas yang disetujui. Jankos Glow tidak
                akan bertanggung jawab atas pengeluaran lain apa pun, baik
                secara langsung maupun tidak langsung, termasuk kerugian dan
                kerusakan, biaya atau kompensasi, dsb.
              </li>
              <li>
                Pihak dalam Kontrak akan bertanggung jawab untuk memastikan
                bahwa berkas-berkas yang diunggah sepenuhnya mematuhi
                spesifikasi teknis yang tertera dalam halaman-halaman SITUS WEB,
                dan Pihak dalam Kontrak bertanggung jawab untuk mempersiapkan
                berkas-berkas dan memverifikasi spesifikasi teknis.
              </li>
              <li>
                Pihak dalam Kontrak mengetahui dan menyetujui bahwa konten
                Jankos Glow, termasuk tetapi tidak terbatas pada teks, aplikasi
                perangkat lunak, musik, suara, fotografi, grafik, video, atau
                materi lain apa pun, mungkin dilindungi oleh merek dagang, hak
                cipta, hak paten, atau hak milik intelektual lainnya.
              </li>
              <li>
                Pihak dalam Kontrak mengetahui dan menyetujui bahwa Pihak dalam
                Kontrak hanya dapat menggunakan materi dan informasi tersebut
                sesuai yang diizinkan secara tegas oleh pemilik yang berwenang
                atau oleh pihak-pihak yang berhak, dan tidak diizinkan untuk
                menyalin, mereproduksi, memindahkan, mendistribusikan, atau
                membuat pekerjaan derivatif berdasarkan materi atau informasi
                tersebut tanpa izin secara tegas dari pemilik yang berwenang.
              </li>
              <li>
                Pihak dalam Kontrak membebaskan Jankos Glow, direktur dan
                karyawannya dari kerugian, pengeluaran, tuntutan, atau keluhan
                apa pun yang diajukan oleh pihak ketiga mana pun atau dialami
                oleh Jankos Glow terkait atau akibat dari penggunaan atau
                pengiriman apa pun yang mungkin terjadi di bawah alamat email
                dan kata sandi Pihak dalam Kontrak (login Pihak dalam Kontrak ke
                SITUS WEB) dan yang melanggar ketentuan layanan yang
                ditawarkan/diminta, hukum atau peraturan daerah, nasional, atau
                internasional yang berlaku, atau hak pihak ketiga mana pun,
                terlepas dari kerusakan apa pun yang muncul.
              </li>
            </ul>
          </li>
          <li className="mb-9 font-semibold">
            Tentang Penghentian
            <ul className="list ml-4 mt-2 list-inside list-decimal font-normal">
              <li>
                Jankos Glow berhak untuk membatalkan akses Pihak dalam Kontrak
                ke layanan yang ditawarkan sepenuhnya atau sebagian, kapan pun,
                dengan atau tanpa sebab, dengan atau tanpa pemberitahuan
                terlebih dahulu, sesuai dengan kebijakan tunggal Jankos Glow.
              </li>
              <li>
                Jika Pihak dalam Kontrak ingin menghentikan pendaftaran, Pihak
                dalam Kontrak hanya perlu mengirimkan email ke alamat email
                layanan pelanggan Jankos Glow.
              </li>
              <li>
                Jankos Glow berhak mengubah atau menghentikan layanan yang
                ditawarkan tanpa memberi tahu Pihak dalam Kontrak.
              </li>
              <li>
                Jankos Glow dapat mengirimkan pemberitahuan promosi dan
                informasi tentang layanan baru, dsb. Komunikasi ini dapat
                dilakukan melalui pesan langsung, kontak telepon, kontak telepon
                seluler, email, SMS, MMS, dan cara lain yang tersedia.
              </li>
              <li>
                Jankos Glow dapat mengubah Syarat & Ketentuan serta spesifikasi
                teknis dari masing-masing produk. Pembelian produk-produk oleh
                Pihak dalam Kontrak setelah perubahan tersebut dilakukan akan
                dianggap sebagai perjanjian sesuai dengan perubahan.
              </li>
            </ul>
          </li>
          <li className="mb-9 font-semibold">
            Tentang Penggunaan dan Tanggung Jawab Atas Data
            <ul className="list ml-4 mt-2 list-inside list-decimal font-normal">
              <li>
                Informasi yang diberikan oleh Pihak dalam Kontrak hanya akan
                digunakan untuk keperluan pendaftaran, hukum, pajak, dan Syarat
                & Ketentuan ini, sehingga Jankos Glow berkomitmen untuk tidak
                menggunakan informasi ini untuk tujuan lain dan juga
                mengungkapnya ke publik, kecuali diwajibkan oleh keputusan
                pengadilan.
              </li>
              <li>
                Akan tetapi, Jankos Glow dapat menggunakan informasi yang
                diberikan oleh Pihak dalam Kontrak untuk keperluan statistik.
              </li>
              <li>
                Apabila terdapat perubahan data pendaftaran apa pun yang
                diberikan oleh Pihak dalam Kontrak, data harus segera diperbarui
                atau Pihak dalam Kontrak akan bertanggung jawab atas kerugian
                atau kerusakan yang mungkin dialami oleh Jankos Glow atau pihak
                ketiga sebagai akibat dari keterlambatan pembaruan data
                pendaftaran, yang dapat mengakibatkan pengiriman yang salah atau
                kegagalan untuk mengirimkan.
              </li>
              <li>
                Apabila terdapat perubahan data pendaftaran apa pun yang
                diberikan oleh Pihak dalam Kontrak, data harus segera diperbarui
                atau Pihak dalam Kontrak akan bertanggung jawab atas kerugian
                atau kerusakan yang mungkin dialami oleh Jankos Glow atau pihak
                ketiga sebagai akibat dari keterlambatan pembaruan data
                pendaftaran, yang dapat mengakibatkan pengiriman yang salah atau
                kegagalan untuk mengirimkan.
              </li>
            </ul>
          </li>
          <li className="mb-9 font-semibold">
            Tentang Ketentuan Umum
            <ul className="list ml-4 mt-2 list-inside list-decimal font-normal">
              <li>
                Jankos Glow hanya memberikan layanan dan produk grafis untuk
                tujuan promosi. Pihak dalam Kontrak menyatakan bahwa produk
                tidak akan dijual ulang di pasar.
              </li>
              <li>
                Saat membeli produk melalui SITUS WEB, Pihak dalam Kontrak harus
                menyatakan bahwa Pihak dalam Kontrak adalah pemilik dari berkas
                dan hak yang terlampir di berkas, dan dengan demikian, secara
                tegas berwenang, dan jika karya harus disahkan, pengesahan
                tersebut telah diperoleh dengan baik, dan bahwa layanan tidak
                akan digunakan untuk mengubah kenyataan sebenarnya dan
                membahayakan orang lain, baik dalam proses pengadilan maupun
                konteks lain apa pun.
              </li>
              <li>
                Jankos Glow menyediakan lingkungan teknologi yang aman.
                Komunikasi antara komputer Pihak dalam Kontrak dan komputer
                Jankos Glow selama proses pendaftaran dan/atau perubahan data
                pendaftaran dan pengiriman kata sandi menggunakan protokol SSL
                (Secure Sockets Layer).
              </li>
              <li>
                Syarat & Ketentuan ini merupakan keseluruhan perjanjian antara
                pihak-pihak yang terlibat dan menggantikan perjanjian lain apa
                pun yang sebelumnya telah ditandatangani.
              </li>
              <li>
                Pihak dalam Kontrak berkomitmen untuk tidak menggunakan
                gambar-gambar dari Pihak dalam Kontrak lainnya, baik melanggar
                sistem keamanan maupun tidak, di bawah sanksi pidana dan perdata
                untuk kerusakan yang ditimbulkan.
              </li>
              <li>
                Semua pemberitahuan ke pihak mana pun yang terlibat harus dalam
                bentuk tertulis, melalui email atau pesan konvensional. Jankos
                Glow berhak untuk mengungkap pemberitahuan atau pesan melalui
                SITUS WEB untuk menginformasikan kepada Pihak dalam Kontrak
                mengenai perubahan layanan atau ketentuan layanan apa pun atau
                hal-hal lain yang relevan.
              </li>
              <li>
                SITUS WEB akan menampilkan layanan yang tersedia, dengan
                menampilkan harga masing-masing, dan Pihak dalam Kontrak akan
                memilih jenis layanan yang diinginkan. Informasi harga dapat
                diganti tanpa pemberitahuan sebelumnya dan akan dianggap sebagai
                informasi yang valid saat permintaan layanan. Untuk alasan ini,
                Pihak dalam Kontrak selalu harus memeriksa jumlah akhir yang
                diperbarui di SITUS WEB, yang akan disertakan pada saat
                melakukan pesanan.
              </li>
              <li>
                SITUS WEB hanya akan menjalankan layanan dari berkas digital,
                sehingga Pihak dalam Kontrak, untuk tujuan ini, harus memiliki
                akses internet dengan risiko sendiri.
              </li>
              <li>
                Jankos Glow berhak untuk mengubah, kapan pun,
                ketentuan-ketentuan yang ada di dalam sini, melalui SITUS WEB.
              </li>
              <li>
                Jankos Glow adalah pemilik dari merek dagang dan logo merek
                dagang yang digunakan dalam SITUS WEB ini, dijamin semua atas
                haknya.
              </li>
              <li>
                Selain itu, Jankos Glow adalah pemegang semua informasi,
                dokumen, gambar, dan materi yang terkandung dalam SITUS WEB.
                Dengan demikian, reproduksi atau penyimpanan materi-materi ini
                oleh Pihak dalam Kontrak akan dikenakan sanksi hukum.
              </li>
              <li>
                Perjanjian ini tidak menandakan bentuk kemasyarakatan, asosiasi,
                kemitraan sebenarnya atau hukum antar pihak maupun menandakan
                adanya solidaritas antar pihak, dan tidak satu pun dari pihak
                tersebut berhak untuk mewakili atau menjanjikan apa pun atas
                nama pihak yang lain.
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <Footer page_contents={global_contents} />
    </>
  )
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const initial_global_contents = await fetcher(
    '/api/page-contents?page.eq=global'
  )

  return {
    props: {
      initial_global_contents,
    },
  }
}
export default TermAndCondition
