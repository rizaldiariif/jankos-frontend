import { NextFetchEvent, NextRequest, NextResponse } from 'next/server'

export async function middleware(req: NextRequest, ev: NextFetchEvent) {
  if (!process.env.NEXT_PUBLIC_APP_COOKIE_NAME_ADMIN) {
    throw new Error('ENV VARIABLE FOR COOKIE NAME IS NOT SET!')
  }

  const jwt = req.cookies[process.env.NEXT_PUBLIC_APP_COOKIE_NAME_ADMIN]

  if (typeof jwt !== 'string' && req.nextUrl.pathname !== '/cms/auth/signin') {
    const url = req.nextUrl.clone()
    url.pathname = '/cms/auth/signin'
    return NextResponse.redirect(url)
  }

  const response = NextResponse.next()

  response.cookie(process.env.NEXT_PUBLIC_APP_COOKIE_NAME_ADMIN, jwt)

  return response
}
