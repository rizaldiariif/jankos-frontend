import type { NextPage } from 'next'
import { useEffect, useMemo, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, { InputItemProps } from '../../../components/shared/BasicForm'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

const ProductCreate: NextPage = () => {
  const [product_categories, setProductCategories] = useState([])

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan Produk...')
    const form_data = new FormData()

    for (const key in data) {
      form_data.append(key, data[key])
    }
    form_data.delete('thumbnail')
    form_data.append('image_thumbnail', data.thumbnail[0])

    try {
      await apiAdmin.post(`/api/products`, form_data)
      toast.success('Sukses Menyimpan Produk!')
      window.location.href = '/cms/product'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  useEffect(() => {
    const fetchProductCategories = async () => {
      try {
        const {
          data: { data },
        } = await apiAdmin.get('/api/product-categories')
        setProductCategories(data)
      } catch (error) {
        console.log(error)
      }
    }
    fetchProductCategories()
  }, [])

  const input_fields = useMemo(
    () =>
      [
        {
          field_name: 'thumbnail',
          label: 'Foto',
          type: 'file',
        },
        {
          field_name: 'name',
          label: 'Nama Produk',
        },
        {
          field_name: 'product_category_id',
          label: 'Kategori Produk',
          option: true,
          options: product_categories.map((pc: any) => {
            return {
              label: pc.name,
              value: pc.id,
            }
          }),
        },
        {
          field_name: 'price',
          label: 'Harga',
          type: 'number',
        },
        {
          field_name: 'weight',
          label: 'Berat (gram)',
          type: 'number',
        },
        {
          field_name: 'width',
          label: 'Lebar (cm)',
          type: 'number',
        },
        {
          field_name: 'length',
          label: 'Panjang (cm)',
          type: 'number',
        },
        {
          field_name: 'height',
          label: 'Tinggi (cm)',
          type: 'number',
        },
        {
          field_name: 'description',
          label: 'Deskripsi',
          wysiwyig: true,
          setValue: setValue,
        },
        {
          field_name: 'short_description',
          label: 'Deskripsi Singkat',
          wysiwyig: true,
          setValue: setValue,
        },
        // {
        //   field_name: 'rating',
        //   label: 'Rating',
        //   type: 'number',
        // },
        {
          field_name: 'is_package',
          label: 'Apakah Produk Paket?',
          placeholder: 'Pilih Salah Satu',
          option: true,
          options: [
            {
              label: 'Ya',
              value: true,
            },
            {
              label: 'Tidak',
              value: false,
            },
          ],
        },
        {
          field_name: 'is_starter',
          label: 'Apakah Produk Starter?',
          placeholder: 'Pilih Salah Satu',
          option: true,
          options: [
            {
              label: 'Ya',
              value: true,
            },
            {
              label: 'Tidak',
              value: false,
            },
          ],
        },
      ] as InputItemProps[],
    [product_categories]
  )

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default ProductCreate
