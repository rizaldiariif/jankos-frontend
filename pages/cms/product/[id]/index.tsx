import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import BasicDetail, {
  ItemDetailProps,
} from '../../../../components/shared/BasicDetail'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const ProductDetail: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [product, setProduct] = useState<any>(null)

  useEffect(() => {
    const fetchProduct = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/products')
      setProduct(data.find((d: any) => d.id == id))
    }
    fetchProduct()
  }, [id])

  const item_details = useMemo(() => {
    const temp_fields = [
      {
        key: 'thumbnail',
        label: 'Foto',
        type: 'image',
      },
      {
        key: 'name',
        label: 'Nama Produk',
        type: 'text',
      },
      {
        key: 'price',
        label: 'Harga',
        type: 'currency',
      },
      {
        key: 'weight',
        label: 'Berat (gram)',
        type: 'text',
      },
      {
        key: 'width',
        label: 'Lebar (cm)',
        type: 'text',
      },
      {
        key: 'length',
        label: 'Panjang (cm)',
        type: 'text',
      },
      {
        key: 'height',
        label: 'Tinggi (cm)',
        type: 'text',
      },
      {
        key: 'description',
        label: 'Deskripsi',
        type: 'wysiwyig',
      },
    ] as ItemDetailProps[]

    return temp_fields
  }, [product])

  if (!product) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicDetail item={product} item_details={item_details} />
      </div>
    </CMSLayout>
  )
}

export default ProductDetail
