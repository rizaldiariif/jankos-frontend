import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useMemo, useEffect, useState } from 'react'
import DataTable from 'react-data-table-component'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, {
  InputItemProps,
} from '../../../../components/shared/BasicForm'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const ProductEdit: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [product, setProduct] = useState<any>(null)

  useEffect(() => {
    const fetchProduct = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/products')
      setProduct(data.find((d: any) => d.id == id))
    }
    fetchProduct()
  }, [id])

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan Produk...')
    const form_data = new FormData()

    for (const key in data) {
      form_data.append(key, data[key])
    }
    form_data.delete('thumbnail')
    if (data.thumbnail && typeof data.thumbnail !== 'string') {
      form_data.append('image_thumbnail', data.thumbnail[0])
    }

    product.product_level_prices.forEach((level_price: any) => {
      form_data.append(
        'product_level_prices',
        `${level_price.id}_${getValues(level_price.id)}`
      )
      form_data.delete(level_price.id)
    })

    try {
      await apiAdmin.post(`/api/products/${id}`, form_data)
      toast.success('Sukses Menyimpan Produk!')
      window.location.href = '/cms/product'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(() => {
    const temp_fields = [
      {
        field_name: 'thumbnail',
        label: 'Foto',
        type: 'file',
        default_value: product?.thumbnail,
        validation: { required: false },
      },
      {
        field_name: 'name',
        label: 'Nama Produk',
        default_value: product?.name,
      },
      {
        field_name: 'price',
        label: 'Harga',
        type: 'number',
        default_value: product?.price,
      },
      {
        field_name: 'weight',
        label: 'Berat (gram)',
        type: 'number',
        default_value: product?.weight,
      },
      {
        field_name: 'width',
        label: 'Lebar (cm)',
        type: 'number',
        default_value: product?.width,
      },
      {
        field_name: 'length',
        label: 'Panjang (cm)',
        type: 'number',
        default_value: product?.length,
      },
      {
        field_name: 'height',
        label: 'Tinggi (cm)',
        type: 'number',
        default_value: product?.height,
      },
      {
        field_name: 'description',
        label: 'Deskripsi',
        default_value: product?.description,
        wysiwyig: true,
        setValue: setValue,
      },
      {
        field_name: 'short_description',
        label: 'Deskripsi Singkat',
        wysiwyig: true,
        default_value: product?.short_description,
        setValue: setValue,
      },
      {
        field_name: 'is_package',
        label: 'Apakah Produk Paket?',
        placeholder: 'Pilih Salah Satu',
        default_value: product?.is_package,
        option: true,
        options: [
          {
            label: 'Ya',
            value: true,
          },
          {
            label: 'Tidak',
            value: false,
          },
        ],
        validation: { required: false },
      },
      {
        field_name: 'is_starter',
        label: 'Apakah Produk Starter?',
        placeholder: 'Pilih Salah Satu',
        default_value: product?.is_starter,
        option: true,
        options: [
          {
            label: 'Ya',
            value: true,
          },
          {
            label: 'Tidak',
            value: false,
          },
        ],
        validation: { required: false },
      },
    ] as InputItemProps[]

    if (product) {
      product.product_level_prices
        .sort((a: any, b: any) => {
          if (a.member_level.minimum_order < b.member_level.minimum_order) {
            return -1
          }
          if (a.member_level.minimum_order > b.member_level.minimum_order) {
            return 1
          }
          return 0
        })
        .map((price: any) => {
          temp_fields.push({
            field_name: price.id,
            label: `Harga ${price.member_level.name}`,
            default_value: price.price,
            type: 'number',
          })
        })
    }
    return temp_fields
  }, [product])

  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Foto',
        cell: (row: any) => (
          <img src={row.url} className="h-16 w-auto rounded border p-1" />
        ),
      },
      // {
      //   name: 'Nama',
      //   selector: (row: any) => row.name,
      // },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            <button
              onClick={onDeleteImage.bind(this, row.id)}
              className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white"
            >
              Hapus
            </button>
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  const uploadFile = async (file: any, product_id: any) => {
    toast.info('Menyimpan Gambar...')
    const form_data = new FormData()

    form_data.append('image', file)
    form_data.append('product_id', product_id)

    try {
      await apiAdmin.post(`/api/product-images`, form_data)
      toast.success('Sukses Menyimpan Gambar!')
      window.location.reload()
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  }

  const onDeleteImage = async (id: string) => {
    toast.info('Menghapus Gambar...')
    try {
      await apiAdmin.delete(`/api/product-images/${id}`)
      toast.success('Sukses Menghapus Gambar!')
      window.location.reload()
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  }

  if (!product) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <button className="relative mb-6 ml-auto block w-min whitespace-nowrap rounded bg-jankos-pink-300 py-2 px-6 text-xs text-white">
          Tambah Gambar Baru
          <input
            type="file"
            className="absolute top-0 left-0 h-full w-full cursor-pointer opacity-0"
            onChange={(e) => {
              if (e.target.files) {
                uploadFile(e.target.files[0], product.id)
              }
            }}
          />
        </button>
        <h1 className="mb-2 text-xl font-bold">Gambar Produk</h1>
        <DataTable columns={columns} data={product.product_images} />
      </div>
    </CMSLayout>
  )
}

export default ProductEdit
