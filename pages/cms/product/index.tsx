import type { NextPage } from 'next'
import Link from 'next/link'
import { useEffect, useMemo, useState } from 'react'
import DataTable, { TableColumn } from 'react-data-table-component'
import { toast } from 'react-toastify'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

const ProductIndex: NextPage = () => {
  const [products, setProducts] = useState<any[]>([])
  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Foto',
        cell: (row: any) => (
          <img src={row.thumbnail} className="h-16 w-auto rounded border p-1" />
        ),
      },
      {
        name: 'Nama Produk',
        selector: (row: any) => row.name,
      },
      {
        name: 'Harga',
        selector: (row: any) => row.price,
        format: (row: any) => `Rp${row.price.toLocaleString()}`,
      },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            <Link href={`/cms/product/${row.id}`}>
              <a className="mr-2 rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Detail
              </a>
            </Link>
            <Link href={`/cms/product/${row.id}/edit`}>
              <a className="mr-2 rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Edit
              </a>
            </Link>
            <button
              onClick={onDelete.bind(this, row.id)}
              className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white"
            >
              Hapus
            </button>
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  useEffect(() => {
    const fetchProducts = async () => {
      const {
        data: { data },
      } = await apiAdmin.get('/api/products')
      setProducts(data)
    }
    fetchProducts()
  }, [])

  const onDelete = async (id: string) => {
    if (window.confirm('Apakah anda yakin?')) {
      toast.info('Deleting...')
      await apiAdmin.delete(`/api/products/${id}`)
      toast.success('Deleted!')
      setTimeout(() => {
        window.location.reload()
      }, 3000)
    }
  }

  return (
    <CMSLayout>
      <Link href="/cms/product/create">
        <a className="mb-6 ml-auto block w-min whitespace-nowrap rounded bg-jankos-pink-300 py-2 px-6 text-xs text-white">
          Tambah Produk Baru
        </a>
      </Link>
      <h1 className="mb-2 text-xl font-bold">Modul Produk</h1>
      <div className="rounded-lg shadow-md">
        <DataTable columns={columns} data={products} />
      </div>
    </CMSLayout>
  )
}

export default ProductIndex
