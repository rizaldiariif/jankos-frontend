import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useMemo, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, {
  InputItemProps,
} from '../../../../components/shared/BasicForm'
import CMSLayout from '../../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../../helper/api-admin'
import { format } from 'date-fns'
import LoadingScreen from '../../../../components/shared/LoadingScreen'

const MemberLevelEdit: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [member_level, setMemberLevel] = useState<any>(null)

  useEffect(() => {
    const fetchMemberLevel = async () => {
      let { data } = await apiAdmin.get(`/api/member-levels/${id}`)
      setMemberLevel(data)
    }
    fetchMemberLevel()
  }, [id])

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Submitting Data...')
    const form_data = {}

    for (const key in data) {
      // @ts-ignore
      form_data[key] = data[key]
    }

    try {
      await apiAdmin.post(`/api/member-levels/${id}`, form_data)
      toast.success('Success!')
      window.location.href = '/cms/member-level'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(() => {
    const temp_fields = [
      {
        field_name: 'name',
        label: 'Nama',
        default_value: member_level?.name,
      },
      {
        field_name: 'minimum_order',
        label: 'Minimal Order',
        default_value: member_level?.minimum_order,
        type: 'number',
      },
      {
        field_name: 'hierarchy_number',
        label: 'Urutan Jenjang',
        default_value: member_level?.hierarchy_number,
        type: 'number',
      },
      {
        field_name: 'commision_percentage',
        label: 'Persentase Komisi',
        default_value: member_level?.commision_percentage,
        type: 'number',
      },
      // {
      //   field_name: 'price_cut_percentage',
      //   label: 'Diskon Harga Barang',
      //   default_value: member_level?.price_cut_percentage,
      //   type: 'number',
      // },
    ] as InputItemProps[]

    return temp_fields
  }, [member_level])

  if (!member_level) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default MemberLevelEdit
