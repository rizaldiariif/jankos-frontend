import type { NextPage } from 'next'
import Link from 'next/link'
import { useEffect, useMemo, useState } from 'react'
import DataTable from 'react-data-table-component'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

const MemberLevelIndex: NextPage = () => {
  const [member_levels, setMemberLevels] = useState<any[]>([])
  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Nama',
        selector: (row: any) => row.name,
      },
      {
        name: 'Minimal Order',
        selector: (row: any) => row.minimum_order,
      },
      {
        name: 'Urutan Jenjang',
        selector: (row: any) => row.hierarchy_number,
      },
      {
        name: 'Persentase Komisi',
        selector: (row: any) => row.commision_percentage,
      },
      // {
      //   name: 'Diskon Harga Barang',
      //   selector: (row: any) => row.price_cut_percentage,
      // },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            {/* <Link href={`/cms/member-level/${row.id}`}>
              <a className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Detail
              </a>
            </Link> */}
            <Link href={`/cms/member-level/${row.id}/edit`}>
              <a className="ml-2 rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Edit
              </a>
            </Link>
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  useEffect(() => {
    const fetchMembers = async () => {
      const {
        data: { data },
      } = await apiAdmin.get('/api/member-levels')
      setMemberLevels(data)
    }
    fetchMembers()
  }, [])

  return (
    <CMSLayout>
      <h1 className="mb-2 text-xl font-bold">Modul Member Level</h1>
      <div className="rounded-lg shadow-md">
        <DataTable columns={columns} data={member_levels} />
      </div>
    </CMSLayout>
  )
}

export default MemberLevelIndex
