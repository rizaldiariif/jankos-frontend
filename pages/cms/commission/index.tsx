import { format } from 'date-fns'
import Link from 'next/link'
import React, { useEffect, useMemo, useState } from 'react'
import DataTable from 'react-data-table-component'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

type Props = {}

const CommissionIndex = (props: Props) => {
  const [referral_ledgers, setReferralLedgers] = useState<any[]>([])
  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Waktu',
        selector: (row: any) => row.createdAt,
        format: (row: any) => format(new Date(row.createdAt), 'dd-MM-yyyy'),
      },
      {
        name: 'Upline Member',
        selector: (row: any) => row.member_referral.upline_member.name,
      },
      {
        name: 'Referred Member',
        selector: (row: any) => row.member_referral.referred_member.name,
      },
      {
        name: 'Jumlah',
        selector: (row: any) => row.amount,
        format: (row: any) => `Rp${row.amount.toLocaleString()}`,
      },
      // {
      //   name: 'Action',
      //   cell: (row: any) => (
      //     <div className="flex items-center">
      //       <Link href={`/cms/commission/${row.id}`}>
      //         <a className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
      //           Detail
      //         </a>
      //       </Link>
      //     </div>
      //   ),
      //   center: true,
      // },
    ],
    []
  )

  useEffect(() => {
    const fetchMembers = async () => {
      const {
        data: { data },
      } = await apiAdmin.get('/api/member-referral-ledgers')
      setReferralLedgers(data)
    }
    fetchMembers()
  }, [])

  return (
    <CMSLayout>
      <h1 className="mb-2 text-xl font-bold">Modul Komisi Member</h1>
      <div className="rounded-lg shadow-md">
        <DataTable columns={columns} data={referral_ledgers} />
      </div>
    </CMSLayout>
  )
}

export default CommissionIndex
