import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useMemo, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, {
  InputItemProps,
} from '../../../../components/shared/BasicForm'
import CMSLayout from '../../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../../helper/api-admin'
import { format } from 'date-fns'
import LoadingScreen from '../../../../components/shared/LoadingScreen'

const TestimonyEdit: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [testimony, setTestimony] = useState<any>(null)

  useEffect(() => {
    const fetchTestimony = async () => {
      let {
        data: { data },
      } = await apiAdmin.get(`/api/testimonies?id.eq=${id}`)
      setTestimony(data.find((d: any) => d.id == id))
    }
    fetchTestimony()
  }, [id])

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Submitting Data...')
    const form_data = new FormData()

    for (const key in data) {
      form_data.append(key, data[key])
    }
    form_data.delete('thumbnail')
    if (data.thumbnail && typeof data.thumbnail !== 'string') {
      form_data.append('image_thumbnail', data.thumbnail[0])
    }

    try {
      await apiAdmin.post(`/api/testimonies/${id}`, form_data)
      toast.success('Success!')
      window.location.href = '/cms/testimony'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(() => {
    const temp_fields = [
      {
        field_name: 'thumbnail',
        label: 'Foto',
        type: 'file',
        default_value: testimony?.thumbnail,
        validation: { required: false },
      },
      {
        field_name: 'name',
        label: 'Nama',
        default_value: testimony?.name,
      },
      {
        field_name: 'text',
        label: 'Teks',
        textarea: true,
        default_value: testimony?.text,
      },
    ] as InputItemProps[]

    return temp_fields
  }, [testimony])

  if (!testimony) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default TestimonyEdit
