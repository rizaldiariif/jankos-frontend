import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import BasicDetail, {
  ItemDetailProps,
} from '../../../../components/shared/BasicDetail'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const TestimonyDetail: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [testimony, setTestimony] = useState<any>(null)

  useEffect(() => {
    const fetchTestimony = async () => {
      let {
        data: { data },
      } = await apiAdmin.get(`/api/testimonies?id.eq=${id}`)
      setTestimony(data.find((d: any) => d.id == id))
    }
    fetchTestimony()
  }, [id])

  const item_details = useMemo(() => {
    const temp_fields = [
      {
        key: 'thumbnail',
        label: 'Foto',
        type: 'image',
      },
      {
        key: 'name',
        label: 'Nama',
        type: 'text',
      },
      {
        key: 'text',
        label: 'Teks',
        type: 'text',
      },
    ] as ItemDetailProps[]

    return temp_fields
  }, [testimony])

  if (!testimony) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicDetail item={testimony} item_details={item_details} />
      </div>
    </CMSLayout>
  )
}

export default TestimonyDetail
