import type { NextPage } from 'next'
import { useMemo } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, { InputItemProps } from '../../../components/shared/BasicForm'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

const TestimonyCreate: NextPage = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan Data...')
    const form_data = new FormData()

    for (const key in data) {
      form_data.append(key, data[key])
    }
    form_data.delete('thumbnail')
    form_data.append('image_thumbnail', data.thumbnail[0])

    try {
      await apiAdmin.post(`/api/testimonies`, form_data)
      toast.success('Sukses Menyimpan Data!')
      window.location.href = '/cms/testimony'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(
    () =>
      [
        {
          field_name: 'thumbnail',
          label: 'Foto',
          type: 'file',
        },
        {
          field_name: 'name',
          label: 'Nama',
        },
        {
          field_name: 'text',
          label: 'Teks',
          textarea: true,
        },
      ] as InputItemProps[],
    []
  )

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default TestimonyCreate
