import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import BasicDetail, {
  ItemDetailProps,
} from '../../../../components/shared/BasicDetail'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const ArticleDetail: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [article, setArticle] = useState<any>(null)

  useEffect(() => {
    const fetchArticle = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/articles')
      setArticle(data.find((d: any) => d.id == id))
    }
    fetchArticle()
  }, [id])

  const item_details = useMemo(() => {
    const temp_fields = [
      {
        key: 'thumbnail',
        label: 'Foto',
        type: 'image',
      },
      {
        key: 'title',
        label: 'Judul',
        type: 'text',
      },
      {
        key: 'author',
        label: 'Penulis',
        type: 'text',
      },
      {
        key: 'text',
        label: 'Teks',
        type: 'wysiwyig',
      },
    ] as ItemDetailProps[]

    return temp_fields
  }, [article])

  if (!article) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicDetail item={article} item_details={item_details} />
      </div>
    </CMSLayout>
  )
}

export default ArticleDetail
