import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useMemo, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, {
  InputItemProps,
} from '../../../../components/shared/BasicForm'
import CMSLayout from '../../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../../helper/api-admin'
import { format } from 'date-fns'
import LoadingScreen from '../../../../components/shared/LoadingScreen'

const ArticleEdit: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [article, setArticle] = useState<any>(null)

  useEffect(() => {
    const fetchArticle = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/articles')
      setArticle(data.find((d: any) => d.id == id))
    }
    fetchArticle()
  }, [id])

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan artikel...')
    const form_data = new FormData()

    for (const key in data) {
      form_data.append(key, data[key])
    }
    form_data.delete('thumbnail')
    if (data.thumbnail && typeof data.thumbnail !== 'string') {
      form_data.append('image_thumbnail', data.thumbnail[0])
    }

    try {
      await apiAdmin.post(`/api/articles/${id}`, form_data)
      toast.success('Sukses menyimpan artikel!')
      window.location.href = '/cms/article'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(() => {
    const temp_fields = [
      {
        field_name: 'thumbnail',
        label: 'Foto',
        type: 'file',
        default_value: article?.thumbnail,
        validation: { required: false },
      },
      {
        field_name: 'title',
        label: 'Judul',
        default_value: article?.title,
      },
      {
        field_name: 'author',
        label: 'Penulis',
        default_value: article?.author,
      },
      {
        field_name: 'text',
        label: 'Teks',
        type: 'text',
        default_value: article?.text,
        wysiwyig: true,
        setValue: setValue,
      },
    ] as InputItemProps[]

    return temp_fields
  }, [article])

  if (!article) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default ArticleEdit
