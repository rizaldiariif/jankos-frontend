import type { NextPage } from 'next'
import Link from 'next/link'
import { useEffect, useMemo, useState } from 'react'
import DataTable, { TableColumn } from 'react-data-table-component'
import { toast } from 'react-toastify'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

const ArticleIndex: NextPage = () => {
  const [articles, setArticles] = useState<any[]>([])
  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Foto',
        cell: (row: any) => (
          <img src={row.thumbnail} className="h-16 w-auto rounded border p-1" />
        ),
      },
      {
        name: 'Judul',
        selector: (row: any) => row.title,
      },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            <Link href={`/cms/article/${row.id}`}>
              <a className="mr-2 rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Detail
              </a>
            </Link>
            <Link href={`/cms/article/${row.id}/edit`}>
              <a className="mr-2 rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Edit
              </a>
            </Link>
            <button
              onClick={onDelete.bind(this, row.id)}
              className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white"
            >
              Hapus
            </button>
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  useEffect(() => {
    const fetchArticles = async () => {
      const {
        data: { data },
      } = await apiAdmin.get('/api/articles')
      setArticles(data)
    }
    fetchArticles()
  }, [])

  const onDelete = async (id: string) => {
    if (window.confirm('Apakah anda yakin?')) {
      toast.info('Deleting...')
      await apiAdmin.delete(`/api/articles/${id}`)
      toast.success('Deleted!')
      setTimeout(() => {
        window.location.reload()
      }, 3000)
    }
  }

  return (
    <CMSLayout>
      <Link href="/cms/article/create">
        <a className="mb-6 ml-auto block w-min whitespace-nowrap rounded bg-jankos-pink-300 py-2 px-6 text-xs text-white">
          Tambah Artikel Baru
        </a>
      </Link>
      <h1 className="mb-2 text-xl font-bold">Modul Artikel</h1>
      <div className="rounded-lg shadow-md">
        <DataTable columns={columns} data={articles} />
      </div>
    </CMSLayout>
  )
}

export default ArticleIndex
