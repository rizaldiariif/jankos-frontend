import type { NextPage } from 'next'
import { useMemo } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, { InputItemProps } from '../../../components/shared/BasicForm'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

const ArticleCreate: NextPage = () => {
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan artikel...')
    const form_data = new FormData()

    for (const key in data) {
      form_data.append(key, data[key])
    }
    form_data.delete('thumbnail')
    form_data.append('image_thumbnail', data.thumbnail[0])

    try {
      await apiAdmin.post(`/api/articles`, form_data)
      toast.success('Sukses menyimpan artikel!')
      window.location.href = '/cms/article'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(
    () =>
      [
        {
          field_name: 'thumbnail',
          label: 'Foto',
          type: 'file',
        },
        {
          field_name: 'title',
          label: 'Judul',
        },
        {
          field_name: 'author',
          label: 'Penulis',
        },
        {
          field_name: 'text',
          label: 'Teks',
          wysiwyig: true,
          setValue: setValue,
        },
      ] as InputItemProps[],
    []
  )

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default ArticleCreate
