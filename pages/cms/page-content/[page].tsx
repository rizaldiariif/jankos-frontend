import { useRouter } from 'next/router'
import React, { useEffect, useMemo, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, { InputItemProps } from '../../../components/shared/BasicForm'
import CMSLayout from '../../../components/shared/CMSLayout'
import LoadingScreen from '../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../helper/api-admin'

type Props = {}

const PageContentEdit = (props: Props) => {
  const {
    query: { page },
  } = useRouter()
  const [page_contents, setPageContents] = useState<any[]>([])

  useEffect(() => {
    const fetchPageContents = async () => {
      let {
        data: { data },
      } = await apiAdmin.get(`/api/page-contents?page.eq=${page}&limit=100`)
      setPageContents(data)
    }
    fetchPageContents()
  }, [page])

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Submitting Data...')
    try {
      for (const [id, value] of Object.entries(data)) {
        const current_page_content = page_contents.find(
          (page_content: any) => page_content.id === id
        )

        const form_data = new FormData()

        console.log({ value })

        if (current_page_content.type !== 'file') {
          form_data.append('content', value)
        } else {
          if (value) {
            form_data.append('content', ' ')
            form_data.append('content_file', value[0])
          }
        }

        if (current_page_content.type !== 'file') {
          await apiAdmin.post(`/api/page-contents/${id}`, form_data)
        } else {
          if (value) {
            await apiAdmin.post(`/api/page-contents/${id}`, form_data)
          }
        }
      }
      toast.success('Success!')
      window.location.href = '/cms/page-content'
    } catch (error) {
      toast.error('Error Happened!')
      console.log(error)
    }

    // for (const key in data) {
    //   form_data.append(key, data[key])
    // }
    // form_data.delete('thumbnail')
    // if (data.thumbnail && typeof data.thumbnail !== 'string') {
    //   form_data.append('image_thumbnail', data.thumbnail[0])
    // }

    // try {
    //   await apiAdmin.post(`/api/testimonies/${id}`, form_data)
    //   toast.success('Success!')
    //   window.location.href = '/cms/testimony'
    // } catch (error) {
    //   toast.error('Error!')
    //   console.log(error)
    // }
  })

  const input_fields = useMemo(() => {
    const temp_fields = page_contents.map((page_content: any) => {
      const input_props = {
        field_name: page_content.id,
        label: page_content.label,
        default_value: page_content.content,
        type: page_content.type,
        setValue: setValue,
      } as InputItemProps

      if (page_content.type === 'textarea') {
        input_props.textarea = true
      }

      if (page_content.type === 'file') {
        input_props.default_value = undefined
        input_props.validation = { required: false }
      }

      if (page_content.type === 'wysiwyig') {
        input_props.wysiwyig = true
      }

      return input_props
    }) as InputItemProps[]

    return temp_fields
  }, [page_contents])

  if (!page_contents) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default PageContentEdit
