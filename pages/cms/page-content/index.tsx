import Link from 'next/link'
import React, { useMemo } from 'react'
import DataTable from 'react-data-table-component'
import CMSLayout from '../../../components/shared/CMSLayout'

type Props = {}

const PageContentIndex = (props: Props) => {
  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Halaman',
        selector: (row: any) => row.label,
      },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            <Link href={`/cms/page-content/${row.page}`}>
              <a className="mr-2 rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Edit
              </a>
            </Link>
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  const pages = useMemo(
    () => [
      {
        page: 'home',
        label: 'Homepage',
      },
      {
        page: 'blog',
        label: 'Blog',
      },
      {
        page: 'global',
        label: 'Global',
      },
    ],
    []
  )

  return (
    <CMSLayout>
      <h1 className="mb-2 text-xl font-bold">Modul Konten Halaman</h1>
      <div className="rounded-lg shadow-md">
        <DataTable columns={columns} data={pages} />
      </div>
    </CMSLayout>
  )
}

export default PageContentIndex
