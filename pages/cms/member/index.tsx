import type { NextPage } from 'next'
import Link from 'next/link'
import { useEffect, useMemo, useState } from 'react'
import DataTable from 'react-data-table-component'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

const MemberIndex: NextPage = () => {
  const [members, setMembers] = useState<any[]>([])
  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Email',
        selector: (row: any) => row.email,
      },
      {
        name: 'Nama',
        selector: (row: any) => row.name,
      },
      {
        name: 'Jenjang',
        selector: (row: any) => row.member_level.name,
      },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            <Link href={`/cms/member/${row.id}`}>
              <a className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Detail
              </a>
            </Link>
            {/* <Link href={`/cms/member/${row.id}/edit`}>
              <a className="mr-2 rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Edit
              </a>
            </Link> */}
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  useEffect(() => {
    const fetchMembers = async () => {
      const {
        data: { data },
      } = await apiAdmin.get('/api/members')
      setMembers(data)
    }
    fetchMembers()
  }, [])

  return (
    <CMSLayout>
      <h1 className="mb-2 text-xl font-bold">Modul Member</h1>
      <div className="rounded-lg shadow-md">
        <DataTable columns={columns} data={members} />
      </div>
    </CMSLayout>
  )
}

export default MemberIndex
