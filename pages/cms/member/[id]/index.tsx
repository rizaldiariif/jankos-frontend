import { format } from 'date-fns'
import type { NextPage } from 'next'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import DataTable from 'react-data-table-component'
import shortUUID from 'short-uuid'
import BasicDetail, {
  ItemDetailProps,
} from '../../../../components/shared/BasicDetail'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const MemberDetail: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [member, setMember] = useState<any>(null)

  useEffect(() => {
    const fetchMember = async () => {
      if (id) {
        let { data } = await apiAdmin.get(`/api/members/${id}`)
        setMember(data)
      }
    }
    fetchMember()
  }, [id])

  const item_details = useMemo(() => {
    const temp_fields = [
      // {
      //   key: 'id',
      //   label: 'ID',
      //   type: 'text',
      // },
      {
        key: 'name',
        label: 'Nama',
        type: 'text',
      },
      {
        key: 'email',
        label: 'Email',
        type: 'text',
      },
      {
        key: 'phone',
        label: 'No HP',
        type: 'text',
      },
      {
        key: 'marketplace_url',
        label: 'URL Marketplace',
        type: 'url',
      },
      {
        key: 'social_media_url',
        label: 'URL Sosial Media',
        type: 'url',
      },
      // {
      //   key: 'referral_code',
      //   label: 'Kode Referral',
      //   type: 'text',
      // },
      {
        key: 'image_id_card',
        label: 'Foto ID Card',
        type: 'image',
      },
    ] as ItemDetailProps[]

    return temp_fields
  }, [member])

  const order_columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'ID',
        selector: (row: any) => row.id,
      },
      {
        name: 'Nama Penerima',
        selector: (row: any) => row.contact_name,
      },
      {
        name: 'Status',
        selector: (row: any) => row.order_status.name,
      },
      {
        name: 'Terakhir Update',
        selector: (row: any) => row.updatedAt,
        format: (row: any) =>
          format(new Date(row.updatedAt), 'dd-MM-yyyy kk:mm:ss'),
      },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            <Link href={`/cms/order/${row.id}`}>
              <a className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Lihat Pesanan
              </a>
            </Link>
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  const member_level_history_columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Nama',
        selector: (row: any) => row.member_level.name,
      },
      {
        name: 'Waktu',
        selector: (row: any) => row.createdAt,
        format: (row: any) =>
          format(new Date(row.createdAt), 'dd-MM-yyyy kk:mm:ss'),
      },
    ],
    []
  )

  const member_shipping_address_columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Nama Alamat',
        selector: (row: any) => row.address_name,
      },
      {
        name: 'Nama Penerima',
        selector: (row: any) => row.contact_name,
      },
      {
        name: 'No HP',
        selector: (row: any) => row.contact_phone,
      },
      {
        name: 'Alamat',
        selector: (row: any) => row.contact_address,
      },
      {
        name: 'Kecamatan',
        selector: (row: any) => row.subdistrict_name,
      },
      {
        name: 'Kota',
        selector: (row: any) => row.city_name,
      },
      {
        name: 'Provinsi',
        selector: (row: any) => row.province_name,
      },
    ],
    []
  )

  const member_bank_account_columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Nama Bank',
        selector: (row: any) => row.bank_name,
      },
      {
        name: 'Nama Akun',
        selector: (row: any) => row.bank_account_name,
      },
      {
        name: 'Nomor Akun',
        selector: (row: any) => row.bank_account_number,
      },
    ],
    []
  )

  // const member_referral_columns = useMemo(
  //   () => [
  //     {
  //       name: 'No',
  //       selector: (_row: any, index: any) => (index ? index + 1 : 1),
  //     },
  //     {
  //       name: 'Upline Member',
  //       selector: (row: any) => row.upline_member.email,
  //     },
  //     {
  //       name: 'Referred Member',
  //       selector: (row: any) => row.referred_member.email,
  //     },
  //     {
  //       name: 'Waktu',
  //       selector: (row: any) => row.createdAt,
  //       format: (row: any) =>
  //         format(new Date(row.createdAt), 'dd-MM-yyyy kk:mm:ss'),
  //     },
  //   ],
  //   []
  // )

  // const member_point_ledger_columns = useMemo(
  //   () => [
  //     {
  //       name: 'No',
  //       selector: (_row: any, index: any) => (index ? index + 1 : 1),
  //     },
  //     {
  //       name: 'Jumlah',
  //       selector: (row: any) => row.amount,
  //       format: (row: any) => `Rp${row.amount.toLocaleString()}`,
  //     },
  //     {
  //       name: 'Waktu',
  //       selector: (row: any) => row.createdAt,
  //       format: (row: any) =>
  //         format(new Date(row.createdAt), 'dd-MM-yyyy kk:mm:ss'),
  //     },
  //     {
  //       name: 'Action',
  //       cell: (row: any) => (
  //         <div className="flex items-center">
  //           <Link href={`/cms/order/${row.order_id}`}>
  //             <a className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
  //               Lihat Pesanan
  //             </a>
  //           </Link>
  //         </div>
  //       ),
  //       center: true,
  //     },
  //   ],
  //   []
  // )

  if (!member) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Data Member</h3>
        <p className="mt-3 mb-3 font-semibold text-jankos-pink-300">
          {shortUUID().fromUUID(member.id).toUpperCase()}
        </p>
        <BasicDetail item={member} item_details={item_details} />
      </div>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Alamat Pengiriman</h3>
        <DataTable
          data={member.member_shipping_addresses}
          columns={member_shipping_address_columns}
        />
      </div>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Akun Bank</h3>
        <DataTable
          data={[member.member_bank_account]}
          columns={member_bank_account_columns}
        />
      </div>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Pesanan</h3>
        <DataTable data={member.orders} columns={order_columns} />
      </div>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Histori Jenjang</h3>
        <DataTable
          data={member.member_level_histories}
          columns={member_level_history_columns}
        />
      </div>
      {/* <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Referral Member</h3>
        <DataTable
          data={member.member_referrals}
          columns={member_referral_columns}
        />
      </div> */}
      {/* <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Poin Member</h3>
        <DataTable
          data={member.member_point_ledgers}
          columns={member_point_ledger_columns}
        />
      </div> */}
    </CMSLayout>
  )
}

export default MemberDetail
