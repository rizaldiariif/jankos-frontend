import { setCookies } from 'cookies-next'
import type { NextPage } from 'next'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import AuthHeader from '../../../components/shared/AuthHeader'
import { apiAdmin } from '../../../helper/api-admin'
import { useCurrentAdmin } from '../../../helper/useCurrentAdmin'

const SignInAdmin: NextPage = () => {
  const { admin } = useCurrentAdmin()
  const { push } = useRouter()

  const { register, handleSubmit } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Loading...')

    if (!process.env.NEXT_PUBLIC_APP_COOKIE_NAME_ADMIN) {
      toast.error('ENV VARIABLE FOR COOKIE NAME ADMIN IS NOT SET!')
      return
    }

    try {
      const response = await apiAdmin.post(
        `${process.env.NEXT_PUBLIC_BACKEND_URL}/api/admins/auth/signin`,
        data
      )

      setCookies(
        process.env.NEXT_PUBLIC_APP_COOKIE_NAME_ADMIN,
        response.data.jwt
      )
      toast.success('Login Success!')
      window.location.href = '/cms/order'
    } catch (error) {
      toast.error('Login Error!')
      console.log(error)
    }
  })

  if (admin) {
    push('/cms/order')
  }

  return (
    <div className="relative flex h-screen w-screen flex-col">
      <AuthHeader show_login={false} />
      <div className="relative flex flex-grow items-center justify-center bg-red-100">
        <div className="relative z-10 h-auto bg-white p-16 lg:w-1/3">
          <p className="mb-8 text-center text-sm">CMS LOGIN</p>
          <form onSubmit={onSubmit}>
            <div className="mb-5 grid lg:grid-cols-4">
              <div className="flex items-center">
                <label className="text-sm font-semibold">Email</label>
              </div>
              <input
                type="email"
                className="rounded border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-3"
                {...register('email')}
              />
            </div>
            <div className="mb-2 grid lg:grid-cols-4">
              <div className="flex items-center">
                <label className="text-sm font-semibold">Password</label>
              </div>
              <input
                type="password"
                className="rounded border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-3"
                {...register('password')}
              />
            </div>
            <div className="mb-8 text-center">
              <input
                type="submit"
                value="Login"
                className="rounded-lg bg-jankos-pink-300 py-3 px-14 text-sm font-semibold text-white"
              />
            </div>
          </form>
        </div>
        <Image src="/images/bg-auth.png" layout="fill" objectFit="cover" />
      </div>
    </div>
  )
}

export default SignInAdmin
