import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useMemo, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, {
  InputItemProps,
} from '../../../../components/shared/BasicForm'
import CMSLayout from '../../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../../helper/api-admin'
import { format } from 'date-fns'
import LoadingScreen from '../../../../components/shared/LoadingScreen'

const MarketingMaterialEdit: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [marketing_material, setMarketingMaterial] = useState<any>(null)

  useEffect(() => {
    const fetchMarketingMaterial = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/marketing-materials')
      setMarketingMaterial(data.find((d: any) => d.id == id))
    }
    fetchMarketingMaterial()
  }, [id])

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Submitting Data...')
    const form_data = new FormData()

    for (const key in data) {
      form_data.append(key, data[key])
    }
    form_data.delete('thumbnail')
    if (data.thumbnail && typeof data.thumbnail !== 'string') {
      form_data.append('image_thumbnail', data.thumbnail[0])
    }

    try {
      await apiAdmin.post(`/api/marketing-materials/${id}`, form_data)
      toast.success('Success!')
      window.location.href = '/cms/marketing-material'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(() => {
    const temp_fields = [
      {
        field_name: 'thumbnail',
        label: 'Foto',
        type: 'file',
        default_value: marketing_material?.thumbnail,
        validation: { required: false },
      },
      {
        field_name: 'title',
        label: 'Judul',
        default_value: marketing_material?.title,
      },
      {
        field_name: 'text',
        label: 'Teks',
        textarea: true,
        default_value: marketing_material?.text,
      },
      {
        field_name: 'suggestion_date',
        label: 'Saran Tanggal Terbit',
        type: 'date',
        default_value:
          marketing_material?.suggestion_date &&
          format(new Date(marketing_material?.suggestion_date), 'yyyy-MM-dd'),
      },
      {
        field_name: 'code',
        label: 'Kategori',
        default_value: marketing_material?.code,
        option: true,
        options: [
          { value: 'product', label: 'Produk' },
          { value: 'promotion', label: 'Promosi' },
        ],
      },
    ] as InputItemProps[]

    return temp_fields
  }, [marketing_material])

  if (!marketing_material) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default MarketingMaterialEdit
