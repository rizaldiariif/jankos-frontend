import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import BasicDetail, {
  ItemDetailProps,
} from '../../../../components/shared/BasicDetail'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const MarketingMaterialDetail: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [marketing_material, setMarketingMaterial] = useState<any>(null)

  useEffect(() => {
    const fetchMarketingMaterial = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/marketing-materials')
      setMarketingMaterial(data.find((d: any) => d.id == id))
    }
    fetchMarketingMaterial()
  }, [id])

  const item_details = useMemo(() => {
    const temp_fields = [
      {
        key: 'thumbnail',
        label: 'Foto',
        type: 'image',
      },
      {
        key: 'title',
        label: 'Judul',
        type: 'text',
      },
      {
        key: 'code',
        label: 'Kategori',
        type: 'text',
      },
      {
        key: 'suggestion_date',
        label: 'Saran Tanggal Terbit',
        type: 'date',
      },
      {
        key: 'text',
        label: 'Teks',
        type: 'text',
      },
    ] as ItemDetailProps[]

    return temp_fields
  }, [marketing_material])

  if (!marketing_material) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicDetail item={marketing_material} item_details={item_details} />
      </div>
    </CMSLayout>
  )
}

export default MarketingMaterialDetail
