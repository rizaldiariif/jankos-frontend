import type { NextPage } from 'next'
import { useMemo, useEffect, useState } from 'react'
import DataTable from 'react-data-table-component'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import CMSLayout from '../../../components/shared/CMSLayout'
import LoadingScreen from '../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../helper/api-admin'

const ProductEdit: NextPage = () => {
  const [banner_homes, setBannerHomes] = useState<any>(null)

  useEffect(() => {
    const fetchBannerHome = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/banner-homes')
      setBannerHomes(data)
    }
    fetchBannerHome()
  }, [])

  const {
    formState: { errors },
  } = useForm()

  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Foto',
        cell: (row: any) => (
          <img src={row.thumbnail} className="h-16 w-auto rounded border p-1" />
        ),
      },
      // {
      //   name: 'Nama',
      //   selector: (row: any) => row.name,
      // },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            <button
              onClick={onDeleteImage.bind(this, row.id)}
              className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white"
            >
              Hapus
            </button>
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  const uploadFile = async (file: any) => {
    toast.info('Menyimpan Gambar...')
    const form_data = new FormData()

    form_data.append('image_thumbnail', file)

    try {
      await apiAdmin.post(`/api/banner-homes`, form_data)
      toast.success('Sukses Menyimpan Gambar!')
      window.location.reload()
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  }

  const onDeleteImage = async (id: string) => {
    toast.info('Menghapus Gambar...')
    try {
      await apiAdmin.delete(`/api/banner-homes/${id}`)
      toast.success('Sukses Menghapus Gambar!')
      window.location.reload()
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  }

  if (!banner_homes) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <button className="relative mb-6 ml-auto block w-min whitespace-nowrap rounded bg-jankos-pink-300 py-2 px-6 text-xs text-white">
          Tambah Gambar Baru
          <input
            type="file"
            className="absolute top-0 left-0 h-full w-full cursor-pointer opacity-0"
            onChange={(e) => {
              if (e.target.files) {
                uploadFile(e.target.files[0])
              }
            }}
          />
        </button>
        <h1 className="mb-2 text-xl font-bold">Banner</h1>
        <DataTable columns={columns} data={banner_homes} />
      </div>
    </CMSLayout>
  )
}

export default ProductEdit
