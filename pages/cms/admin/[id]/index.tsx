import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import BasicDetail, {
  ItemDetailProps,
} from '../../../../components/shared/BasicDetail'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const AdminDetail: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [admin, setAdmin] = useState<any>(null)

  useEffect(() => {
    const fetchAdmin = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/admins')
      setAdmin(data.find((d: any) => d.id == id))
    }
    fetchAdmin()
  }, [id])

  const item_details = useMemo(() => {
    const temp_fields = [
      {
        key: 'name',
        label: 'Nama',
        type: 'text',
      },
      {
        key: 'email',
        label: 'Email',
        type: 'text',
      },
    ] as ItemDetailProps[]

    return temp_fields
  }, [admin])

  if (!admin) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Data Admin</h3>
        <BasicDetail item={admin} item_details={item_details} />
      </div>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Jabatan</h3>
        <BasicDetail
          item={admin.admin_level}
          item_details={[{ key: 'name', label: 'Nama Jabatan', type: 'text' }]}
        />
      </div>
    </CMSLayout>
  )
}

export default AdminDetail
