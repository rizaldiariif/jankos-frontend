import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useMemo, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, {
  InputItemProps,
} from '../../../../components/shared/BasicForm'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const AdminEdit: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [admin, setAdmin] = useState<any>(null)
  const [admin_levels, setAdminLevels] = useState<any[]>([])

  useEffect(() => {
    const fetchAdmin = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/admins')
      setAdmin(data.find((d: any) => d.id == id))
    }
    fetchAdmin()

    const fetchAdminLevels = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/admin-levels')
      setAdminLevels(data)
    }
    fetchAdminLevels()
  }, [id])

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan data...')
    const form_data = {} as any

    for (const key in data) {
      form_data[key] = data[key]
    }

    if (!form_data.password) {
      delete form_data.password
    }

    try {
      await apiAdmin.post(`/api/admins/${id}`, form_data)
      toast.success('Sukses menyimpan data!')
      window.location.href = '/cms/admin'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(() => {
    const temp_fields = [
      {
        field_name: 'name',
        label: 'Nama',
        default_value: admin?.name,
      },
      {
        field_name: 'email',
        label: 'Email',
        default_value: admin?.email,
      },
      {
        field_name: 'password',
        label: 'Password',
        default_value: admin?.password,
        validation: { required: false },
      },
      {
        field_name: 'admin_level_id',
        label: 'Jabatan',
        default_value: admin?.admin_level_id,
        option: true,
        options: admin_levels.map((al) => {
          return {
            value: al.id,
            label: al.name,
          }
        }),
      },
    ] as InputItemProps[]

    return temp_fields
  }, [admin, admin_levels])

  if (!admin) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default AdminEdit
