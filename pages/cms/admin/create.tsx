import type { NextPage } from 'next'
import { useEffect, useMemo, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, { InputItemProps } from '../../../components/shared/BasicForm'
import CMSLayout from '../../../components/shared/CMSLayout'
import LoadingScreen from '../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../helper/api-admin'

const AdminCreate: NextPage = () => {
  const [admin_levels, setAdminLevels] = useState<any[]>([])

  useEffect(() => {
    const fetchAdminLevels = async () => {
      const {
        data: { data },
      } = await apiAdmin.get('/api/admin-levels')
      setAdminLevels(data)
    }
    fetchAdminLevels()
  }, [])

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan data...')
    const form_data = {} as any

    for (const key in data) {
      form_data[key] = data[key]
    }

    try {
      await apiAdmin.post(`/api/admins`, form_data)
      toast.success('Sukses menyimpan data!')
      window.location.href = '/cms/admin'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(
    () =>
      [
        {
          field_name: 'name',
          label: 'Nama',
        },
        {
          field_name: 'email',
          label: 'Email',
        },
        {
          field_name: 'password',
          label: 'Password',
        },
        {
          field_name: 'admin_level_id',
          label: 'Jabatan',
          option: true,
          options: admin_levels.map((al) => {
            return {
              value: al.id,
              label: al.name,
            }
          }),
        },
      ] as InputItemProps[],
    [admin_levels]
  )

  if (!admin_levels) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default AdminCreate
