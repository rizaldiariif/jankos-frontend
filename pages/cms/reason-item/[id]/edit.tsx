import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useMemo, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, {
  InputItemProps,
} from '../../../../components/shared/BasicForm'
import CMSLayout from '../../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../../helper/api-admin'
import { format } from 'date-fns'
import LoadingScreen from '../../../../components/shared/LoadingScreen'

const ReasonItemEdit: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [reason_item, setReasonItem] = useState<any>(null)

  useEffect(() => {
    const fetchReasonItem = async () => {
      let {
        data: { data },
      } = await apiAdmin.get(`/api/reason-items?id.eq=${id}`)
      setReasonItem(data.find((d: any) => d.id == id))
    }
    fetchReasonItem()
  }, [id])

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Submitting Data...')
    const form_data = new FormData()

    for (const key in data) {
      form_data.append(key, data[key])
    }
    form_data.delete('thumbnail')
    if (data.thumbnail && typeof data.thumbnail !== 'string') {
      form_data.append('image_thumbnail', data.thumbnail[0])
    }

    try {
      await apiAdmin.post(`/api/reason-items/${id}`, form_data)
      toast.success('Success!')
      window.location.href = '/cms/reason-item'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(() => {
    const temp_fields = [
      {
        field_name: 'thumbnail',
        label: 'Foto',
        type: 'file',
        default_value: reason_item?.thumbnail,
        validation: { required: false },
      },
      {
        field_name: 'title',
        label: 'Judul',
        default_value: reason_item?.title,
      },
      {
        field_name: 'text',
        label: 'Teks',
        textarea: true,
        default_value: reason_item?.text,
      },
    ] as InputItemProps[]

    return temp_fields
  }, [reason_item])

  if (!reason_item) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default ReasonItemEdit
