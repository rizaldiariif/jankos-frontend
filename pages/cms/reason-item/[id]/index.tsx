import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import BasicDetail, {
  ItemDetailProps,
} from '../../../../components/shared/BasicDetail'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const ReasonItemDetail: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [reason_item, setReasonItem] = useState<any>(null)

  useEffect(() => {
    const fetchReasonItem = async () => {
      let {
        data: { data },
      } = await apiAdmin.get(`/api/reason-items?id.eq=${id}`)
      setReasonItem(data.find((d: any) => d.id == id))
    }
    fetchReasonItem()
  }, [id])

  const item_details = useMemo(() => {
    const temp_fields = [
      {
        key: 'thumbnail',
        label: 'Foto',
        type: 'image',
      },
      {
        key: 'title',
        label: 'Judul',
        type: 'text',
      },
      {
        key: 'text',
        label: 'Teks',
        type: 'text',
      },
    ] as ItemDetailProps[]

    return temp_fields
  }, [reason_item])

  if (!reason_item) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicDetail item={reason_item} item_details={item_details} />
      </div>
    </CMSLayout>
  )
}

export default ReasonItemDetail
