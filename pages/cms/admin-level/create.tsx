import type { NextPage } from 'next'
import { useMemo } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, { InputItemProps } from '../../../components/shared/BasicForm'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

const AdminLevelCreate: NextPage = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan Data...')
    const form_data = {} as any

    for (const key in data) {
      form_data[key] = data[key]
    }

    try {
      await apiAdmin.post(`/api/admin-levels`, form_data)
      toast.success('Sukses menyimpan data!')
      window.location.href = '/cms/admin-level'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(
    () =>
      [
        {
          field_name: 'name',
          label: 'Nama Jabatan',
        },
        {
          field_name: 'hierarchy_number',
          label: 'Level',
          validation: {
            required: true,
            min: 2,
            max: 998,
          },
        },
      ] as InputItemProps[],
    []
  )

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default AdminLevelCreate
