import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import BasicDetail, {
  ItemDetailProps,
} from '../../../../components/shared/BasicDetail'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const AdminLevelDetail: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [admin_level, setAdminLevel] = useState<any>(null)

  useEffect(() => {
    const fetchAdminLevel = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/admin-levels')
      setAdminLevel(data.find((d: any) => d.id == id))
    }
    fetchAdminLevel()
  }, [id])

  const item_details = useMemo(() => {
    const temp_fields = [
      {
        key: 'name',
        label: 'Nama Jabatan',
        type: 'text',
      },
      {
        key: 'hierarchy_number',
        label: 'Level',
        type: 'text',
      },
    ] as ItemDetailProps[]

    return temp_fields
  }, [admin_level])

  if (!admin_level) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicDetail item={admin_level} item_details={item_details} />
      </div>
    </CMSLayout>
  )
}

export default AdminLevelDetail
