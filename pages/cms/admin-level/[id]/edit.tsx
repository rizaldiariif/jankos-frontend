import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useMemo, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import BasicForm, {
  InputItemProps,
} from '../../../../components/shared/BasicForm'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'

const AdminLevelEdit: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [admin_level, setAdminLevel] = useState<any>(null)

  useEffect(() => {
    const fetchAdminLevel = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/admin-levels')
      setAdminLevel(data.find((d: any) => d.id == id))
    }
    fetchAdminLevel()
  }, [id])

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Menyimpan Data...')
    const form_data = {} as any

    for (const key in data) {
      form_data[key] = data[key]
    }

    try {
      await apiAdmin.post(`/api/admin-levels/${id}`, form_data)
      toast.success('Sukses menyimpan data!')
      window.location.href = '/cms/admin-level'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  const input_fields = useMemo(() => {
    const temp_fields = [
      {
        field_name: 'name',
        label: 'Nama Jabatan',
        default_value: admin_level?.name,
      },
      {
        field_name: 'hierarchy_number',
        label: 'Level',
        type: 'number',
        default_value: admin_level?.hierarchy_number,
        validation: {
          required: { value: true, message: 'Level is Required!' },
          min: { value: 2, message: 'Level is less than 2!' },
          max: { value: 998, message: 'Level is greater than 998!' },
        },
      },
    ] as InputItemProps[]

    return temp_fields
  }, [admin_level])

  if (!admin_level) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <BasicForm
          register={register}
          errors={errors}
          input_fields={input_fields}
          onSubmit={onSubmit}
        />
      </div>
    </CMSLayout>
  )
}

export default AdminLevelEdit
