import { format } from 'date-fns'
import type { NextPage } from 'next'
import Link from 'next/link'
import { useCallback, useEffect, useMemo, useState } from 'react'
import DataTable, { TableColumn } from 'react-data-table-component'
import { toast } from 'react-toastify'
import BasicDataTable from '../../../components/shared/BasicDataTable'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

const OrderIndex: NextPage = () => {
  const onDownloadLabel = useCallback(async (order_id) => {
    toast.info('Downloading Shipping Label!')
    const response = await apiAdmin.get(
      `/api/flow/shipping-label/${order_id}`,
      {
        responseType: 'blob',
      }
    )

    const url = window.URL.createObjectURL(new Blob([response.data]))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download', `Shipping Label ${order_id}.pdf`)
    document.body.appendChild(link)
    link.click()
  }, [])

  const onPickup = useCallback(async (order_id) => {
    toast.info('Pesanan sedang diproses!')
    try {
      await apiAdmin.get(`/api/flow/pickup-order/${order_id}`)
      toast.success('Order requested pickup!')
    } catch (error) {
      console.log(error)
      toast.error('Error Happened!')
    }
  }, [])

  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
        width: '60px',
      },
      {
        name: 'Tanggal Order',
        selector: (row: any) => row.createdAt,
        format: (row: any) => format(new Date(row.createdAt), 'dd-MM-yyyy'),
        width: '130px',
      },
      {
        name: 'Jam Order',
        selector: (row: any) => row.createdAt,
        format: (row: any) => format(new Date(row.createdAt), 'kk:mm:ss'),
        width: '130px',
      },
      {
        name: 'Member',
        selector: (row: any) => row.member.email,
      },
      {
        name: 'Status',
        selector: (row: any) => row.order_status.name,
      },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            <Link href={`/cms/order/${row.id}`}>
              <a className="mr-2 whitespace-nowrap rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Detail
              </a>
            </Link>
            {row.order_status.code !== 'menunggu_pembayaran' &&
              row.order_status.code !== 'expired' && (
                <>
                  <button
                    onClick={onDownloadLabel.bind(this, row.id)}
                    className="mr-2 whitespace-nowrap rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white"
                  >
                    Label
                  </button>
                </>
              )}
            {row.order_status.code === 'processing' && (
              <>
                <button
                  onClick={onPickup.bind(this, row.id)}
                  className="mr-2 whitespace-nowrap rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white"
                >
                  Pick Up
                </button>
              </>
            )}
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  const order_statuses = useMemo(() => {
    return [
      {
        label: 'Semua Pesanan',
        value: '',
      },
      {
        label: 'Menunggu Pembayaran',
        value: '38d64e47-7b71-47b0-a3f8-7171c64ea073',
      },
      {
        label: 'Diproses',
        value: '4e07f208-843b-4419-bba4-5a6c18e1e066',
      },
      {
        label: 'Diambil',
        value: '1e9fa11e-43df-4ebc-9cf7-27d15d85d140',
      },
      {
        label: 'Perjalanan',
        value: 'd4b02d1d-3a21-4ba6-a658-7d304065713a',
      },
      {
        label: 'Diterima',
        value: 'ce94b478-df89-451d-8b6f-5cd27ea0096a',
      },
      {
        label: 'Kadaluwarsa',
        value: '47da5cef-eb0d-4d5d-bd85-6f6fc41ad00c',
      },
    ]
  }, [])

  const [selected_order_status, setSelectedOrderStatus] = useState('')

  return (
    <CMSLayout>
      <h1 className="mb-2 text-xl font-bold">Modul Pesanan</h1>
      <div className="rounded-lg shadow-md">
        <select
          className="m-4 rounded bg-jankos-pink-100 px-6 py-2 shadow"
          onChange={(e) => {
            setSelectedOrderStatus(e.target.value)
          }}
          value={selected_order_status}
        >
          {order_statuses.map((order_status) => (
            <option value={order_status.value}>{order_status.label}</option>
          ))}
        </select>
        <BasicDataTable
          columns={columns}
          api={apiAdmin}
          url={'/api/orders'}
          additional_parameter={`${
            selected_order_status != ''
              ? `&order_status_id.eq=${selected_order_status}`
              : ''
          }`}
        />
        {/* <DataTable columns={columns} data={orders} /> */}
      </div>
    </CMSLayout>
  )
}

export default OrderIndex
