import { format } from 'date-fns'
import type { NextPage } from 'next'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import DataTable from 'react-data-table-component'
import BasicDetail, {
  ItemDetailProps,
} from '../../../../components/shared/BasicDetail'
import CMSLayout from '../../../../components/shared/CMSLayout'
import LoadingScreen from '../../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../../helper/api-admin'
import short from 'short-uuid'
import shortUUID from 'short-uuid'
import { formatIncrement } from '../../../../helper/format-increment'

const OrderDetail: NextPage = () => {
  const {
    query: { id },
  } = useRouter()
  const [order, setOrder] = useState<any>(null)

  useEffect(() => {
    const fetchOrder = async () => {
      if (id) {
        let { data } = await apiAdmin.get(`/api/orders/by-id/${id}`)
        setOrder(data)
      }
    }
    fetchOrder()
  }, [id])

  const item_details = useMemo(() => {
    const temp_fields = [
      // {
      //   key: 'id',
      //   label: 'ID',
      //   type: 'text',
      // },
      {
        key: 'payment_url',
        label: 'Link Pembayaran',
        type: 'url',
      },
      {
        key: 'contact_name',
        label: 'Nama Penerima',
        type: 'text',
      },
      {
        key: 'contact_phone',
        label: 'Nomor HP Penerima',
        type: 'text',
      },
      {
        key: 'contact_address',
        label: 'Alamat Penerima',
        type: 'text',
      },
      {
        key: 'contact_email',
        label: 'Email Penerima',
        type: 'text',
      },
      {
        key: 'province_name',
        label: 'Provinsi',
        type: 'text',
      },
      {
        key: 'city_name',
        label: 'Kota',
        type: 'text',
      },
      {
        key: 'subdistrict_name',
        label: 'Kecamatan',
        type: 'text',
      },
      {
        key: 'postal_code',
        label: 'Kode Pos',
        type: 'text',
      },
      {
        key: 'address_note',
        label: 'Catatan Alamat',
        type: 'text',
      },
      {
        key: 'shipping_courier_code',
        label: 'Kode Kurir',
        type: 'text',
      },
      {
        key: 'shipping_courier_service',
        label: 'Kode Servis Pengiriman',
        type: 'text',
      },
      {
        key: 'shipping_id',
        label: 'ID Pengiriman',
        type: 'text',
      },
      {
        key: 'shipping_price',
        label: 'Ongkos Kirim',
        type: 'currency',
      },
      {
        key: 'airwaybill_number',
        label: 'Resi',
        type: 'text',
      },
      {
        key: 'createdAt',
        label: 'Waktu Order',
        type: 'date',
      },
    ] as ItemDetailProps[]

    return temp_fields
  }, [order])

  const member_detail = useMemo(() => {
    const temp_fields = [
      {
        key: 'address',
        label: 'Alamat',
        type: 'text',
      },
      {
        key: 'email',
        label: 'Email',
        type: 'text',
      },
      // {
      //   key: 'id',
      //   label: 'ID',
      //   type: 'text',
      // },
      {
        key: 'image_id_card',
        label: 'Foto ID Card',
        type: 'image',
      },
      {
        key: 'name',
        label: 'Nama',
        type: 'text',
      },
      {
        key: 'phone',
        label: 'Nomor HP',
        type: 'text',
      },
      // {
      //   key: 'referral_code',
      //   label: 'Kode Referral',
      //   type: 'text',
      // },
    ] as ItemDetailProps[]

    return temp_fields
  }, [order])

  const order_detail_columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Nama Produk',
        selector: (row: any) => row.name,
      },
      {
        name: 'Harga',
        selector: (row: any) => row.final_price,
        format: (row: any) => `Rp${row.final_price.toLocaleString()}`,
      },
      {
        name: 'Berat (g)',
        selector: (row: any) => row.weight,
      },
      {
        name: 'Dimensi (cm)',
        selector: (row: any) => `${row.length}x${row.width}x${row.height}`,
      },
      {
        name: 'Jumlah',
        selector: (row: any) => row.quantity,
      },
      {
        name: 'Total Harga',
        selector: (row: any) => row.final_price * row.quantity,
        format: (row: any) =>
          `Rp${(row.final_price * row.quantity).toLocaleString()}`,
      },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            <Link href={`/cms/product/${row.product.id}`}>
              <a className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white">
                Lihat Produk
              </a>
            </Link>
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  const order_status_history_columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Status',
        selector: (row: any) => row.order_status.name,
      },
      {
        name: 'Waktu',
        selector: (row: any) => row.updatedAt,
        format: (row: any) =>
          format(new Date(row.updatedAt), 'dd-MM-yyyy kk:mm:ss'),
      },
    ],
    []
  )

  if (!order) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Data Order</h3>
        {order.order_status.code == 'menunggu_pembayaran' && (
          <p className="mt-3 mb-3 font-semibold text-jankos-pink-300">
            INV/{format(new Date(order.createdAt), 'yyyy')}/
            {format(new Date(order.createdAt), 'MM')}/
            {format(new Date(order.createdAt), 'dd')}/
            {formatIncrement(order.increment_count)}
          </p>
        )}
        {order.order_status.code != 'menunggu_pembayaran' && (
          <a
            target={'_blank'}
            href={`${process.env.NEXT_PUBLIC_BACKEND_URL}/api/flow/download-invoice?order_id=${order.id}`}
            className="mt-3 mb-3 font-semibold text-jankos-pink-300"
          >
            INV/{format(new Date(order.createdAt), 'yyyy')}/
            {format(new Date(order.createdAt), 'MM')}/
            {format(new Date(order.createdAt), 'dd')}/
            {formatIncrement(order.increment_count)}
          </a>
        )}
        <BasicDetail item={order} item_details={item_details} />
      </div>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Data Barang Pesanan</h3>
        <DataTable data={order.order_details} columns={order_detail_columns} />
      </div>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Histori Status Pesanan</h3>
        <DataTable
          data={order.order_status_histories.sort((a: any, b: any) => {
            if (a.createdAt < b.createdAt) {
              return -1
            }
            if (a.createdAt > b.createdAt) {
              return 1
            }
            return 0
          })}
          columns={order_status_history_columns}
        />
      </div>
      <div className="mb-4 rounded-lg bg-white p-6 shadow-md">
        <h3 className="mb-2 text-lg font-semibold">Data Member</h3>
        <p className="mt-3 mb-3 font-semibold text-jankos-pink-300">
          {shortUUID().fromUUID(order.member.id).toUpperCase()}
        </p>
        <BasicDetail item={order.member} item_details={member_detail} />
      </div>
    </CMSLayout>
  )
}

export default OrderDetail
