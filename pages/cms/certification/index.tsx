import type { NextPage } from 'next'
import { useMemo, useEffect, useState } from 'react'
import DataTable from 'react-data-table-component'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import CMSLayout from '../../../components/shared/CMSLayout'
import LoadingScreen from '../../../components/shared/LoadingScreen'
import { apiAdmin } from '../../../helper/api-admin'

const CertificationIndex: NextPage = () => {
  const [certifications, setCertifications] = useState<any>(null)

  useEffect(() => {
    const fetchCertifications = async () => {
      let {
        data: { data },
      } = await apiAdmin.get('/api/certifications')
      setCertifications(data)
    }
    fetchCertifications()
  }, [])

  const {
    formState: { errors },
  } = useForm()

  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Foto',
        cell: (row: any) => (
          <img src={row.thumbnail} className="h-16 w-auto rounded border p-1" />
        ),
      },
      // {
      //   name: 'Nama',
      //   selector: (row: any) => row.name,
      // },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            <button
              onClick={onDeleteImage.bind(this, row.id)}
              className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white"
            >
              Hapus
            </button>
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  const uploadFile = async (file: any) => {
    toast.info('Menyimpan Sertifikasi...')
    const form_data = new FormData()

    form_data.append('image_thumbnail', file)

    try {
      await apiAdmin.post(`/api/certifications`, form_data)
      toast.success('Sukses Menyimpan Sertifikasi!')
      window.location.reload()
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  }

  const onDeleteImage = async (id: string) => {
    toast.info('Menghapus Sertifikasi...')
    try {
      await apiAdmin.delete(`/api/certifications/${id}`)
      toast.success('Sukses Menghapus Sertifikasi!')
      window.location.reload()
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  }

  if (!certifications) {
    return <LoadingScreen />
  }

  return (
    <CMSLayout>
      <div className="rounded-lg bg-white p-6 shadow-md">
        <button className="relative mb-6 ml-auto block w-min whitespace-nowrap rounded bg-jankos-pink-300 py-2 px-6 text-xs text-white">
          Tambah Sertifikasi Baru
          <input
            type="file"
            className="absolute top-0 left-0 h-full w-full cursor-pointer opacity-0"
            onChange={(e) => {
              if (e.target.files) {
                uploadFile(e.target.files[0])
              }
            }}
          />
        </button>
        <h1 className="mb-2 text-xl font-bold">Sertifikasi</h1>
        <DataTable columns={columns} data={certifications} />
      </div>
    </CMSLayout>
  )
}

export default CertificationIndex
