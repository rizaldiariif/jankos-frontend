import { format } from 'date-fns'
import React, { useEffect, useMemo, useState } from 'react'
import DataTable from 'react-data-table-component'
import { toast } from 'react-toastify'
import CMSLayout from '../../../components/shared/CMSLayout'
import { apiAdmin } from '../../../helper/api-admin'

type Props = {}

const CommissionIndex = (props: Props) => {
  const [withdrawal_ledgers, setWithdrawalLedgers] = useState<any[]>([])
  const columns = useMemo(
    () => [
      {
        name: 'No',
        selector: (_row: any, index: any) => (index ? index + 1 : 1),
      },
      {
        name: 'Waktu',
        selector: (row: any) => row.createdAt,
        format: (row: any) => format(new Date(row.createdAt), 'dd-MM-yyyy'),
      },
      {
        name: 'Member',
        selector: (row: any) => row.member.name,
      },
      {
        name: 'Reference No',
        selector: (row: any) => row.reference_no,
      },
      {
        name: 'Status',
        selector: (row: any) => row.status,
      },
      {
        name: 'Jumlah',
        selector: (row: any) => row.amount,
        format: (row: any) => `Rp${row.amount.toLocaleString()}`,
      },
      {
        name: 'Action',
        cell: (row: any) => (
          <div className="flex items-center">
            {row.status === 'Request Approval' && (
              <button
                className="rounded bg-jankos-pink-300 py-1 px-3 text-xs text-white"
                onClick={onApprove.bind(this, row.id)}
              >
                Approve
              </button>
            )}
          </div>
        ),
        center: true,
      },
    ],
    []
  )

  useEffect(() => {
    const fetchMembers = async () => {
      const {
        data: { data },
      } = await apiAdmin.get('/api/member-withdrawal-ledgers')
      setWithdrawalLedgers(data)
    }
    fetchMembers()
  }, [])

  const onApprove = (member_withdrawal_ledger_id: string) => {
    if (confirm('Apakah anda yakin?')) {
      toast.info('Processing Withdrawal...')
      setTimeout(() => {
        toast.success('Withdrawal Processed!')
      }, 500)
    }
  }

  return (
    <CMSLayout>
      <h1 className="mb-2 text-xl font-bold">Modul Penarikan Komisi</h1>
      <div className="rounded-lg shadow-md">
        <DataTable columns={columns} data={withdrawal_ledgers} />
      </div>
    </CMSLayout>
  )
}

export default CommissionIndex
