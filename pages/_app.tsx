import '../styles/globals.css'
import '@splidejs/react-splide/css'
import 'react-toastify/dist/ReactToastify.css'
import 'react-responsive-carousel/lib/styles/carousel.css'
import 'react-datepicker/dist/react-datepicker.css'
import type { AppProps } from 'next/app'
import { ToastContainer } from 'react-toastify'
import Head from 'next/head'

function MyApp({ Component, pageProps }: AppProps) {

  
  return (
    <>
      <Head>
        <title>Jankos Glow</title>
      </Head>
      <Component {...pageProps} />
      <ToastContainer />
    </>
  )
}

export default MyApp
