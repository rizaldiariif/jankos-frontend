import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html>
      <Head />
      <body className="text-black">
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
