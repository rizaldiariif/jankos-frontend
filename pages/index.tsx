import { Splide, SplideSlide } from '@splidejs/react-splide'
import { format } from 'date-fns'
import type { NextPage } from 'next'
import Image from 'next/image'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import { useInterval } from 'react-use'
import useSWR from 'swr'
import Footer from '../components/shared/Footer'
import Header from '../components/shared/Header'
import HeaderMobile from '../components/shared/HeaderMobile'
import { fetcher } from '../helper/fetcher'
import { pageContentFinder } from '../helper/page-content-finder'
import SimpleImageSlider from 'react-simple-image-slider'
import { Carousel } from 'react-responsive-carousel'

interface PageProps {
  initial_products: any[]
  initial_banner_homes: any[]
  initial_articles: any[]
  initial_testimonies: any[]
  initial_page_contents: any[]
  initial_global_contents: any[]
  initial_reason_items: any[]
}

const Home: NextPage<PageProps> = (props: PageProps) => {
  const {
    initial_products,
    initial_banner_homes,
    initial_articles,
    initial_testimonies,
    initial_page_contents,
    initial_global_contents,
    initial_reason_items,
  } = props

  const { data: products } = useSWR('/api/products', fetcher, {
    fallbackData: initial_products,
  })
  const { data: banner_homes } = useSWR('/api/banner-homes', fetcher, {
    fallbackData: initial_banner_homes,
  })
  const { data: articles } = useSWR('/api/articles', fetcher, {
    fallbackData: initial_articles,
  })
  const { data: testimonies } = useSWR('/api/testimonies', fetcher, {
    fallbackData: initial_testimonies,
  })
  const {
    data: { data: page_contents },
  } = useSWR('/api/page-contents?page.eq=home', fetcher, {
    fallbackData: initial_page_contents,
  })
  const {
    data: { data: global_contents },
  } = useSWR('/api/page-contents?page.eq=global', fetcher, {
    fallbackData: initial_global_contents,
  })
  const { data: reason_items } = useSWR('/api/reason-items', fetcher, {
    fallbackData: initial_reason_items,
  })

  const [hero_active_index, setHeroActiveIndex] = useState(0)

  useEffect(() => {
    setInterval(() => {
      setHeroActiveIndex(hero_active_index + 1)
    }, 3000)
  }, [])

  return (
    <div className="relative overflow-hidden">
      <Header page_contents={global_contents} />
      <HeaderMobile />
      <div className="relative w-screen max-w-full bg-white">
        <div className="grid w-full grid-cols-1 lg:grid-cols-2 lg:gap-2">
          <div className="relative flex h-112 w-full items-center justify-center overflow-hidden bg-jankos-pink-300 lg:h-180">
            {/* <Image
              layout="fill"
              objectFit="cover"
              src={pageContentFinder(page_contents, 'hero_image')}
            /> */}
            {/* <Splide
              aria-label="My Favorite Images"
              id="splide-hero-home"
              options={{
                type: 'loop',
                autoHeight: true,
                autoWidth: true,
                autoplay: true,
                arrows: false,
                interval: 3000,
              }}
            >
              {banner_homes.data.map((banner: any) => (
                <SplideSlide>
                  <img src={banner.thumbnail} alt="Image 2" />
                </SplideSlide>
              ))}
            </Splide> */}
            {/* <SimpleImageSlider
              width={'100%'}
              height={'100%'}
              showBullets={true}
              showNavs={false}
              autoPlay={true}
              images={banner_homes.data.map((banner: any) => {
                return {
                  url: banner.thumbnail,
                }
              })}
              loop={false}
            /> */}
            <Carousel
              showIndicators={true}
              showArrows={false}
              showStatus={false}
              showThumbs={false}
              className="h-full"
              autoPlay={true}
              infiniteLoop={true}
            >
              {banner_homes.data.map((banner: any) => (
                <Image
                  src={banner.thumbnail}
                  layout="fill"
                  objectFit="cover"
                  objectPosition={'center center'}
                  quality={100}
                />
                // <img
                //   src={banner.thumbnail}
                //   alt=""
                //   className="bg-cover bg-center object-cover"
                // />
              ))}
            </Carousel>
            {/* {banner_homes.data.map((banner: any, banner_index: number) => (
              <Image
                src={banner.thumbnail}
                layout="fill"
                objectFit="cover"
                objectPosition={'center center'}
              />
            ))} */}
          </div>
          <div className="flex h-144 flex-col items-start justify-center bg-jankos-pink-100 px-8 lg:h-180 lg:px-24">
            <h1 className="text-4xl leading-tight lg:text-5xl lg:leading-tight">
              <span className="font-bold">
                {pageContentFinder(page_contents, 'hero_title_bold')}
              </span>
              {pageContentFinder(page_contents, 'hero_title_normal')}
            </h1>
            <div
              className="wysiwyig-container py-8"
              dangerouslySetInnerHTML={{
                __html: pageContentFinder(page_contents, 'hero_content_1'),
              }}
            />
            {/* <p className="py-8 text-base">
              {pageContentFinder(page_contents, 'hero_content_1')}
            </p> */}
            <p className="pb-4 text-base">
              {pageContentFinder(page_contents, 'hero_content_2')}
            </p>
            <Link href="/auth/signup">
              <a className="rounded-full border border-jankos-pink-300 bg-jankos-pink-300 py-3 px-6 text-base font-semibold text-white transition-colors hover:bg-transparent hover:text-jankos-pink-300">
                {pageContentFinder(page_contents, 'hero_button')}
              </a>
            </Link>
          </div>
        </div>
      </div>
      <div
        className="relative h-auto w-screen max-w-full bg-white"
        id="tentang-kami"
      >
        <div className="z-10 ml-auto block h-auto w-2/3 lg:absolute lg:bottom-0 lg:right-0 lg:top-1/2 lg:w-1/3 lg:-translate-y-1/2 lg:transform">
          <Image
            src={pageContentFinder(page_contents, 'about_us_image')}
            width="778px"
            height="896px"
          />
        </div>
        <div className="container relative z-10 m-auto mb-24 flex h-full flex-col items-start justify-center">
          <p className="mb-2 text-base text-jankos-pink-300 lg:mt-24">
            {pageContentFinder(page_contents, 'about_us_subtitle')}
          </p>
          <h2 className="mb-4 text-4xl font-bold leading-tight lg:w-1/2">
            {pageContentFinder(page_contents, 'about_us_title')}
          </h2>
          <div
            className="wysiwyig-container text-base lg:w-1/2"
            dangerouslySetInnerHTML={{
              __html: pageContentFinder(page_contents, 'about_us_content'),
            }}
          />
          {/* <p className="text-base lg:w-1/2">
            {pageContentFinder(page_contents, 'about_us_content')}
          </p> */}
        </div>
        <div className="absolute top-0 left-0 z-0 h-auto w-1/2 lg:w-1/4">
          <Image
            src="https://res.cloudinary.com/rizaldiariif/image/upload/v1664732116/jankos/Group_45_judhzn.png"
            width="416.47px"
            height="286.45px"
          />
        </div>
        {/* <span className="absolute bottom-24 right-24 hidden h-96 w-96 rounded-full bg-jankos-pink-100 lg:block" /> */}
      </div>
      <div
        className="relative h-auto w-screen max-w-full bg-jankos-pink-100 lg:h-180"
        id="produk"
      >
        <div className="container mx-auto flex h-full w-full flex-col py-14 ">
          <h2 className="relative mx-auto mb-11 w-min whitespace-nowrap pb-4 text-4xl">
            {pageContentFinder(page_contents, 'product_title_normal')}
            <span className="font-bold">
              {' ' + pageContentFinder(page_contents, 'product_title_bold')}
            </span>
            <span className="absolute bottom-0 left-1/2 h-1 w-1/2 -translate-x-1/2 transform rounded-full bg-jankos-green-200"></span>
          </h2>
          <p className="mb-11 text-center">
            {pageContentFinder(page_contents, 'product_subtitle')}
          </p>
          <div className="h-full w-full flex-grow">
            <Splide
              aria-label="My Favorite Images"
              options={{
                perPage: 4,
                gap: '36px',
                pagination: false,
                perMove: 1,
                breakpoints: {
                  600: {
                    perPage: 1,
                    gap: '0px',
                  },
                  800: {
                    perPage: 3,
                    gap: '24px',
                  },
                },
              }}
            >
              {products.data.map((product: any) => (
                <SplideSlide key={product.id}>
                  <Link href={`/product/${product.id}`}>
                    <a className="relative flex h-full w-full flex-col items-center justify-start">
                      <div className="z-10 mb-7 h-auto w-full">
                        <Image
                          src={product.thumbnail}
                          width="480px"
                          height="480px"
                        />
                      </div>
                      <p className="w-full text-center text-lg font-bold">
                        {product.name}
                      </p>
                      {/* <div className="mb-3 grid w-1/3 grid-cols-5 place-items-center">
                        {[1, 2, 3, 4, 5].map((val) =>
                          val <= product.rating ? (
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="h-5 w-5 text-jankos-pink-300"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                          ) : (
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="h-5 w-5 text-gray-400"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                          )
                        )}
                      </div> */}
                      <div
                        className="text-center"
                        dangerouslySetInnerHTML={{
                          __html: product.short_description,
                        }}
                      ></div>
                      {/* <p className="text-center">{}</p> */}
                    </a>
                  </Link>
                </SplideSlide>
              ))}
            </Splide>
          </div>
        </div>
      </div>
      <div className="relative h-auto w-screen max-w-full bg-white">
        <div className="container mx-auto py-28">
          <h2 className="relative mx-auto mb-4 pb-4 text-center text-4xl leading-tight lg:w-min lg:whitespace-nowrap">
            {pageContentFinder(page_contents, 'reason_title_normal')}
            <span className="font-bold">
              {' ' + pageContentFinder(page_contents, 'reason_title_bold')}
            </span>
            <span className="absolute bottom-0 left-1/2 h-1 w-1/3 -translate-x-1/2 transform rounded-full bg-jankos-pink-300"></span>
          </h2>
          <p className="mb-4 text-center font-bold">
            {pageContentFinder(page_contents, 'reason_subtitle_bold')}
          </p>
          <div
            className="wysiwyig-container mb-11 text-center"
            dangerouslySetInnerHTML={{
              __html: pageContentFinder(
                page_contents,
                'reason_subtitle_normal'
              ),
            }}
          />
          <div className="h-full w-full flex-grow">
            <Splide
              aria-label="Jankos Reasons"
              options={{
                perPage: 4,
                gap: '72px',
                perMove: 1,
                rewind: false,
                pagination: false,
                breakpoints: {
                  600: {
                    perPage: 1,
                    gap: '0px',
                  },
                  800: {
                    perPage: 3,
                    gap: '24px',
                  },
                },
              }}
            >
              {reason_items.data.map((reason_item: any) => (
                <SplideSlide key={reason_item.id}>
                  <div className="relative flex h-full w-full flex-col items-center justify-start">
                    <div className="z-10 mx-auto mb-6 flex h-auto w-full items-center justify-center text-center">
                      <Image
                        src={reason_item.thumbnail}
                        width="93px"
                        height="93px"
                        className="mx-auto text-center"
                      />
                    </div>
                    <p className="mb-2 w-full text-center text-sm font-bold">
                      {reason_item.title}
                    </p>
                    <p className="text-center text-xs">{reason_item.text}</p>
                  </div>
                </SplideSlide>
              ))}
            </Splide>
          </div>
          {/* <div className="my-20 grid items-start gap-12 md:grid-cols-2 lg:grid-cols-4 lg:gap-6">
            {[1, 2, 3, 4].map((val) => (
              <div className="flex flex-col items-center justify-center text-center">
                <div className="mb-8">
                  <Image
                    src={pageContentFinder(page_contents, `reason_${val}_icon`)}
                    width={128}
                    height={128}
                    quality={100}
                  />
                </div>
                <span className="mb-8 flex h-32 w-32 items-center justify-center rounded-full bg-jankos-pink-100 text-5xl text-jankos-pink-300">
                  {pageContentFinder(page_contents, `reason_${val}_icon`)}
                </span>
                <p className="mb-2 w-2/3 font-semibold">
                  {pageContentFinder(page_contents, `reason_${val}_title`)}
                </p>
                <p className="w-2/3 text-sm">
                  {pageContentFinder(page_contents, `reason_${val}_content`)}
                </p>
              </div>
            ))}
          </div> */}
        </div>
      </div>
      <div className="relative h-auto w-screen max-w-full bg-white">
        <div className="grid w-full lg:grid-cols-2">
          <div className="relative h-124 w-full lg:h-168">
            <Image
              layout="fill"
              objectFit="cover"
              src={pageContentFinder(page_contents, 'benefit_image')}
            />
          </div>
          <div className="flex h-auto w-full flex-col items-start justify-center bg-jankos-green-100 py-14 px-8 lg:h-168 lg:py-0 lg:px-24">
            <h2 className="relative mb-4 w-min whitespace-nowrap pb-4 text-4xl">
              {pageContentFinder(page_contents, 'benefit_title_normal')}
              <span className="font-bold">
                {' ' + pageContentFinder(page_contents, 'benefit_title_bold')}
              </span>
              <span className="absolute bottom-0 left-0 h-1.5 w-1/3 transform rounded-full bg-jankos-pink-300"></span>
            </h2>
            <p className="mb-12">
              {pageContentFinder(page_contents, 'benefit_subtitle')}
            </p>
            <ul className="mb-4">
              {[1, 2, 3, 4, 5].map((val) => (
                <li className="mb-6 flex items-center justify-start">
                  <span className="relative flex h-9 w-9 flex-nowrap items-center justify-center rounded-full bg-jankos-green-200 pt-1 text-center text-white">
                    {val}
                  </span>
                  <p className="ml-5 flex-1">
                    {pageContentFinder(page_contents, `benefit_content_${val}`)}
                  </p>
                </li>
              ))}
            </ul>
            <p className="font-bold">
              {pageContentFinder(page_contents, `benefit_footer`)}
            </p>
          </div>
        </div>
      </div>
      <div className="relative h-auto w-screen max-w-full overflow-hidden bg-white">
        <div className="container relative z-10 mx-auto py-32">
          <h2 className="mb-4 pb-4 text-center text-4xl">
            {pageContentFinder(page_contents, 'testimony_title_normal')}
            <span className="font-bold">
              {' ' + pageContentFinder(page_contents, 'testimony_title_bold')}
            </span>
          </h2>
          <div
            className="mx-auto mb-24 text-center md:w-3/4 lg:w-1/2"
            dangerouslySetInnerHTML={{
              __html: pageContentFinder(page_contents, 'testimony_subtitle'),
            }}
          ></div>
          <Splide
            options={{
              perPage: 3,
              gap: '64px',
              // type: 'loop',
              rewind: false,
              pagination: false,
              perMove: 1,
              breakpoints: {
                600: {
                  perPage: 1,
                  gap: '0px',
                },
                800: {
                  perPage: 2,
                  gap: '24px',
                },
              },
            }}
          >
            {testimonies.data.map((testimony: any) => (
              <SplideSlide>
                <div className="mx-4 mt-12 mb-12 h-80 transform rounded-xl border border-slate-100 bg-white px-8 pt-24 pb-8 shadow-lg">
                  <div className="absolute -top-10 left-1/2 z-20 block h-20 w-20 -translate-x-1/2 transform overflow-hidden rounded-full">
                    <Image
                      src={testimony.thumbnail}
                      layout="fill"
                      objectFit="cover"
                      objectPosition={'center center'}
                    />
                  </div>
                  <div className="relative mb-4 flex h-full w-full flex-col items-center justify-center px-2 pb-12 pt-4 text-center">
                    <p className="mb-2 text-sm font-medium">{testimony.name}</p>
                    <p className="text-xs">{testimony.text}</p>
                    <svg
                      width="18"
                      height="15"
                      viewBox="0 0 18 15"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="absolute -top-8 -left-2 h-8 w-8"
                    >
                      <path
                        d="M13.1275 13.9525C12.1008 13.9525 11.2667 13.6225 10.625 12.9625C9.98333 12.3025 9.6625 11.3675 9.6625 10.1575C9.6625 8.63584 10.1392 7.03167 11.0925 5.345C12.0642 3.65834 13.3017 2.21 14.805 1L16.0975 2.1275C15.7492 2.49417 15.41 2.98 15.08 3.585C14.7683 4.17167 14.4933 4.80417 14.255 5.4825C14.035 6.16084 13.8975 6.83 13.8425 7.49C14.6492 7.655 15.2908 8.03084 15.7675 8.6175C16.2625 9.18583 16.51 9.87334 16.51 10.68C16.51 11.6333 16.1983 12.4217 15.575 13.045C14.9517 13.65 14.1358 13.9525 13.1275 13.9525ZM4.465 13.9525C3.42 13.9525 2.57667 13.6225 1.935 12.9625C1.31167 12.3025 1 11.3675 1 10.1575C1 8.63584 1.47667 7.03167 2.43 5.345C3.38333 3.65834 4.61167 2.21 6.115 1L7.435 2.1275C7.08667 2.49417 6.7475 2.98 6.4175 3.585C6.10583 4.17167 5.83083 4.80417 5.5925 5.4825C5.3725 6.16084 5.235 6.83 5.18 7.49C5.98667 7.655 6.62833 8.03084 7.105 8.6175C7.6 9.18583 7.8475 9.87334 7.8475 10.68C7.8475 11.6333 7.53583 12.4217 6.9125 13.045C6.28917 13.65 5.47333 13.9525 4.465 13.9525Z"
                        fill="#CC676A"
                      />
                    </svg>
                    <svg
                      width="18"
                      height="15"
                      viewBox="0 0 18 15"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="absolute -bottom-4 -right-2 h-8 w-8"
                    >
                      <path
                        d="M4.38251 13.9525C5.40917 13.9525 6.24334 13.6225 6.88501 12.9625C7.52668 12.3025 7.84751 11.3675 7.84751 10.1575C7.84751 8.63584 7.37084 7.03167 6.41751 5.345C5.44584 3.65834 4.20834 2.21 2.70501 1L1.41251 2.1275C1.76084 2.49417 2.10001 2.98 2.43001 3.585C2.74168 4.17167 3.01668 4.80417 3.25501 5.4825C3.47501 6.16084 3.61251 6.83 3.66751 7.49C2.86084 7.655 2.21917 8.03084 1.74251 8.6175C1.24751 9.18583 1.00001 9.87334 1.00001 10.68C1.00001 11.6333 1.31168 12.4217 1.93501 13.045C2.55834 13.65 3.37417 13.9525 4.38251 13.9525ZM13.045 13.9525C14.09 13.9525 14.9333 13.6225 15.575 12.9625C16.1983 12.3025 16.51 11.3675 16.51 10.1575C16.51 8.63584 16.0333 7.03167 15.08 5.345C14.1267 3.65834 12.8983 2.21 11.395 1L10.075 2.1275C10.4233 2.49417 10.7625 2.98 11.0925 3.585C11.4042 4.17167 11.6792 4.80417 11.9175 5.4825C12.1375 6.16084 12.275 6.83 12.33 7.49C11.5233 7.655 10.8817 8.03084 10.405 8.6175C9.91001 9.18583 9.66251 9.87334 9.66251 10.68C9.66251 11.6333 9.97418 12.4217 10.5975 13.045C11.2208 13.65 12.0367 13.9525 13.045 13.9525Z"
                        fill="#CC676A"
                      />
                    </svg>
                  </div>
                </div>
              </SplideSlide>
            ))}
          </Splide>
        </div>
        <span
          className="absolute top-0 right-0 z-0 h-96 w-96 translate-x-1/2 transform rounded-full"
          style={{ backgroundColor: 'rgba(247, 160, 155, 0.34)' }}
        />
        <span
          className="absolute top-1/3 left-4 z-0 h-16 w-16 translate-x-full -translate-y-1/2 transform rounded-full"
          style={{ backgroundColor: '#F3A39E' }}
        />
        <span
          className="bottom-164 absolute right-4 z-0 h-16 w-16 -translate-x-full transform rounded-full"
          style={{ backgroundColor: 'rgba(123, 154, 98, 0.57)' }}
        />
        <span
          className="absolute bottom-0 left-0 z-0 h-124 w-124 -translate-x-1/2 transform rounded-full"
          style={{ backgroundColor: 'rgba(212, 227, 190, 0.31)' }}
        />
      </div>
      <div className="relative h-auto w-screen max-w-full bg-jankos-pink-100">
        <div className="container mx-auto py-20">
          <h2 className="mb-10 pb-4 text-center text-4xl">
            {pageContentFinder(page_contents, 'article_title_normal')}
            <span className="font-bold">
              {' ' + pageContentFinder(page_contents, 'article_title_bold')}
            </span>
          </h2>
          <div className="grid grid-cols-1 gap-5 md:grid-cols-2 lg:grid-cols-3">
            {articles.data.slice(0, 3).map((article: any) => (
              <Link href={`/blog/${article.slug}`} key={article.id}>
                <a className="relative w-full overflow-hidden rounded-md bg-white">
                  <div className="relative h-52 w-full">
                    <Image
                      src={article.thumbnail}
                      layout="fill"
                      objectFit="cover"
                      objectPosition="center"
                    />
                  </div>
                  <div className="bg-white p-4">
                    <p className="mb-20 font-semibold">{article.title}</p>
                    <div className="grid w-full grid-cols-2">
                      <div className="flex items-center justify-start">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6 text-jankos-pink-300"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                          strokeWidth={2}
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                          />
                        </svg>
                        <p className="mt-1 ml-2 text-xs leading-none">
                          {format(new Date(article.createdAt), 'dd MMMM yyyy')}
                        </p>
                      </div>
                      <div className="flex items-center justify-start">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-5 w-5 text-jankos-pink-300"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                        >
                          <path
                            fillRule="evenodd"
                            d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                            clipRule="evenodd"
                          />
                        </svg>{' '}
                        <p className="mt-1 ml-2 text-xs leading-none">
                          {article.author}
                        </p>
                      </div>
                    </div>
                  </div>
                </a>
              </Link>
            ))}
          </div>
        </div>
      </div>
      <div className="relative h-auto w-screen max-w-full bg-white">
        <div className="container relative z-10 mx-auto py-24 lg:py-44">
          <h2 className="text-center text-2xl leading-normal text-white lg:text-4xl lg:leading-loose">
            {pageContentFinder(page_contents, 'cta_subtitle')}
          </h2>
          <h2 className="mb-9 text-center text-2xl font-bold leading-normal text-white lg:text-4xl lg:leading-loose">
            {pageContentFinder(page_contents, 'cta_title')}
          </h2>
          <div className="text-center">
            <Link href="/auth/signup">
              <a className="mx-auto w-min whitespace-nowrap rounded-full bg-jankos-pink-300 py-4 px-14 text-sm font-semibold text-white">
                {pageContentFinder(page_contents, 'cta_button')}
              </a>
            </Link>
          </div>
        </div>
        <Image
          src={pageContentFinder(page_contents, 'cta_image')}
          layout="fill"
          objectFit="cover"
          objectPosition="center"
        />
      </div>
      <Footer page_contents={global_contents} />
    </div>
  )
}

export async function getStaticProps() {
  const initial_products = await fetcher('/api/products')
  const initial_banner_homes = await fetcher('/api/banner-homes')
  const initial_articles = await fetcher('/api/articles')
  const initial_testimonies = await fetcher('/api/testimonies')
  const initial_page_contents = await fetcher('/api/page-contents?page.eq=home')
  const initial_global_contents = await fetcher(
    '/api/page-contents?page.eq=global'
  )
  const initial_reason_items = await fetcher('/api/reason-items')
  return {
    props: {
      initial_products,
      initial_banner_homes,
      initial_articles,
      initial_testimonies,
      initial_page_contents,
      initial_global_contents,
      initial_reason_items,
    },
    revalidate: 6000,
  }
}

export default Home
