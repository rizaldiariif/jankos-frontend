import type { NextPage } from 'next'
import { useEffect, useMemo, useState } from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import useSWR from 'swr'
import AuthHeader from '../../components/shared/AuthHeader'
import { api } from '../../helper/api'
import { fetcher } from '../../helper/fetcher'
import Select from 'react-select/'
import { useThrottle, useThrottleFn } from 'react-use'
import { setCookies } from 'cookies-next'
import Image from 'next/image'
import IconShipping from '../../components/shared/IconShipping'
import BasicInput from '../../components/shared/BasicInput'
import HeaderMobile from '../../components/shared/HeaderMobile'
import { pageContentFinder } from '../../helper/page-content-finder'

interface PageProps {
  initial_member_levels: any
  initial_products: any
  initial_provinces: any
  initial_cities: any
  initial_subdistricts: any
  initial_global_contents: any[]
}

const SignUp: NextPage<PageProps> = (props: PageProps) => {
  const { initial_member_levels, initial_products, initial_global_contents } =
    props

  const { data: member_levels } = useSWR('/api/member-levels', fetcher, {
    fallbackData: initial_member_levels,
  })

  const { data: products } = useSWR('/api/products?is_starter.eq=1', fetcher, {
    fallbackData: initial_products,
  })

  const {
    data: { data: global_contents },
  } = useSWR('/api/page-contents?page.eq=global', fetcher, {
    fallbackData: initial_global_contents,
  })

  // const [current_member_level, setCurrentMemberLevel] = useState<null | any>(
  //   null
  // )
  const [next_member_level, setNextMemberLevel] = useState<null | any>(null)
  // const [total_product, setTotalProduct] = useState(0)

  const {
    register,
    handleSubmit,
    watch,
    setValue,
    getValues,
    formState: { errors },
  } = useForm()
  const watch_member_level_id = watch('member_level_id')
  const watch_quantity = watch('quantity')
  const watch_subdistrict = watch('subdistrict')
  const watch_postal_code = watch('postal_code')
  const watch_courier = watch('courier')
  const watch_shipping_address = watch('shipping_address')
  const watch_referral_code = watch('referral_code')
  const throttled_referral_code = useThrottle(watch_referral_code, 2000)
  const watch_image_id_card = watch('image_id_card')
  const watch_shipping_type = watch('shipping_type')
  useEffect(() => {
    setValue('shipping_type', 'courier')
  }, [])
  const watch_password = watch('password')

  const [subdistrict_options, setSubdistrictOptions] = useState([])
  const [postal_code_options, setPostalCodeOptions] = useState([])
  const [subdistrict, setSubdistrict] = useState<string | null>(null)
  const [loading_subdistrict_option, setLoadingSubdistrictOption] =
    useState(false)
  const throttled_subdistrict = useThrottle(subdistrict, 2000)
  const [courier_options, setCourierOptions] = useState([])
  // const [upline_member, setUplineMember] = useState(null)
  // const [loading_upline_member, setLoadingUplineMember] = useState(false)
  const [step, setStep] = useState(1)
  const [error_subdistrict, setErrorSubdistrict] = useState<null | string>(null)
  const [error_postal_code, setErrorPostalCode] = useState<null | string>(null)
  const [error_courier, setErrorCourier] = useState<null | string>(null)
  const [error_ktp, setErrorKTP] = useState<null | string>(null)

  const current_member_level = useMemo(() => {
    if (watch_member_level_id) {
      const found_member_level = member_levels.data.find(
        (member_level: any) => member_level.id == watch_member_level_id
      )
      return found_member_level
    }
  }, [watch_member_level_id, member_levels])

  useEffect(() => {
    const fetchSubdistrict = async () => {
      if (throttled_subdistrict) {
        setLoadingSubdistrictOption(true)
        const { data } = await fetcher(
          `/api/flow/listing-locations?type=subdistrict&name=${throttled_subdistrict}`
        )
        setSubdistrictOptions(data)
        setTimeout(() => {
          setLoadingSubdistrictOption(false)
        }, 2000)
      }
    }
    fetchSubdistrict()
  }, [throttled_subdistrict])

  // useEffect(() => {
  //   const fetchUplineMember = async () => {
  //     setUplineMember(null)
  //     setLoadingUplineMember(true)
  //     if (throttled_referral_code) {
  //       try {
  //         const { data } = await api(
  //           `/api/flow/check-referral-code?referral_code=${throttled_referral_code}`
  //         )

  //         setUplineMember(data)
  //       } catch (error: any) {
  //         console.log(error.response)
  //       }
  //     }
  //     setLoadingUplineMember(false)
  //   }
  //   fetchUplineMember()
  // }, [throttled_referral_code])

  useEffect(() => {
    const fetchPostalCodeOptions = async () => {
      if (watch_subdistrict) {
        const { data } = await fetcher(
          `/api/flow/listing-postal-codes?name=${watch_subdistrict}`
        )
        setPostalCodeOptions(data)
      }
    }
    fetchPostalCodeOptions()
  }, [watch_subdistrict])

  useEffect(() => {
    const fetchCouriers = async () => {
      if (
        products.data[0].id &&
        watch(`quantity_${products.data[0].id}`) &&
        watch_subdistrict &&
        watch_postal_code
      ) {
        const response = await api.post(
          '/api/flow/check-shipping-price-signup',
          {
            product_id: products.data[0].id,
            quantity: watch(`quantity_${products.data[0].id}`),
            destination_subdistrict_code: watch_subdistrict,
            destination_postal_code: watch_postal_code,
            products: JSON.stringify(
              products.data.map((product: any) => {
                return {
                  product_id: product.id,
                  quantity: getValues(`quantity_${product.id}`),
                }
              })
            ),
          }
        )

        setCourierOptions(response.data)
      }
    }
    fetchCouriers()
  }, [products.data[0].id, watch_subdistrict, watch_postal_code])

  useEffect(() => {
    if (watch_member_level_id) {
      const found_member_level = member_levels.data.find(
        (member_level: any) => member_level.id == watch_member_level_id
      )
      const found_next_member_level = member_levels.data.find(
        (member_level: any) =>
          member_level.minimum_order > found_member_level.minimum_order
      )
      setNextMemberLevel(found_next_member_level)
    }
  }, [watch_member_level_id, member_levels])

  const onSubmit = handleSubmit(async (data) => {
    if (!getValues('subdistrict')) {
      setErrorSubdistrict('Alamat Kecamatan harus diisi')
      return
    }

    if (!getValues('postal_code')) {
      setErrorPostalCode('Kode Pos harus diisi')
      return
    }

    if (!getValues('courier') && getValues('shipping_type') == 'courier') {
      setErrorCourier('Kurir Pengiriman harus diisi')
      return
    }

    if (watch_image_id_card.length == 0) {
      setErrorKTP('Foto KTP Harus diisi')
      return
    }

    if (
      error_subdistrict ||
      error_postal_code ||
      error_courier ||
      error_ktp ||
      !getValues('subdistrict') ||
      !getValues('postal_code')
    ) {
      return
    }

    let total_product_quantity = 0
    products.data.forEach((p: any) => {
      total_product_quantity += parseInt(getValues(`quantity_${p.id}`))
    })

    if (total_product_quantity < current_member_level.minimum_order) {
      toast.error(
        `Total pembelian kurang dari minimum order Jenjang ${current_member_level.name}`
      )
      return
    }

    if (step === 1) {
      setStep(2)
      return
    }

    const { subdistrict, postal_code, courier } = data

    toast.info('Submitting Data...')
    if (!process.env.NEXT_PUBLIC_APP_COOKIE_NAME) {
      toast.error('ENV VARIABLE FOR COOKIE NAME IS NOT SET!')
      return
    }

    const form_data = new FormData()

    for (const key in data) {
      form_data.append(key, data[key])
    }

    form_data.delete('quantity')
    form_data.append('quantity', getValues(`quantity_${products.data[0].id}`))

    form_data.delete('image_id_card')
    form_data.delete('courier')
    form_data.delete('postal_code')
    form_data.delete('subdistrict')
    form_data.delete('member_level_id')

    form_data.delete('shipping_address')
    form_data.append(
      'shipping_address',
      `${getValues('shipping_address')}, RT${getValues(
        'shipping_rt'
      )}, RW${getValues('shipping_rw')}, ${getValues('shipping_kelurahan')}`
    )

    form_data.append('member_level_id', watch_member_level_id)
    form_data.append('shipping_postal_code', postal_code)
    form_data.append('shipping_subdistrict_code', subdistrict)
    form_data.append('product_id', products.data[0].id)
    form_data.append('image_id_card', data.image_id_card[0])
    form_data.append(
      'products',
      JSON.stringify(
        products.data.map((product: any) => {
          return {
            product_id: product.id,
            quantity: getValues(`quantity_${product.id}`),
          }
        })
      )
    )

    if (courier) {
      const found_courier: any = courier_options.find(
        (c: any) => c.service == courier
      )
      if (!found_courier) {
        toast.error('Kurir is invalid!')
        return
      } else {
        form_data.append('shipping_courier_code', found_courier.courierCode)
        form_data.append('shipping_courier_service', found_courier.service)
      }
    }

    try {
      const response = await api.post(`/api/flow/signup`, form_data)
      console.log(response.data)
      toast.success('Register Success!')
      setCookies(process.env.NEXT_PUBLIC_APP_COOKIE_NAME, response.data.jwt)
      window.location.href = '/dashboard/overview'
    } catch (error) {
      toast.error('Register Error!')
      console.log(error)
    }
  })

  const selected_subdistrict: any = useMemo(
    () =>
      subdistrict_options.find(
        (so: any) => so.subdistrict_code == watch_subdistrict
      ),
    [watch_subdistrict]
  )

  // const selected_postal_code: any = useMemo(
  //   () =>
  //     postal_code_options.find(
  //       (so: any) => so.postal_code_code == watch_postal_code
  //     ),
  //   [watch_postal_code]
  // )

  const selected_courier: any = useMemo(
    () => courier_options.find((so: any) => so.service == watch_courier),
    [watch_courier]
  )

  // const onChangeTotalProduct = () => {
  //   let temp_total_product = 0

  //   products.data.map((product: any) => {
  //     temp_total_product += parseInt(getValues(`quantity_${product.id}`))
  //   })

  //   setTotalProduct(temp_total_product)
  // }

  // useEffect(() => {
  //   member_levels.data.map((member_level: any) => {
  //     if (total_product >= member_level.minimum_order) {
  //       setCurrentMemberLevel(member_level.id)
  //     }
  //   })
  // }, [total_product, setCurrentMemberLevel, member_levels])

  useEffect(() => {
    setValue('image_id_card', FileList)
    setValue('shipping_type', 'courier')
  }, [])

  return (
    <>
      <AuthHeader />
      <HeaderMobile />
      <div className="relative h-auto w-screen max-w-full bg-slate-50 pb-24">
        {/* <div className="absolute top-10 left-0 z-10 hidden h-auto w-1/2 lg:block">
          <Image
            src="/images/auth/signup-image.png"
            width="560.27px"
            height="583.37px"
          />
        </div> */}
        <div className="grid pb-16 lg:grid-cols-2 lg:gap-8">
          <div className="relative col-span-2 block h-124 w-screen lg:col-span-1 lg:h-full lg:w-full">
            <Image
              src={pageContentFinder(global_contents, 'register_image')}
              layout="fill"
              objectFit="cover"
              objectPosition={'center center'}
              quality={100}
            />
          </div>
          <div className="col-span-2 py-16 px-4 lg:col-span-1 lg:col-start-2 lg:pr-16">
            <h2 className="mb-6 text-3xl leading-normal">
              Apa sih keuntungan gabung jadi
              <br />
              <span className="font-semibold text-jankos-pink-300">
                Reseller Jankos Glow?
              </span>
            </h2>
            <p className="mb-10 text-base">
              Bisa hasilkan puluhan juta setiap bulan dengan modal minim? Yuk
              gabung jadi Reseller Jankos Glow, brand skincare local dengan
              potensi pasar yang menguntungkan!
            </p>
            <ul className="mb-14">
              <li className="mb-5 flex items-center justify-start">
                <span className="mr-4 flex h-10 w-10 items-center justify-center rounded-full bg-jankos-pink-300 pt-1 text-center text-white">
                  1
                </span>
                <p className="w-10/12 text-base font-semibold">
                  Produk on demand dengan target pasar dan potensi repeat order
                  yang tinggi
                </p>
              </li>
              <li className="mb-5 flex items-center justify-start">
                <span className="mr-4 flex h-10 w-10 items-center justify-center rounded-full bg-jankos-pink-300 pt-1 text-center text-white">
                  2
                </span>
                <p className="w-10/12 text-base font-semibold">
                  Brand terpercaya dan sudah direview 1.000+ orang dalam 3
                  bulan.
                </p>
              </li>
              <li className="mb-5 flex items-center justify-start">
                <span className="mr-4 flex h-10 w-10 items-center justify-center rounded-full bg-jankos-pink-300 pt-1 text-center text-white">
                  3
                </span>
                <p className="w-10/12 text-base font-semibold">
                  Potongan diskon hingga 30%/paket dan bonus ekstra setiap
                  pembelanjaan.
                </p>
              </li>
              <li className="mb-5 flex items-center justify-start">
                <span className="mr-4 flex h-10 w-10 items-center justify-center rounded-full bg-jankos-pink-300 pt-1 text-center text-white">
                  4
                </span>
                <p className="w-10/12 text-base font-semibold">
                  Modal kecil, produk berkualitas dan efektif
                </p>
              </li>
              <li className="mb-5 flex items-center justify-start">
                <span className="mr-4 flex h-10 w-10 items-center justify-center rounded-full bg-jankos-pink-300 pt-1 text-center text-white">
                  5
                </span>
                <p className="w-10/12 text-base font-semibold">
                  Dikelola oleh tim professional dengan regulasi yang ketat
                </p>
              </li>
            </ul>
            <p>
              Hanya dengan 2 langkah mudah, yuk isi form pendaftaran di bawah
              ini!
            </p>
          </div>
        </div>
        <form onSubmit={onSubmit}>
          {step === 1 && (
            <div className="container mx-auto rounded-lg bg-white p-8 shadow-lg lg:p-16">
              <p className="mb-5 w-full text-center">Langkah 1 dari 2</p>
              <h3 className="mb-12 text-center text-xl font-bold">
                Form Pendaftaran Reseller
              </h3>
              <h4 className="mb-7 font-semibold">1. Identitas Akun</h4>
              <div className="mb-12 grid grid-cols-3 gap-10 border-b border-b-slate-300 pb-20">
                <div className="col-span-3 lg:col-span-2">
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">Nama Lengkap</label>
                    </div>
                    <BasicInput
                      error={errors.name}
                      field_name="name"
                      register={register}
                      label="Nama"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                    />
                    {/* <input
                      type="text"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('name', { required: true })}
                    /> */}
                  </div>
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">No. HP</label>
                    </div>
                    <BasicInput
                      error={errors.phone}
                      field_name="phone"
                      register={register}
                      label="No. HP"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                      type="tel"
                      pattern="[0-9]{8,12}"
                      title={'Contoh nomor handphone 081308130813'}
                    />
                    {/* <input
                      type="text"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('phone', { required: true })}
                    /> */}
                  </div>
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">
                        Alamat E-mail
                      </label>
                    </div>
                    <BasicInput
                      error={errors.email}
                      field_name="email"
                      register={register}
                      label="Alamat E-mail"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                      type="email"
                    />
                    {/* <input
                      type="email"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('email', { required: true })}
                    /> */}
                  </div>
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">
                        Alamat Sosial Media
                      </label>
                    </div>
                    <BasicInput
                      error={errors.social_media_url}
                      field_name="social_media_url"
                      register={register}
                      label="Alamat Sosial Media"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                    />
                    {/* <input
                      type="text"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('social_media_url', { required: true })}
                    /> */}
                  </div>
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">
                        Alamat E-Commerce
                        <br />
                        (*Jika Ada)
                      </label>
                    </div>
                    <BasicInput
                      error={errors.marketplace_url}
                      field_name="marketplace_url"
                      register={register}
                      label="Alamat E-Commerce"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                      validation={{ required: false }}
                    />
                    {/* <input
                      type="text"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('marketplace_url', { required: true })}
                    /> */}
                  </div>
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">Kata Sandi</label>
                    </div>
                    <BasicInput
                      error={errors.password}
                      field_name="password"
                      register={register}
                      label="Kata Sandi"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                      type="password"
                    />
                    {/* <input
                      type="password"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('password', { required: true })}
                    /> */}
                  </div>
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">
                        Ketik Ulang Kata Sandi
                      </label>
                    </div>
                    <BasicInput
                      error={errors.password_confirm}
                      field_name="password_confirm"
                      register={register}
                      label="Konfirmasi Kata Sandi"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                      type="password"
                      validation={{
                        required: {
                          value: true,
                          message: `Konfirmasi Kata Sandi harus diisi!`,
                        },
                        validate: (value: string) => {
                          if (value !== watch_password) {
                            return 'Konfirmasi Kata Sandi tidak sama!'
                          }
                        },
                      }}
                    />
                    {/* <input
                      type="password"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('password_confirm', { required: true })}
                    /> */}
                  </div>
                </div>
                {/* <div className="col-span-3 rounded-lg bg-jankos-pink-100 p-8 lg:col-span-1">
                  <p className="mb-2 text-sm font-semibold">
                    Punya kode referral?
                  </p>
                  <p className="mb-8 text-xs">
                    Kode referral adalah kode yang dimiliki oleh ipsum dolor sit
                    amet, consectetur adipiscing elit, sed do
                  </p>
                  <div className="flex items-center">
                    <input
                      type="text"
                      className="mr-2 rounded border border-slate-300 bg-white py-2 px-6"
                      {...register('referral_code', { required: false })}
                    />
                    {throttled_referral_code ? (
                      loading_upline_member ? (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6 text-yellow-600"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                          strokeWidth={2}
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"
                          />
                        </svg>
                      ) : upline_member ? (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6 text-green-600"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                          strokeWidth={2}
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                          />
                        </svg>
                      ) : (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6 text-red-600"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                          strokeWidth={2}
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
                          />
                        </svg>
                      )
                    ) : null}
                  </div>
                </div> */}
              </div>
              <h4 className="mb-7 font-semibold">2. Data Pribadi</h4>
              <div className="mb-12 grid grid-cols-3 gap-10 border-b border-b-slate-300 pb-20">
                <div className="col-span-3 lg:col-span-2">
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">Nama Bank</label>
                    </div>
                    <BasicInput
                      error={errors.bank_name}
                      field_name="bank_name"
                      register={register}
                      label="Nama Bank"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                    />
                    {/* <input
                      type="text"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('bank_name', { required: true })}
                    /> */}
                  </div>
                  <div className="mb-0 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">No. Rekening</label>
                    </div>
                    <BasicInput
                      error={errors.bank_account_number}
                      field_name="bank_account_number"
                      register={register}
                      label="No. Rekening"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                    />
                    {/* <input
                      type="text"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('bank_account_number', { required: true })}
                    /> */}
                  </div>
                  <div className="mb-4 grid lg:grid-cols-3">
                    <p className="col-span-2 col-start-2 text-xs">
                      Pastikan data yang diinput sudah benar. Penggantian nomor
                      rekening hanya dapat dilakukan melalui Call Center
                      Customer Service.
                    </p>
                  </div>
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">
                        Nama Pemilik Rekening
                      </label>
                    </div>
                    <BasicInput
                      error={errors.bank_account_name}
                      field_name="bank_account_name"
                      register={register}
                      label="Nama Pemilik Rekening"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                    />
                    {/* <input
                      type="text"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('bank_account_name', { required: true })}
                    /> */}
                  </div>
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">
                        Alamat sesuai KTP
                      </label>
                    </div>
                    <BasicInput
                      error={errors.address}
                      field_name="address"
                      register={register}
                      label="Alamat"
                      container_additional_class="col-span-2"
                      label_additional_class="hidden"
                    />
                    {/* <input
                      type="text"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                      {...register('address', { required: true })}
                    /> */}
                  </div>
                  <div className="mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">
                        Unggah Foto KTP
                      </label>
                    </div>
                    <div className="relative flex h-auto flex-col items-center justify-center rounded border border-dashed border-slate-300 bg-jankos-pink-100 py-2 px-6 text-center lg:col-span-2">
                      {watch_image_id_card !== undefined &&
                        watch_image_id_card.length === 0 && (
                          <div className="py-10">
                            <p className="mb-1 text-sm">
                              Letakkan gambar di sini atau{' '}
                              <span className="font-semibold text-jankos-pink-300">
                                cari
                              </span>
                              .
                            </p>
                            <p className="mb-1 text-xs text-slate-400">
                              Max File size 2 mb
                            </p>
                          </div>
                        )}
                      {watch_image_id_card && watch_image_id_card[0] && (
                        <img
                          src={URL.createObjectURL(
                            watch_image_id_card && watch_image_id_card[0]
                          )}
                          alt=""
                          className="h-32 w-auto"
                        />
                      )}
                      <input
                        type="file"
                        className="absolute top-0 left-0 h-full w-full cursor-pointer opacity-0"
                        onChange={(e) => {
                          if (e.target.files && e.target.files.length > 0) {
                            setValue('image_id_card', e.target.files)
                            setErrorKTP(null)
                          }
                        }}
                      />
                    </div>
                    {error_ktp && (
                      <p className="mt-1 text-xs text-red-500 lg:col-start-2">
                        {error_ktp}
                      </p>
                    )}
                  </div>
                </div>
              </div>
              <h4 className="mb-7 font-semibold">3. Starter Pack</h4>
              <div className="mb-12 grid grid-cols-3 gap-10">
                <div className="col-span-3 mb-4 grid lg:grid-cols-3">
                  <div className="mb-2 flex items-center">
                    <label className="col-span-1 text-sm">
                      Starter Pack Pilihan
                    </label>
                  </div>
                  <div
                    className={`col-span-2 grid grid-cols-2 gap-4 lg:grid-cols-4`}
                  >
                    {member_levels?.data?.map((member_level: any) => (
                      <div className="" key={member_level.id}>
                        {/* <img
                          src={products.data[0].thumbnail}
                          alt=""
                          className="mb-4 h-auto w-full rounded border border-jankos-pink-100"
                        /> */}
                        <div className="flex items-start justify-start">
                          <input
                            type="radio"
                            value={member_level?.id}
                            className="mr-2 h-4 w-4 "
                            {...register('member_level_id', { required: true })}
                            // checked={current_member_level === member_level.id}
                            onChange={(e) => {
                              setValue('member_level_id', e.target.value)
                              products.data.forEach((p: any) => {
                                setValue(`quantity_${p.id}`, 0)
                              })
                            }}
                          />
                          <div className="">
                            <p className="mb-2 text-sm font-medium">
                              {member_level.name}
                            </p>
                            {/* <p className="mb-2 text-base font-semibold">
                              Rp{products.data[0].price.toLocaleString()}
                            </p> */}
                            {products.data.map((p: any) => (
                              <p className="mb-2 text-xs" key={p.id}>
                                {p.name} :
                                <br />
                                Rp
                                {p.product_level_prices
                                  .find(
                                    (plp: any) =>
                                      plp.member_level_id === member_level.id
                                  )
                                  .price.toLocaleString()}
                              </p>
                            ))}
                            <p className="mb-2 text-xs">
                              min. {member_level?.minimum_order} pack
                            </p>
                            {/* <p className="mb-2 text-xs">
                              Berat: {products.data[0].weight}g
                            </p> */}
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                {products.data.map((p: any) => (
                  <div
                    className="col-span-3 mb-2 grid lg:mb-4 lg:grid-cols-3"
                    key={p.id}
                  >
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">{p.name}</label>
                    </div>
                    <div className="grid gap-2 lg:col-span-2 lg:grid-cols-3 lg:gap-8">
                      <input
                        type="number"
                        className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6"
                        {...register(`quantity_${p.id}`, {
                          required: true,
                          value: 0,
                        })}
                        onChange={(e) => {
                          const next_value = parseInt(e.target.value)
                          if (e.target.value == '') {
                            setValue(`quantity_${p.id}`, 0)
                          }
                          if (current_member_level) {
                            let total_product_quantity = 0
                            products.data.forEach((p_2: any) => {
                              if (p_2.id != p.id) {
                                total_product_quantity += parseInt(
                                  getValues(`quantity_${p_2.id}`)
                                )
                              }
                            })
                            total_product_quantity += next_value
                            if (!next_value) {
                              setValue(`quantity_${p.id}`, 0)
                            } else if (
                              current_member_level &&
                              current_member_level.name == 'Diamond'
                            ) {
                              setValue(`quantity_${p.id}`, next_value)
                            } else if (
                              total_product_quantity <
                                next_member_level.minimum_order - 1 &&
                              next_value < next_member_level.minimum_order
                            ) {
                              setValue(`quantity_${p.id}`, next_value)
                            } else if (
                              total_product_quantity >=
                              next_member_level.minimum_order
                            ) {
                              let other_products =
                                total_product_quantity - next_value
                              setValue(
                                `quantity_${p.id}`,
                                next_member_level.minimum_order -
                                  other_products -
                                  1
                              )
                            } else {
                              // @ts-ignore
                              setValue(`quantity_${p.id}`, next_value)
                            }
                          } else {
                            setValue(`quantity_${p.id}`, 0)
                          }
                          // onChangeTotalProduct()
                        }}
                        min={0}
                      />
                      <div className="flex flex-col lg:flex-row lg:items-center lg:gap-6">
                        <label className="col-span-1 text-sm">Total</label>
                        <input
                          type="text"
                          className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6"
                          disabled
                          value={
                            watch(`quantity_${p.id}`) &&
                            watch_member_level_id &&
                            `Rp${(
                              watch(`quantity_${p.id}`) *
                              p.product_level_prices.find(
                                (plp: any) =>
                                  plp.member_level_id === watch_member_level_id
                              ).price
                            ).toLocaleString()}`
                          }
                        />
                      </div>
                    </div>
                  </div>
                ))}
                <div className="col-span-3 mb-4 grid lg:grid-cols-3">
                  <div className="hidden items-center opacity-0 lg:flex">
                    <label className="col-span-1 text-sm"></label>
                  </div>
                  <div className="col-span-2 grid gap-8 lg:grid-cols-3">
                    <input
                      type="number"
                      className="hidden rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 opacity-0 lg:block"
                    />
                    <div className="flex flex-col gap-2 lg:flex-row lg:items-center lg:gap-6">
                      <label className="col-span-1 text-sm">Total</label>
                      <input
                        type="text"
                        className="rounded-lg border border-slate-300 py-2 px-6"
                        disabled
                        style={{ backgroundColor: 'rgba(219, 219, 219, 0.8)' }}
                        value={
                          // watch(`quantity_${products.data[0].id}`) &&
                          current_member_level &&
                          // `Rp${(
                          //   watch(`quantity_${p.id}`) * p.price
                          // ).toLocaleString()}`
                          `Rp${products.data
                            .map(
                              (product: any) =>
                                watch(`quantity_${product.id}`) *
                                product.product_level_prices.find(
                                  (plp: any) =>
                                    plp.member_level_id ===
                                    watch_member_level_id
                                ).price
                            )
                            .reduce(
                              (previous_value: number, current_value: number) =>
                                previous_value + current_value,
                              0
                            )
                            .toLocaleString()}`
                        }
                      />
                    </div>
                  </div>
                </div>
                {/* <div className="col-span-3 mb-4 grid lg:grid-cols-3">
                  <div className="flex items-center">
                    <label className="col-span-1 text-sm">
                      Jumlah Pembelian
                    </label>
                  </div>
                  <div className="col-span-2 grid gap-8 lg:grid-cols-3">
                    <input
                      type="number"
                      className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6"
                      {...register('quantity', { required: true, value: 0 })}
                    />
                    <div className="grid lg:col-span-2 lg:grid-cols-2 lg:gap-8">
                      <div className="flex items-center">
                        <label className="col-span-1 text-sm">Total</label>
                      </div>
                      <input
                        type="text"
                        className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6"
                        disabled
                        value={
                          watch_quantity &&
                          `Rp${(
                            watch_quantity * products.data[0].price
                          ).toLocaleString()}`
                        }
                      />
                    </div>
                  </div>
                </div> */}
                <div className="col-span-3 mb-4 grid lg:grid-cols-3">
                  <div className="flex items-center">
                    <label className="col-span-1 text-sm">
                      Alamat Kecamatan Pengiriman
                    </label>
                  </div>
                  <Select
                    options={subdistrict_options.map((option: any) => {
                      return {
                        value: option.subdistrict_code,
                        label: `${option.subdistrict_name}, ${option.city.city_name}, ${option.city.province.province_name}`,
                      }
                    })}
                    onInputChange={(value) => setSubdistrict(value)}
                    onChange={(value: any) => {
                      setValue('subdistrict', value.value)
                      if (value.value) {
                        setErrorSubdistrict(null)
                      }
                    }}
                    isLoading={loading_subdistrict_option}
                    defaultValue={
                      selected_subdistrict
                        ? {
                            value: watch_subdistrict,
                            label: `${selected_subdistrict.subdistrict_name}, ${selected_subdistrict.city.city_name}, ${selected_subdistrict.city.province.province_name}`,
                          }
                        : null
                    }
                    className="lg:col-span-2"
                    placeholder="Ketik alamat kecamatan anda..."
                  />
                  {error_subdistrict && (
                    <p className="mt-1 text-xs text-red-500 lg:col-start-2">
                      {error_subdistrict}
                    </p>
                  )}
                </div>
                <div className="col-span-3 mb-4 grid lg:grid-cols-3">
                  <div className="flex items-center">
                    <label className="col-span-1 text-sm">Kelurahan</label>
                  </div>
                  <BasicInput
                    error={errors.shipping_kelurahan}
                    field_name="shipping_kelurahan"
                    register={register}
                    label="Nama Kelurahan"
                    container_additional_class="col-span-2"
                    label_additional_class="hidden"
                  />
                </div>
                <div className="col-span-3 mb-4 grid lg:grid-cols-3">
                  <div className="flex items-center">
                    <label className="col-span-1 text-sm">Kode Pos</label>
                  </div>
                  <Select
                    options={postal_code_options.map((option: any) => {
                      return {
                        value: option.postal_code,
                        label: option.postal_code,
                      }
                    })}
                    onChange={(value: any) => {
                      setValue('postal_code', value.value)
                      if (value.value) {
                        setErrorPostalCode(null)
                      }
                    }}
                    defaultValue={{
                      value: watch_postal_code,
                      label: watch_postal_code,
                    }}
                    className="lg:col-span-2"
                    placeholder="Pilih kode pos anda..."
                  />
                  {error_postal_code && (
                    <p className="mt-1 text-xs text-red-500 lg:col-start-2">
                      {error_postal_code}
                    </p>
                  )}
                </div>
                <div className="col-span-3 grid lg:grid-cols-3">
                  <div className="flex items-center">
                    <label className="col-span-1 text-sm">RT</label>
                  </div>
                  <BasicInput
                    error={errors.shipping_rt}
                    field_name="shipping_rt"
                    register={register}
                    label="Nomor RT"
                    container_additional_class=""
                    label_additional_class="hidden"
                    type="number"
                    validation={{
                      required: {
                        value: true,
                        message: 'Nomor RW Harus Diisi',
                      },
                      min: { value: 1, message: 'Nomor RT Invalid' },
                    }}
                  />
                  {/* <textarea
                    cols={30}
                    rows={3}
                    className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                    {...register('shipping_address', { required: true })}
                  ></textarea> */}
                </div>
                <div className="col-span-3 grid lg:grid-cols-3">
                  <div className="flex items-center">
                    <label className="col-span-1 text-sm">RW</label>
                  </div>
                  <BasicInput
                    error={errors.shipping_rw}
                    field_name="shipping_rw"
                    register={register}
                    label="Nomor RW"
                    container_additional_class=""
                    label_additional_class="hidden"
                    type="number"
                    validation={{
                      required: {
                        value: true,
                        message: 'Nomor RW Harus Diisi',
                      },
                      min: { value: 1, message: 'Nomor RW Invalid' },
                    }}
                  />
                  {/* <textarea
                    cols={30}
                    rows={3}
                    className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                    {...register('shipping_address', { required: true })}
                  ></textarea> */}
                </div>
                <div className="col-span-3 mb-4 grid lg:grid-cols-3">
                  <div className="flex items-center">
                    <label className="col-span-1 text-sm">
                      Alamat Pengiriman
                    </label>
                  </div>
                  <BasicInput
                    error={errors.shipping_address}
                    field_name="shipping_address"
                    register={register}
                    label="Silahkan masukkan alamat dengan lengkap dan benar"
                    container_additional_class="col-span-2"
                    label_additional_class="hidden"
                    textarea={true}
                  />
                  {/* <textarea
                    cols={30}
                    rows={3}
                    className="rounded-lg border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-2"
                    {...register('shipping_address', { required: true })}
                  ></textarea> */}
                </div>
                <div className="col-span-3 mb-4 grid lg:grid-cols-3">
                  <div className="flex items-center">
                    <label className="col-span-1 mb-2 text-sm">
                      Tipe Pengiriman
                    </label>
                  </div>
                  <div
                    className={`col-span-2 grid grid-cols-2 gap-4 lg:grid-cols-4`}
                  >
                    <div className="flex items-start justify-start">
                      <input
                        type="radio"
                        value={'courier'}
                        className="mr-2 h-4 w-4 bg-red-700 text-red-400"
                        {...register('shipping_type', { required: true })}
                      />
                      <div className="">
                        <p className="mb-2 text-sm font-medium">Kurir</p>
                      </div>
                    </div>
                    <div className="flex items-start justify-start">
                      <input
                        type="radio"
                        value={'pickup'}
                        className="mr-2 h-4 w-4"
                        {...register('shipping_type', { required: true })}
                        onChange={() => {
                          setErrorCourier(null)
                          setValue('shipping_type', 'pickup')
                          setValue('courier', null)
                        }}
                      />
                      <div className="">
                        <p className="mb-2 text-sm font-medium">Pickup</p>
                      </div>
                    </div>
                  </div>
                </div>

                {watch_shipping_type == 'courier' ? (
                  <div className="col-span-3 mb-4 grid lg:grid-cols-3">
                    <div className="flex items-center">
                      <label className="col-span-1 text-sm">
                        Kurir Pengiriman
                      </label>
                    </div>
                    <Select
                      options={courier_options.map((option: any) => {
                        return {
                          value: option.service,
                          label: `${option.courier} - ${
                            option.service
                          } - Rp${option.price.toLocaleString()}`,
                        }
                      })}
                      onChange={(value: any) => {
                        setValue('courier', value.value)
                        if (value.value) {
                          setErrorCourier(null)
                        }
                      }}
                      defaultValue={
                        selected_courier
                          ? {
                              value: watch_courier,
                              label: `${selected_courier.courier} - ${
                                selected_courier.service
                              } - Rp${selected_courier.price.toLocaleString()}`,
                            }
                          : null
                      }
                      className="lg:col-span-2"
                      placeholder="Pilih kurir pengiriman..."
                    />
                    {error_courier && (
                      <p className="mt-1 text-xs text-red-500 lg:col-start-2">
                        {error_courier}
                      </p>
                    )}
                  </div>
                ) : (
                  <p className="col-span-3">
                    Pengambilan barang dapat dilakukan di kantor Jankos Glow di
                    Jl Meruya Ilir No.19 Kembangan , Meruya Utara, Jakarta Barat
                    11620 Indonesia, pada hari Senin-Jumat pukul 08.00-20.00 WIB
                  </p>
                )}
              </div>
              <div className="text-right">
                <input
                  type="submit"
                  value="Berikutnya"
                  className="w-full cursor-pointer rounded-md border border-jankos-pink-300 bg-jankos-pink-300 py-3 px-20 text-sm font-semibold text-white lg:w-auto"
                />
              </div>
            </div>
          )}
          {step === 2 && (
            <div className="container mx-auto rounded-lg bg-white p-8 shadow-lg lg:p-16">
              <p className="mb-5 w-full text-center">Langkah 2 dari 2</p>
              <h3 className="mb-12 text-center text-xl font-bold">
                Ringkasan Pesanan
              </h3>
              {products.data.map((p: any) => (
                <div className="mb-5 grid w-full grid-cols-5 gap-4" key={p.id}>
                  <img
                    src={p.thumbnail}
                    alt=""
                    className="rounded border border-slate-300"
                  />
                  <div className="col-span-4 grid grid-cols-3">
                    <div className="">
                      <p className="mb-1 text-sm">{p.name}</p>
                      <p className="mb-7 font-semibold">
                        {current_member_level && current_member_level.name}
                      </p>
                      <p className="mb-2 font-semibold">
                        Rp
                        {p.product_level_prices
                          .find(
                            (plp: any) =>
                              plp.member_level_id === watch_member_level_id
                          )
                          .price.toLocaleString()}
                      </p>
                      <p className="text-xs">{p.weight} gr</p>
                    </div>
                    <p className="text-sm">
                      <span className="font-semibold">
                        x{' '}
                        {watch(`quantity_${p.id}`) &&
                          watch(`quantity_${p.id}`).toLocaleString()}
                      </span>{' '}
                      (
                      {watch(`quantity_${p.id}`) &&
                        (
                          watch(`quantity_${p.id}`) * p.weight
                        ).toLocaleString()}{' '}
                      gr)
                    </p>
                    <p className="text-right text-sm font-semibold">
                      Rp
                      {watch(`quantity_${p.id}`) &&
                        (
                          watch(`quantity_${p.id}`) *
                          p.product_level_prices.find(
                            (plp: any) =>
                              plp.member_level_id === watch_member_level_id
                          ).price
                        ).toLocaleString()}
                    </p>
                  </div>
                </div>
              ))}
              <div className="mb-16 flex w-full items-center justify-end border-y border-slate-300 py-7">
                <p className="mr-9">Subtotal</p>
                <p className="font-semibold">
                  Rp
                  {products.data
                    .map(
                      (p: any) =>
                        p.product_level_prices.find(
                          (plp: any) =>
                            plp.member_level_id === watch_member_level_id
                        ).price * watch(`quantity_${p.id}`)
                    )
                    .reduce(
                      (total_price: number, current_price: number) =>
                        total_price + current_price,
                      0
                    )
                    .toLocaleString()}
                  {watch_quantity &&
                    (watch_quantity * products.data[0].price).toLocaleString()}
                </p>
              </div>
              <div className="grid grid-cols-2 gap-8">
                <div className="w-full">
                  <h4 className="mb-4 font-semibold">Alamat Pengiriman</h4>
                  <div className="mb-12 rounded-lg bg-jankos-pink-100 p-4">
                    <p className="text-xs">
                      {watch_shipping_address}, RT{getValues('shipping_rt')}, RW
                      {getValues('shipping_rw')},{' '}
                      {getValues('shipping_kelurahan')}
                    </p>
                    <p className="mb-14 text-xs">{`${selected_subdistrict.subdistrict_name}, ${selected_subdistrict.city.city_name}, ${selected_subdistrict.city.province.province_name} ${watch_postal_code}`}</p>
                  </div>
                  <h4 className="mb-4 font-semibold">Metode Pengiriman</h4>
                  <div className="mb-12 flex items-center rounded-lg bg-jankos-pink-100 p-4">
                    <div className="mr-4 h-10 w-10">
                      <IconShipping />
                    </div>
                    {selected_courier ? (
                      <div className="">
                        <p className="text-sm font-semibold">
                          {selected_courier?.courier}
                        </p>
                        <p className="text-xs">
                          Estimasi Pengiriman: {selected_courier?.duration}
                        </p>
                        <p className="text-sm font-medium">
                          Rp{selected_courier?.price.toLocaleString()}
                        </p>
                      </div>
                    ) : (
                      <div className="">
                        <p className="text-sm font-semibold">Pickup</p>
                      </div>
                    )}
                  </div>
                </div>
                <div className="my-10 w-full rounded-lg bg-jankos-pink-100 px-6 py-9">
                  <div className="mb-2 flex items-center justify-between">
                    <p className="text-xs">Subtotal</p>
                    <p className="text-xs font-semibold">
                      Rp
                      {products.data
                        .map(
                          (p: any) =>
                            p.product_level_prices.find(
                              (plp: any) =>
                                plp.member_level_id === watch_member_level_id
                            ).price * watch(`quantity_${p.id}`)
                        )
                        .reduce(
                          (total_price: number, current_price: number) =>
                            total_price + current_price,
                          0
                        )
                        .toLocaleString()}
                    </p>
                  </div>
                  <div className="mb-2 flex items-center justify-between">
                    <p className="text-xs">Ongkos Kirim</p>
                    <p className="text-xs font-semibold">
                      Rp
                      {getValues('shipping_type') === 'courier'
                        ? selected_courier?.price.toLocaleString()
                        : 0}
                    </p>
                  </div>
                  <div className="mb-2 flex items-center justify-between">
                    <p className="text-xs">Total Tagihan</p>
                    <p className="text-xs font-semibold">
                      Rp
                      {getValues('shipping_type') === 'courier'
                        ? (
                            products.data
                              .map(
                                (p: any) =>
                                  p.product_level_prices.find(
                                    (plp: any) =>
                                      plp.member_level_id ===
                                      watch_member_level_id
                                  ).price * watch(`quantity_${p.id}`)
                              )
                              .reduce(
                                (total_price: number, current_price: number) =>
                                  total_price + current_price,
                                0
                              ) + selected_courier?.price
                          ).toLocaleString()
                        : products.data
                            .map(
                              (p: any) =>
                                p.product_level_prices.find(
                                  (plp: any) =>
                                    plp.member_level_id ===
                                    watch_member_level_id
                                ).price * watch(`quantity_${p.id}`)
                            )
                            .reduce(
                              (total_price: number, current_price: number) =>
                                total_price + current_price,
                              0
                            )
                            .toLocaleString()}
                    </p>
                  </div>
                </div>
              </div>
              <div className="flex items-center justify-between">
                <button
                  className={`w-full cursor-pointer rounded-md border border-jankos-pink-300 bg-jankos-pink-300 py-3 px-20 text-sm font-semibold text-white lg:w-auto `}
                  onClick={setStep.bind(this, 1)}
                >
                  Kembali
                </button>
                <input
                  type="submit"
                  value="Bayar Sekarang"
                  className="w-full cursor-pointer rounded-md border border-jankos-pink-300 bg-jankos-pink-300 py-3 px-20 text-sm font-semibold text-white lg:w-auto"
                />
              </div>
            </div>
          )}
        </form>
      </div>
    </>
  )
}

export async function getStaticProps() {
  const initial_member_levels = await fetcher('/api/member-levels')
  const initial_products = await fetcher('/api/products')
  const initial_global_contents = await fetcher(
    '/api/page-contents?page.eq=global'
  )
  return {
    props: {
      initial_member_levels,
      initial_products,
      initial_global_contents,
    },
    revalidate: 6000,
  }
}

export default SignUp
