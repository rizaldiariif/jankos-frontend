import type { NextPage } from 'next'
import Image from 'next/image'
import { useForm } from 'react-hook-form'
import AuthHeader from '../../components/shared/AuthHeader'
import { toast } from 'react-toastify'
import { api } from '../../helper/api'
import { useRouter } from 'next/router'
import BasicInput from '../../components/shared/BasicInput'
import HeaderMobile from '../../components/shared/HeaderMobile'

const SignIn: NextPage = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm()

  const {
    query: { change_password_token },
  } = useRouter()

  const watch_password = watch('password')

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Mengubah kata sandi...')
    const { password } = data

    try {
      const response = await api.post('/api/flow/auth/change-password', {
        new_password: password,
        change_password_token,
      })

      toast.success('Kata Sandi telah diubah!')
      setTimeout(() => {
        window.location.href = '/auth/signin'
      }, 2000)
    } catch (error: any) {
      toast.error('Change Password Error!')
      console.log(error.response)
    }
  })

  return (
    <div className="relative flex h-screen w-screen flex-col">
      <AuthHeader />
      <HeaderMobile />
      <div className="relative flex flex-grow items-center justify-center bg-red-100">
        <div className="relative z-10 h-auto bg-white p-16 lg:w-1/3">
          <p className="mb-8 text-center text-sm">
            Silahkan masukkan password baru.
          </p>
          <form onSubmit={onSubmit}>
            <div className="mb-5 grid lg:grid-cols-4">
              <div className="flex items-center">
                <label className="col-span-1 text-sm">Kata Sandi</label>
              </div>
              <BasicInput
                error={errors.password}
                field_name="password"
                register={register}
                label="Kata Sandi"
                container_additional_class="col-span-3"
                label_additional_class="hidden"
                type="password"
              />
            </div>
            <div className="mb-5 grid lg:grid-cols-4">
              <div className="flex items-center">
                <label className="col-span-1 text-sm">
                  Ketik Ulang Kata Sandi
                </label>
              </div>
              <BasicInput
                error={errors.password_confirm}
                field_name="password_confirm"
                register={register}
                label="Konfirmasi Kata Sandi"
                container_additional_class="col-span-3"
                label_additional_class="hidden"
                type="password"
                validation={{
                  required: {
                    value: true,
                    message: `Konfirmasi Kata Sandi harus diisi!`,
                  },
                  validate: (value: string) => {
                    if (value !== watch_password) {
                      return 'Konfirmasi Kata Sandi tidak sama!'
                    }
                  },
                }}
              />
            </div>
            <div className="mb-8 text-center">
              <input
                type="submit"
                value="Ubah Password"
                className="rounded-lg bg-jankos-pink-300 py-3 px-14 text-sm font-semibold text-white"
              />
            </div>
          </form>
        </div>
        <Image src="/images/bg-auth.png" layout="fill" objectFit="cover" />
      </div>
    </div>
  )
}
export default SignIn
