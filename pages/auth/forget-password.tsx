import type { NextPage } from 'next'
import Image from 'next/image'
import { useForm } from 'react-hook-form'
import AuthHeader from '../../components/shared/AuthHeader'
import { toast } from 'react-toastify'
import { api } from '../../helper/api'
import HeaderMobile from '../../components/shared/HeaderMobile'

const SignIn: NextPage = () => {
  const { register, handleSubmit } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Sending email...')
    const { email } = data

    try {
      const response = await api.post('/api/flow/auth/forget-password', {
        email,
      })

      toast.success('URL Ubah Password Sudah dikirim via Email!')
      setTimeout(() => {
        // window.location.href = '/member/auth/signin'
      }, 2000)
    } catch (error: any) {
      error.response.data.errors.map((e: any) => {
        toast.error(e.message)
      })
    }
  })

  return (
    <div className="relative flex h-screen w-screen flex-col">
      <AuthHeader />
      <HeaderMobile />
      <div className="relative flex flex-grow items-center justify-center bg-red-100">
        <div className="relative z-10 h-auto bg-white p-16 lg:w-1/3">
          <p className="mb-8 text-center text-sm">
            Silahkan masukkan email yang terdaftar.
          </p>
          <form onSubmit={onSubmit}>
            <div className="mb-5 grid lg:grid-cols-4">
              <div className="flex items-center">
                <label className="text-sm font-semibold">Email</label>
              </div>
              <input
                type="email"
                className="rounded border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-3"
                {...register('email', { required: true })}
              />
            </div>
            <div className="mb-8 text-center">
              <input
                type="submit"
                value="Kirim Email"
                className="rounded-lg bg-jankos-pink-300 py-3 px-14 text-sm font-semibold text-white"
              />
            </div>
          </form>
        </div>
        <Image src="/images/bg-auth.png" layout="fill" objectFit="cover" />
      </div>
    </div>
  )
}
export default SignIn
