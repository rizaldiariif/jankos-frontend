import axios from 'axios'
import type { NextPage } from 'next'
import Image from 'next/image'
import Link from 'next/link'
import { useForm } from 'react-hook-form'
import { setCookies } from 'cookies-next'
import AuthHeader from '../../components/shared/AuthHeader'
import { toast } from 'react-toastify'
import BasicInput from '../../components/shared/BasicInput'
import HeaderMobile from '../../components/shared/HeaderMobile'

const SignIn: NextPage = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const onSubmit = handleSubmit(async (data) => {
    toast.info('Loading...')

    if (!process.env.NEXT_PUBLIC_APP_COOKIE_NAME) {
      toast.error('ENV VARIABLE FOR COOKIE NAME IS NOT SET!')
      return
    }

    try {
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_BACKEND_URL}/api/members/auth/signin`,
        data
      )

      setCookies(process.env.NEXT_PUBLIC_APP_COOKIE_NAME, response.data.jwt)
      toast.success('Login Success!')
      window.location.href = '/dashboard/overview'
    } catch (error) {
      toast.error('Error!')
      console.log(error)
    }
  })

  return (
    <div className="relative flex h-screen w-screen flex-col">
      <AuthHeader />
      <HeaderMobile />
      <div className="relative flex flex-grow items-center justify-center bg-red-100">
        <div className="relative z-10 h-auto bg-white p-16 lg:w-1/3">
          <p className="mb-8 text-center text-sm">
            Silahkan masukkan email yang terdaftar dan kata sandi Anda.
          </p>
          <form onSubmit={onSubmit}>
            <div className="mb-5 grid lg:grid-cols-4">
              <div className="flex items-center">
                <label className="text-sm font-semibold">Email</label>
              </div>
              <input
                type="email"
                className="rounded border border-slate-300 bg-jankos-pink-100 py-2 px-6 lg:col-span-3"
                placeholder="Email"
                {...register('email')}
              />
            </div>
            <div className="mb-2 grid lg:grid-cols-4">
              <div className="flex items-center">
                <label className="text-sm font-semibold">Password</label>
              </div>
              <BasicInput
                error={errors.password}
                field_name="password"
                register={register}
                label="Kata Sandi"
                container_additional_class="col-span-3"
                label_additional_class="hidden"
                type="password"
              />
            </div>
            <div className="mb-12 text-right">
              <Link href="/auth/forget-password">
                <a className="text-xs">Lupa kata sandi?</a>
              </Link>
            </div>
            <div className="mb-8 text-center">
              <input
                type="submit"
                value="Login"
                className="rounded-lg bg-jankos-pink-300 py-3 px-14 text-sm font-semibold text-white"
              />
            </div>
            <p className="text-center text-sm font-medium">
              Belum bergabung jadi reseller? Yuk gabung{' '}
              <Link href="/auth/signup">
                <a className="font-bold text-jankos-pink-300">disini</a>
              </Link>
              .
            </p>
          </form>
        </div>
        <Image src="/images/bg-auth.png" layout="fill" objectFit="cover" />
      </div>
    </div>
  )
}
export default SignIn
