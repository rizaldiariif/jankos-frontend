import { Splide, SplideSlide } from '@splidejs/react-splide'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useMemo, useState } from 'react'
import useSWR from 'swr'
import Footer from '../../components/shared/Footer'
import Header from '../../components/shared/Header'
import HeaderMobile from '../../components/shared/HeaderMobile'
import LoadingScreen from '../../components/shared/LoadingScreen'
import { api } from '../../helper/api'
import { fetcher } from '../../helper/fetcher'

type Props = {
  initial_global_contents: any[]
}

const DetailProduct = (props: Props) => {
  const [product, setProduct] = useState<any | null>(null)
  const [active_image, setActiveImage] = useState<string | null>('')
  const [other_products, setOtherProducts] = useState([])
  // const [global_contents, setGlobalContents] = useState([])
  const { initial_global_contents } = props

  const response_global_contents = useSWR(
    '/api/page-contents?page.eq=global',
    fetcher,
    {
      fallbackData: initial_global_contents,
    }
  )

  // useMemo(() => {
  //   if (response_global_contents) {
  //     setGlobalContents(response_global_contents.data.data)
  //   }
  // }, [response_global_contents])

  const {
    query: { id },
  } = useRouter()

  useEffect(() => {
    const fetchProduct = async () => {
      if (id) {
        const {
          data: { data },
        } = await api.get(`/api/products?id.eq=${id}`)

        if (data.length > 0) {
          setProduct(data[0])
        }

        const {
          data: { data: data2 },
        } = await api.get(`/api/products?id.ne=${id}&limit=4`)

        setOtherProducts(data2)
      }
    }
    fetchProduct()
  }, [id])

  if (product === null) {
    return <LoadingScreen />
  }

  return (
    <>
      <Header page_contents={response_global_contents?.data?.data || []} />
      <HeaderMobile />
      <div className="relative h-auto w-screen max-w-full bg-white">
        <div className="container mx-auto py-14">
          <div className="mb-10 flex items-center justify-start">
            <Link href={'/'}>
              <a className="text-sm">Beranda</a>
            </Link>
            <p className="ml-5 text-sm">/</p>
            <p className="ml-5 text-sm">Produk</p>
          </div>
          <div className="mb-14 grid grid-cols-3 gap-16">
            <div className="relative col-span-3 mb-16 h-auto w-full lg:col-span-1">
              <div className="relative mb-4 aspect-square h-auto w-full">
                <img
                  src={active_image || product?.thumbnail}
                  className="mb-4 h-full w-full object-cover object-center"
                  alt=""
                />
              </div>
              <Splide
                aria-label="My Favorite Images"
                options={{
                  height: '64px',
                  autoHeight: true,
                  perPage: 4,
                  gap: '1rem',
                  perMove: 1,
                  pagination: false,
                  rewind: false,
                  breakpoints: {
                    720: {
                      perPage: 4,
                    },
                    1080: {
                      perPage: 3,
                    },
                  },
                }}
                onActive={(_, slide) => {
                  setActiveImage(slide.slide.ariaAtomic)
                }}
              >
                <SplideSlide
                  aria-atomic={product?.thumbnail}
                  onClick={() => {
                    setActiveImage(product?.thumbnail)
                  }}
                >
                  <img
                    src={product?.thumbnail}
                    alt="Image 1"
                    style={{ height: '64px', width: '64px' }}
                  />
                </SplideSlide>
                {product?.product_images.map((image: any) => (
                  <SplideSlide
                    aria-atomic={image.url}
                    onClick={() => {
                      setActiveImage(image.url)
                    }}
                  >
                    <img
                      src={image.url}
                      alt={image.name}
                      style={{ height: '64px', width: '64px' }}
                    />
                  </SplideSlide>
                ))}
              </Splide>
            </div>
            <div className="col-span-3 lg:col-span-2">
              <p className="mb-4 text-xl font-bold">{product.name}</p>
              {/* <p className="mb-1 text-xs">Harga Jual</p> */}
              {/* <p className="mb-1.5 text-xs font-semibold">
                Rp{product.price.toLocaleString()}
              </p> */}
              <p className="mb-4 text-sm font-bold">
                Berat: {product?.weight}gr
              </p>
              <p className="mb-5 font-semibold text-jankos-pink-300">
                Deskripsi Produk
              </p>
              <div
                className="mb-8 text-sm font-medium lg:w-2/3"
                dangerouslySetInnerHTML={{ __html: product?.description }}
              />

              {/* <p className="mb-1 text-xs">Harga {member.member_level.name}</p> */}
              {/* <p className="mb-6 text-sm font-semibold text-jankos-green-200">
            Rp
            {product.product_level_prices
              .find(
                (price: any) => price.member_level_id === member.member_level_id
              )
              .price.toLocaleString()}
          </p> */}
              {/* <p className="mb-1 text-sm font-medium">Quantity</p>
          <div className="mb-4 flex h-10 items-center justify-start">
            <button
              className="flex h-full items-center justify-center rounded-l border border-slate-300 bg-white px-6 text-xl font-semibold transition-colors active:bg-slate-200"
              onClick={() => setValue('quantity', getValues('quantity') - 1)}
            >
              -
            </button>
            <input
              type="number"
              className="flex h-full w-20 items-center justify-center border border-slate-300 bg-white text-center font-semibold"
              defaultValue={0}
              {...register('quantity')}
            />
            <button
              className="flex h-full items-center justify-center rounded-r border border-slate-300 bg-white px-6 text-xl font-semibold transition-colors active:bg-slate-200"
              onClick={() =>
                setValue('quantity', parseInt(getValues('quantity')) + 1)
              }
            >
              +
            </button>
          </div>
          <button
            className="flex items-center justify-start rounded bg-jankos-pink-300 py-2 px-4 font-semibold text-white"
            onClick={() => {
              addItem({
                product_id: product.id,
                quantity: getValues('quantity'),
              })
              toast.success('Added to cart!')
              setTimeout(() => {
                window.location.href = '/dashboard/product'
              }, 2000)
            }}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="mr-3 h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
              />
            </svg>
            Tambah ke Keranjang
          </button> */}
            </div>
          </div>
          {/* <div className="mb-16 w-full">
            <p className="mb-5 font-semibold">Deskripsi Produk</p>
            <p className="mb-2 text-sm font-bold">Berat: {product.weight}gr</p>
            <div
              className="mb-8 w-2/3 text-sm font-medium"
              dangerouslySetInnerHTML={{ __html: product.description }}
            />
            <p className="">{product.description}</p>
            <p className="mb-5 font-semibold">Cocok digunakan untuk</p>
            <div className="grid grid-cols-4">
          {product.suitables.map((suitable: any) => (
            <div className="flex flex-col items-center justify-center text-center">
              <div className="mb-4 flex h-20 w-20 items-center justify-center rounded bg-jankos-pink-100">
                <img src={suitable.icon} alt="" className="h-12 w-auto" />
              </div>
              <p className="text-sm font-medium">{suitable.label}</p>
            </div>
          ))}
        </div>
          </div> */}
          <div className="w-full">
            <p className="mb-4 font-semibold">Produk Favorit</p>
            <div className="grid gap-8 md:grid-cols-2 lg:grid-cols-4">
              {other_products.map((product2: any) => (
                <Link href={`/product/${product2.id}`}>
                  <a className="group relative h-auto w-full transform rounded border border-slate-200 bg-white shadow-md transition hover:-translate-y-1 hover:border-jankos-pink-300 hover:bg-jankos-pink-100 hover:shadow-lg">
                    <div className="relative aspect-square h-auto w-full border-b">
                      <Image
                        src={product2.thumbnail}
                        layout="fill"
                        objectFit="cover"
                        objectPosition={'center center'}
                      />
                    </div>
                    <div className="p-4">
                      <p className="mb-1.5 text-sm font-medium">
                        {product2.name}
                      </p>
                      {/* <p className="mb-1 text-xs">
                    Harga {member.member_level.name}
                  </p>
                  <p className="mb-1.5 text-sm font-semibold text-jankos-green-200">
                    Rp
                    {product2.product_level_prices
                      .find(
                        (price: any) =>
                          price.member_level_id === member.member_level_id
                      )
                      .price.toLocaleString()}
                  </p> */}
                    </div>
                  </a>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
      <Footer page_contents={response_global_contents?.data?.data || []} />
    </>
  )
}

export async function getStaticPaths() {
  const response = await api.get('/api/products?limit=1000')

  const {
    data: { data: products },
  } = response

  const paths = products.map((product: any) => {
    return { params: { id: product.id } }
  })

  return { paths, fallback: true }
}

export async function getStaticProps() {
  const initial_global_contents = await fetcher(
    '/api/page-contents?page.eq=global'
  )
  return {
    props: {
      initial_global_contents,
    },
    revalidate: 6000,
  }
}

export default DetailProduct
