// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'

type Data = {
  name: string
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  // console.log(req.body)
  const { endpoint, method, payload, headers } = req.body

  try {
    const response = await axios.request({
      method: method,
      url: `${process.env.NEXT_PUBLIC_BACKEND_URL}${endpoint}`,
      data: payload,
      headers,
    })

    res.status(response.status).send(response.data)
  } catch (error) {
    res.status(500).send(error)
    // @ts-ignore
    // console.log(error.response)
    // @ts-ignore
    console.log(error.response.data)
  }
}
