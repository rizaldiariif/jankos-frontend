import { format } from 'date-fns'
import type { NextPage, GetStaticProps, GetStaticPaths } from 'next'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import Footer from '../../components/shared/Footer'
import Header from '../../components/shared/Header'
import HeaderMobile from '../../components/shared/HeaderMobile'
import IconFacebookBlogShare from '../../components/shared/IconFacebookBlogShare'
import IconTwitterBlogShare from '../../components/shared/IconTwitterBlogShare'
import IconWhatsappBlogShare from '../../components/shared/IconWhatsappBlogShare'
import LoadingScreen from '../../components/shared/LoadingScreen'
import { api } from '../../helper/api'
import { fetcher } from '../../helper/fetcher'
import { pageContentFinder } from '../../helper/page-content-finder'

interface PageProps {
  initial_article: any
  initial_other_articles: any[]
  initial_page_contents: any[]
  initial_global_contents: any[]
}

const BlogDetail: NextPage<PageProps> = (props: PageProps) => {
  const {
    query: { slug },
    asPath,
    isFallback,
  } = useRouter()

  if (!slug) {
    return <LoadingScreen />
  }

  const {
    initial_article,
    initial_other_articles,
    initial_page_contents,
    initial_global_contents,
  } = props

  const { data: article } = useSWR(`/api/articles/${slug}`, fetcher, {
    fallbackData: initial_article,
  })

  const {
    data: { data: other_articles },
  } = useSWR(`/api/articles?slug.ne=${slug}`, fetcher, {
    fallbackData: initial_other_articles,
  })
  const {
    data: { data: page_contents },
  } = useSWR('/api/page-contents?page.eq=blog', fetcher, {
    fallbackData: initial_page_contents,
  })
  const {
    data: { data: global_contents },
  } = useSWR('/api/page-contents?page.eq=global', fetcher, {
    fallbackData: initial_global_contents,
  })

  if (isFallback) {
    return <LoadingScreen />
  }

  return (
    <>
      <Header page_contents={global_contents} />
      <HeaderMobile />
      <div className="container mx-auto pt-10 pb-20 text-center">
        <h1 className="mx-auto mb-8 text-center text-3xl font-bold lg:w-3/4">
          {article.title}
        </h1>
        <div className="mb-8 flex items-center justify-center">
          <Link href="/">
            <a>Beranda</a>
          </Link>
          <p className="mx-5 pt-1">{'>'}</p>
          <Link href="/blog">
            <a>Artikel</a>
          </Link>
          <p className="mx-5 pt-1">{'>'}</p>
          <p>{article.title}</p>
        </div>
        <div className="flex items-center justify-center">
          <div className="mr-7 flex items-center justify-start">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6 text-jankos-pink-300"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
              />
            </svg>
            <p className="mt-1 ml-2 text-xs leading-none">
              {format(new Date(article.createdAt), 'dd MMMM yyyy')}
            </p>
          </div>
          <div className="flex items-center justify-start">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5 text-jankos-pink-300"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                clipRule="evenodd"
              />
            </svg>{' '}
            <p className="mt-1 ml-2 text-xs leading-none">{article.author}</p>
          </div>
        </div>
      </div>
      <div className="container mx-auto">
        <div className="relative mb-11 h-96 w-full rounded-lg">
          <Image
            src={article.thumbnail}
            layout="fill"
            objectFit="cover"
            objectPosition="center"
            className="rounded-lg"
          />
        </div>
      </div>
      <div
        className="container mx-auto mb-32"
        dangerouslySetInnerHTML={{ __html: article.text }}
      >
        {/* 
        <p className="mb-8 text-sm font-medium">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
        </p>
        <div className="grid gap-8 mb-8 lg:grid-cols-2">
          <div className="relative w-full h-64 rounded-lg lg:h-full">
            <Image
              src="/images/home/article.png"
              layout="fill"
              objectFit="cover"
              objectPosition="center"
              className="rounded-lg"
            />
          </div>
          <div className="">
            <p className="mb-8 text-sm font-medium">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam
            </p>
            <p className="mb-8 text-sm font-medium">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate
            </p>
          </div>
        </div>
        <p className="mb-8 text-sm font-medium">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam{' '}
        </p> */}
      </div>
      <div className="container mx-auto mb-14 flex items-center justify-end">
        <p className="mr-3 text-lg font-semibold">
          {pageContentFinder(page_contents, 'blog_share_title')}
        </p>
        <a
          href={`whatsapp://send?text=Baca ${article.title} di sini: ${process.env.NEXT_PUBLIC_APP_URL}${asPath} !`}
          className="mr-6"
        >
          <IconWhatsappBlogShare />
        </a>
        <a
          href={`https://www.facebook.com/sharer/sharer.php?u=${process.env.NEXT_PUBLIC_APP_URL}${asPath}`}
          className="mr-6"
        >
          <IconFacebookBlogShare />
        </a>
        <a
          href={`http://twitter.com/share?text=Baca ${article.title} di sini:&url=${process.env.NEXT_PUBLIC_APP_URL}${asPath}`}
          className=""
        >
          <IconTwitterBlogShare />
        </a>
      </div>
      <div className="container mx-auto mb-20">
        <h4 className="mb-7 text-xl font-bold">
          {pageContentFinder(page_contents, 'blog_related_title')}
        </h4>
        <div className="grid grid-cols-1 gap-5 md:grid-cols-2 lg:grid-cols-3">
          {other_articles.slice(0, 3).map((other_article: any) => (
            <Link href={`/blog/${other_article.slug}`} key={other_article.id}>
              <a className="relative w-full overflow-hidden rounded-md border bg-white">
                <div className="relative h-52 w-full">
                  <Image
                    src={other_article.thumbnail}
                    layout="fill"
                    objectFit="cover"
                    objectPosition="center"
                  />
                </div>
                <div className="bg-white p-4">
                  <p className="mb-20 font-semibold">{other_article.title}</p>
                  <div className="grid w-full grid-cols-2">
                    <div className="flex items-center justify-start">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6 text-jankos-pink-300"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        strokeWidth={2}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                        />
                      </svg>
                      <p className="mt-1 ml-2 text-xs leading-none">
                        {format(
                          new Date(other_article.createdAt),
                          'dd MMMM yyyy'
                        )}
                      </p>
                    </div>
                    <div className="flex items-center justify-start">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-5 w-5 text-jankos-pink-300"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          fillRule="evenodd"
                          d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                          clipRule="evenodd"
                        />
                      </svg>{' '}
                      <p className="mt-1 ml-2 text-xs leading-none">
                        {other_article.author}
                      </p>
                    </div>
                  </div>
                </div>
              </a>
            </Link>
          ))}
        </div>
      </div>
      <Footer page_contents={global_contents} />
    </>
  )
}

export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await api.get('/api/articles/listing-slugs')

  const paths = data.map((d: any) => {
    return {
      params: {
        slug: d.slug,
      },
    }
  })

  return {
    paths,
    fallback: true,
  }
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { slug } = params as any

  const initial_article = await fetcher(`/api/articles/${slug}`)
  const initial_other_articles = await fetcher(`/api/articles?slug.ne=${slug}`)
  const initial_page_contents = await fetcher('/api/page-contents?page.eq=blog')
  const initial_global_contents = await fetcher(
    '/api/page-contents?page.eq=global'
  )

  return {
    props: {
      initial_article,
      initial_other_articles,
      initial_page_contents,
      initial_global_contents,
    },
    revalidate: 60 * 60,
  }
}

export default BlogDetail
