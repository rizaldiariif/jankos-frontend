import type { NextPage, GetStaticProps } from 'next'
import Image from 'next/image'
import Link from 'next/link'
import Footer from '../../components/shared/Footer'
import Header from '../../components/shared/Header'
import { api } from '../../helper/api'
import useSWR from 'swr'
import { fetcher } from '../../helper/fetcher'
import { format } from 'date-fns'
import { useEffect, useState } from 'react'
import { pageContentFinder } from '../../helper/page-content-finder'
import HeaderMobile from '../../components/shared/HeaderMobile'

interface PageProps {
  initial_recent_articles: any[]
  initial_page_contents: any[]
  initial_global_contents: any[]
}

const BlogIndex: NextPage<PageProps> = (props) => {
  const [keyword, setKeyword] = useState('')
  const [articles, setArticles] = useState<any[]>([])
  const [loading, setLoading] = useState(true)
  const [pagination, setPagination] = useState({
    next: null,
    prev: null,
    current: 1,
  })

  const {
    initial_recent_articles,
    initial_page_contents,
    initial_global_contents,
  } = props

  const { data: recent_articles } = useSWR(`/api/articles/recent`, fetcher, {
    fallbackData: initial_recent_articles,
  })
  const {
    data: { data: page_contents },
  } = useSWR('/api/page-contents?page.eq=blog', fetcher, {
    fallbackData: initial_page_contents,
  })
  const {
    data: { data: global_contents },
  } = useSWR('/api/page-contents?page.eq=global', fetcher, {
    fallbackData: initial_global_contents,
  })

  const fetchArticles = async (pagination: any, keyword: string) => {
    setArticles([])
    let url = `/api/articles?page=${pagination.current}&limit=6`
    if (keyword !== '') {
      url += `&title.iLike=%${keyword}%`
    }
    const { data } = await api.get(url)

    setPagination({
      ...pagination,
      next: data.pagination.next,
      prev: data.pagination.prev,
    })

    setArticles(data.data)
    setLoading(false)
  }

  useEffect(() => {
    fetchArticles(pagination, keyword)
  }, [pagination.current, keyword])

  return (
    <>
      <Header page_contents={global_contents} />
      <HeaderMobile />
      <div className="relative flex h-80 w-screen max-w-full flex-col items-center justify-center bg-white text-center lg:h-80">
        <Image layout="fill" objectFit="cover" src="/images/banner-blog.png" />
        <span className="absolute top-0 left-0 h-full w-full bg-white bg-opacity-50"></span>
        <div className="z-10 flex flex-col items-center justify-center text-center">
          <h1 className="mb-2 text-3xl font-bold">
            {pageContentFinder(page_contents, 'blog_title')}
          </h1>
          <div className="flex items-center">
            <Link href="/">
              <a>Beranda</a>
            </Link>
            <p className="mx-5 pt-1">{'>'}</p>
            <Link href="/blog">
              <a>{pageContentFinder(page_contents, 'blog_subtitle')}</a>
            </Link>
          </div>
        </div>
      </div>
      <div className="container mx-auto grid gap-6 py-7 lg:grid-cols-3">
        <div className="">
          <div className="relative mb-8 flex w-full">
            <input
              type="text"
              className="text rounded-l border py-3 px-4 text-xs"
              placeholder="Cari artikel"
              value={keyword}
              onChange={(e) => setKeyword(e.target.value)}
            />
            <button
              className="flex h-full w-14 items-center justify-center rounded-r border py-3"
              onClick={setKeyword.bind(this, '')}
            >
              x
            </button>
          </div>
          <h3 className="relative mb-1 w-min whitespace-nowrap pb-2 text-lg font-semibold">
            {pageContentFinder(page_contents, 'blog_recent_title')}
            <span className="absolute bottom-0 left-0 h-1 w-1/3 rounded-lg bg-jankos-pink-300" />
          </h3>
          {recent_articles.map((article: any) => (
            <Link href={`/blog/${article.slug}`} key={article.id}>
              <a className="flex border-b-2 py-5">
                <div className="relative mr-3 h-16 w-16 rounded">
                  <Image
                    src={article.thumbnail}
                    layout="fill"
                    objectFit="cover"
                    objectPosition="center center"
                    className="rounded"
                  />
                </div>
                <div className="flex flex-col justify-between">
                  <h4 className="text-xs font-semibold">{article.title}</h4>
                  <div className="flex items-center justify-start">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6 text-jankos-pink-300"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                      />
                    </svg>
                    <p className="mt-1 ml-2 text-xs leading-none">
                      {format(new Date(article.createdAt), 'dd MMMM yyyy')}
                    </p>
                  </div>
                </div>
              </a>
            </Link>
          ))}
        </div>
        <div className="row-start-1 grid gap-4 lg:col-span-2 lg:row-start-auto lg:grid-cols-2">
          {articles.length === 0 && 'Loading Articles'}
          {articles.map((article: any) => (
            <Link href={`/blog/${article.slug}`} key={article.id}>
              <a className="relative w-full overflow-hidden rounded-md border bg-white">
                <div className="relative h-52 w-full">
                  <Image
                    src={article.thumbnail}
                    layout="fill"
                    objectFit="cover"
                    objectPosition="center"
                  />
                </div>
                <div className="bg-white p-4">
                  <p className="mb-20 font-semibold">{article.title}</p>
                  <div className="grid w-full grid-cols-2">
                    <div className="flex items-center justify-start">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6 text-jankos-pink-300"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        strokeWidth={2}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                        />
                      </svg>
                      <p className="mt-1 ml-2 text-xs leading-none">
                        {format(new Date(article.createdAt), 'dd MMMM yyyy')}
                      </p>
                    </div>
                    <div className="flex items-center justify-start">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-5 w-5 text-jankos-pink-300"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          fillRule="evenodd"
                          d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                          clipRule="evenodd"
                        />
                      </svg>{' '}
                      <p className="mt-1 ml-2 text-xs leading-none">
                        {article.author}
                      </p>
                    </div>
                  </div>
                </div>
              </a>
            </Link>
          ))}
          {!loading && (
            <div className="mt-20 flex items-center justify-center lg:col-span-2">
              {pagination.prev && (
                <>
                  <button
                    className="flex h-10 w-10 items-center justify-center rounded-l border bg-jankos-pink-100 text-xs font-semibold"
                    onClick={() => {
                      setLoading(true)
                      setPagination({
                        ...pagination,
                        current: pagination.current - 1,
                      })
                    }}
                  >
                    {'<'}
                  </button>
                  <button
                    className="flex h-10 w-10 items-center justify-center border bg-jankos-pink-100 text-xs font-semibold"
                    onClick={() => {
                      setLoading(true)
                      setPagination({
                        ...pagination,
                        current: pagination.current - 1,
                      })
                    }}
                  >
                    {pagination.current - 1}
                  </button>
                </>
              )}
              <button
                className="flex h-10 w-10 items-center justify-center border bg-jankos-pink-200 text-xs font-bold"
                disabled
              >
                {pagination.current}
              </button>
              {pagination.next && (
                <>
                  <button
                    className="flex h-10 w-10 items-center justify-center border bg-jankos-pink-100 text-xs font-semibold"
                    onClick={() => {
                      setLoading(true)
                      setPagination({
                        ...pagination,
                        current: pagination.current + 1,
                      })
                    }}
                  >
                    {pagination.current + 1}
                  </button>
                  <button
                    className="flex h-10 w-10 items-center justify-center rounded-r border bg-jankos-pink-100 text-xs font-semibold"
                    onClick={() => {
                      setLoading(true)
                      setPagination({
                        ...pagination,
                        current: pagination.current + 1,
                      })
                    }}
                  >
                    {'>'}
                  </button>
                </>
              )}
            </div>
          )}
        </div>
      </div>
      <Footer page_contents={global_contents} />
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const { data: initial_recent_articles } = await api.get(
    `/api/articles/recent`
  )
  const initial_page_contents = await fetcher('/api/page-contents?page.eq=blog')
  const initial_global_contents = await fetcher(
    '/api/page-contents?page.eq=global'
  )

  return {
    props: {
      initial_recent_articles,
      initial_page_contents,
      initial_global_contents,
    },
  }
}

export default BlogIndex
