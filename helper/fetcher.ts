import { api } from './api'

export const fetcher = async (url: string) => {
  try {
    const { data } = await api.get(url)

    return data
  } catch (error) {
    // @ts-ignore
    console.log(error.response)
    // @ts-ignore
    console.log(error.response.data)
    return null
  }
}
