export const formatIncrement = (number: number) => {
  let new_number = `${number}`

  while (new_number.length < 12) {
    new_number = '0' + new_number
  }

  return new_number
}
