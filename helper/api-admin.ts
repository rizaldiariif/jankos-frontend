import axios from 'axios'
import { getCookie } from 'cookies-next'

const apiAdmin = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BACKEND_URL,
})

apiAdmin.interceptors.request.use(
  async (config) => {
    if (!process.env.NEXT_PUBLIC_APP_COOKIE_NAME_ADMIN) {
      return config
    }

    const token = getCookie(process.env.NEXT_PUBLIC_APP_COOKIE_NAME_ADMIN)
    config.headers = {
      Authorization: `Bearer ${token}`,
    }
    return config
  },
  (error) => {
    Promise.reject(error)
  }
)
export { apiAdmin }
