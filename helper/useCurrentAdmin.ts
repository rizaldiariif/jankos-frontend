import { useEffect, useState } from 'react'
import { apiAdmin } from './api-admin'

export const useCurrentAdmin = () => {
  const [admin, setAdmin] = useState<any>(null)
  const [loading_admin, setLoadingAdmin] = useState(true)

  useEffect(() => {
    const fetchCurrentAdmin = async () => {
      const response = await apiAdmin.get('/api/admins/auth/me')

      setAdmin(response.data.current_admin)
      setTimeout(() => {
        setLoadingAdmin(false)
      }, 1000)
    }
    fetchCurrentAdmin()
  }, [])

  return {
    admin,
    loading_admin,
  }
}
