import axios from 'axios'
import { getCookie, getCookies } from 'cookies-next'

const api = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BACKEND_URL,
})

api.interceptors.request.use(
  async (config) => {
    if (!process.env.NEXT_PUBLIC_APP_COOKIE_NAME) {
      return config
    }

    const token = getCookie(process.env.NEXT_PUBLIC_APP_COOKIE_NAME)
    config.headers = {
      Authorization: `Bearer ${token}`,
    }
    return config
  },
  (error) => {
    Promise.reject(error)
  }
)
export { api }
