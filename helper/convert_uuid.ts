const convert_uuid = (uuid: string) => {
  // @ts-ignore
  const letters = uuid
    .match(/.{2}/g)
    .map((pair) => String.fromCharCode(parseInt(pair, 16)))
  const str = letters.join('')
  return str
}

export { convert_uuid }
