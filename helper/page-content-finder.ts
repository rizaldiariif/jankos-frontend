export const pageContentFinder = (page_contents: any, key: string) => {
  const selected_page_content = page_contents.find(
    (page_content: any) => page_content.key === key
  )

  if (!selected_page_content) {
    return null
  }

  if (selected_page_content.type === 'file') {
    return selected_page_content.file
  }
  return selected_page_content.content
}
