export const downloader = (url: string, filename: string) => {
  if (!url) {
    throw new Error('Resource URL not provided! You need to provide one')
  }
  fetch(url)
    .then((response) => response.blob())
    .then((blob) => {
      const blobURL = URL.createObjectURL(blob)
      const a = document.createElement('a')
      a.href = blobURL
      a.setAttribute('style', 'display: none')
      a.setAttribute('download', filename)

      // if (filename && filename.length) a.download = name;
      document.body.appendChild(a)
      a.click()
    })
    .catch(() => {})
}
