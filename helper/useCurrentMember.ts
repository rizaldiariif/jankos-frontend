import { useEffect, useState } from 'react'
import { api } from './api'

export const useCurrentMember = () => {
  const [member, setMember] = useState<any>(null)
  const [my_orders, setMyOrders] = useState<any[]>([])
  const [my_order_detail, setMyOrderDetail] = useState<any>(null)
  const [loading_member, setLoadingMember] = useState(true)

  useEffect(() => {
    const fetchCurrentMember = async () => {
      const response = await api.get('/api/members/auth/me')

      setMember(response.data.current_member)
      setLoadingMember(false)
    }
    fetchCurrentMember()
  }, [])

  const fetchMyOrders = async (code: string) => {
    setMyOrders([])

    const response = await api.get(`/api/flow/my-orders?code=${code}`)

    setMyOrders(response.data)
  }

  const fetchMyOrderDetail = async (id: string) => {
    const response = await api.get(`/api/flow/my-order-detail?id=${id}`)

    setMyOrderDetail(response.data)
  }

  return {
    member,
    loading_member,
    my_orders,
    my_order_detail,
    fetchMyOrders,
    fetchMyOrderDetail,
  }
}
