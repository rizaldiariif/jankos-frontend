import { getCookie, setCookies } from 'cookies-next'
import { useEffect, useState } from 'react'

interface CartItem {
  product_id: string
  quantity: number
}

export const useCart = () => {
  const [cart, setCart] = useState<CartItem[]>([])

  useEffect(() => {
    let cookie_string = getCookie('my-cart')
    if (typeof cookie_string !== 'string') {
      cookie_string = '[]'
    }
    const cart_cookie = JSON.parse(cookie_string)
    setCart(cart_cookie)
  }, [])

  const addItem = (item: CartItem) => {
    const cart_copy = Array.from(cart)
    const item_index = cart_copy.findIndex(
      (i) => i.product_id === item.product_id
    )
    if (item_index >= 0) {
      let item_quantity,
        current_quantity = 0

      if (typeof item.quantity === 'string') {
        item_quantity = parseInt(item.quantity)
      } else {
        item_quantity = item.quantity
      }

      current_quantity = cart_copy[item_index].quantity
      if (typeof current_quantity === 'string') {
        current_quantity = parseInt(current_quantity)
      }

      cart_copy.splice(item_index, 1, {
        ...item,
        quantity: item_quantity + current_quantity,
      })
    } else {
      cart_copy.push(item)
    }
    setCart(cart_copy)
    setCookies('my-cart', JSON.stringify(cart_copy))
  }

  const deleteItem = (product_ids: string[]) => {
    let cart_copy = Array.from(cart)
    cart_copy = cart_copy.filter(
      (item) => !product_ids.includes(item.product_id)
    )
    setCart(cart_copy)
    setCookies('my-cart', JSON.stringify(cart_copy))
  }

  const updateItem = (item: CartItem) => {
    const cart_copy = Array.from(cart)
    const item_index = cart_copy.findIndex(
      (i) => i.product_id === item.product_id
    )
    cart_copy.splice(item_index, 1, item)
    setCart(cart_copy)
    setCookies('my-cart', JSON.stringify(cart_copy))
  }

  return {
    cart,
    addItem,
    deleteItem,
    updateItem,
  }
}
