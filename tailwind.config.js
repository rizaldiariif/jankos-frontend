const plugin = require("tailwindcss/plugin");

module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    fontFamily: {
      'sans': ['Spartan', 'ui-sans-serif', 'system-ui'],
      'secondary': ['Helvetica', 'ui-sans-serif', 'system-ui']
    },
    extend: {
      colors: {
        jankos: {
          font: {
            "100": "#ACACAC",
            "200": "#2D2D2D"
          },
          outline: {
            "100": "#D0D0D0"
          },
          green: {
            "100": "#d4e3be4f",
            "200": "#7B9A6291",
            "300": "#7B9A62",
          },
          pink: {
            "100": "#FEF8F8",
            "200": "#DFA2A4",
            "300": "#CC676A"
          },
          orange: {
            "100": "#C26B41"
          },
          yellow: {
            "100": "#f5c95d1a",
            "200": "#f5c95d1a"
          }
        },
        black: "#2D2D2D"
      },
      height: theme => ({
        "112": "28rem",
        "124": "32rem",
        "144": "36rem",
        "156": "40rem",
        "168": "44rem",
        "180": "48rem",
      }),
      width: theme => ({
        "112": "28rem",
        "124": "32rem",
        "144": "36rem",
        "156": "40rem",
        "168": "44rem",
        "180": "48rem",
      }),
    },
  },
  plugins: [
    plugin(function ({ addUtilities }) {
      const newUtilities = {
        ".container": {
          width: "min(max(325px, 90%), 540px)",
          // Breakpoints
          "@screen sm": {
            width: "min(max(540px, 90%), 668px)",
          },
          "@screen md": {
            width: "min(max(618px, 70%), 874px)",
          },
          "@screen lg": {
            width: "min(max(874px, 70%), 1130px)",
          },
          "@screen xl": {
            width: "min(max(1130px, 70%), 1386px)",
          },
          "@screen 2xl": {
            width: "min(max(1386px, 70%), 1570px)",
          },
        }
      };
      addUtilities(newUtilities, ["responsive"]);
    })
  ],
}
