import Link from 'next/link'
import React, { useState } from 'react'
import IconMain from './IconMain'

type Props = {}

const HeaderMobile = (props: Props) => {
  const [showHamburger, setshowHamburger] = useState(false)
  return (
    <>
      <div className="relative block w-screen max-w-full bg-white lg:hidden">
        <div className="container mx-auto flex items-center justify-between py-4">
          <Link href="/">
            <a>
              <IconMain />
              {/* <img
              className="h-6 w-auto"
              src={pageContentFinder(page_contents, 'header_logo')}
              alt=""
            /> */}
            </a>
          </Link>
          <button onClick={setshowHamburger.bind(this, true)}>
            <svg
              width="24"
              height="17"
              viewBox="0 0 24 17"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect
                width="24"
                height="3"
                rx="1.5"
                fill="#CC676A"
                fillOpacity="0.65"
              />
              <rect
                y="7"
                width="24"
                height="3"
                rx="1.5"
                fill="#CC676A"
                fillOpacity="0.65"
              />
              <rect
                y="14"
                width="24"
                height="3"
                rx="1.5"
                fill="#CC676A"
                fillOpacity="0.65"
              />
            </svg>
          </button>
        </div>
      </div>
      <div
        className={`absolute top-0 left-0 z-50 h-screen w-screen transform bg-white transition-transform ${
          !showHamburger && '-translate-x-full'
        }`}
      >
        <div className="relative w-screen max-w-full bg-white">
          <div className="container mx-auto flex items-center justify-between py-4">
            <Link href="/">
              <a>
                <IconMain />
                {/* <img
              className="h-6 w-auto"
              src={pageContentFinder(page_contents, 'header_logo')}
              alt=""
            /> */}
              </a>
            </Link>
            <button onClick={setshowHamburger.bind(this, false)}>
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g clipPath="url(#clip0_2004_3695)">
                  <path
                    d="M13.4658 12.0136L19.6959 5.78336C20.1014 5.3781 20.1014 4.72287 19.6959 4.31761C19.2907 3.91236 18.6354 3.91236 18.2302 4.31761L11.9999 10.5479L5.76983 4.31761C5.36438 3.91236 4.70934 3.91236 4.30408 4.31761C3.89864 4.72287 3.89864 5.3781 4.30408 5.78336L10.5342 12.0136L4.30408 18.2439C3.89864 18.6492 3.89864 19.3044 4.30408 19.7096C4.50604 19.9118 4.77159 20.0133 5.03695 20.0133C5.30232 20.0133 5.56768 19.9118 5.76983 19.7096L11.9999 13.4794L18.2302 19.7096C18.4323 19.9118 18.6977 20.0133 18.9631 20.0133C19.2284 20.0133 19.4938 19.9118 19.6959 19.7096C20.1014 19.3044 20.1014 18.6492 19.6959 18.2439L13.4658 12.0136Z"
                    fill="#CC676A"
                    fillOpacity="0.65"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_2004_3695">
                    <rect
                      width="16"
                      height="16"
                      fill="white"
                      transform="translate(4 4)"
                    />
                  </clipPath>
                </defs>
              </svg>
            </button>
          </div>
          <ul className="container mx-auto mt-4 flex flex-col justify-start py-4">
            <li className="mb-4" onClick={setshowHamburger.bind(this, false)}>
              <Link href={'/#tentang-kami'}>
                <a className="font-semibold">Tentang Kami</a>
              </Link>
            </li>
            <li className="mb-4" onClick={setshowHamburger.bind(this, false)}>
              <Link href={'/#produk'}>
                <a className="font-semibold">Produk</a>
              </Link>
            </li>
            <li className="mb-4" onClick={setshowHamburger.bind(this, false)}>
              <Link href={'/blog'}>
                <a className="font-semibold">Blog</a>
              </Link>
            </li>
            <li className="mb-4" onClick={setshowHamburger.bind(this, false)}>
              <Link href={'/#hubungi-kami'}>
                <a className="font-semibold">Hubungi Kami</a>
              </Link>
            </li>
            <li className="mb-4" onClick={setshowHamburger.bind(this, false)}>
              <Link href={'/auth/signin'}>
                <a className="font-semibold text-jankos-pink-300">Login</a>
              </Link>
            </li>
            <li className="mb-4" onClick={setshowHamburger.bind(this, false)}>
              <Link href={'/auth/signup'}>
                <a className="font-semibold text-jankos-pink-300">Register</a>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </>
  )
}

export default HeaderMobile
