import { Axios } from 'axios'
import React, { useEffect, useState } from 'react'
import DataTable, { TableColumn } from 'react-data-table-component'
import { useForm } from 'react-hook-form'
import BasicInput, { InputItemOption } from './BasicInput'

type Props = {
  title?: string
  columns: TableColumn<any>[]
  url: string
  api: Axios
  additional_parameter?: string
  filter_options?: InputItemOption[]
}

const BasicDataTable = (props: Props) => {
  const [data, setData] = useState<any[]>([])
  const [total_row, setTotalRow] = useState(20)
  const [per_page, setPerpage] = useState(20)
  const [loading, setLoading] = useState(true)
  const [filter_column, setFilterColumn] = useState<string | null>(null)
  const [filter_value, setFilterValue] = useState<string | null>(null)

  const {
    title,
    columns,
    url,
    api,
    additional_parameter,
    filter_options = [],
  } = props

  const {
    formState: { errors },
    register,
    handleSubmit,
  } = useForm()

  const fetchData = async (page: number) => {
    setLoading(true)
    let url_param = `${url}?page=${page}&limit=${per_page}${additional_parameter}`
    if (filter_column && filter_value) {
      url_param = url_param + `&${filter_column}.iLike=%${filter_value}%`
    }
    const {
      data: { data, total_data },
    } = await api.get(url_param)
    setData(data)
    setTotalRow(total_data)
    setLoading(false)
  }

  const handlePageChange = (page: number) => {
    fetchData(page)
  }

  const handlePerRowsChange = (newPerPage: number, page: number) => {
    setPerpage(newPerPage)
    fetchData(page)
  }

  const onFilter = handleSubmit(async (data) => {
    const {
      filter_column: input_filter_column,
      filter_value: input_filter_value,
    } = data
    setFilterColumn(input_filter_column)
    setFilterValue(input_filter_value)
  })

  useEffect(() => {
    fetchData(1)
  }, [filter_column, filter_value, additional_parameter])

  return (
    <div className="bg-white">
      {filter_options.length > 0 && (
        <form
          onSubmit={onFilter}
          className="mb-2 ml-auto flex w-96 gap-6 rounded px-4 pt-8"
        >
          <BasicInput
            error={errors.filter_column}
            field_name="filter_column"
            label="Column"
            register={register}
            option={true}
            options={filter_options}
            validation={{ required: false }}
          />
          <BasicInput
            error={errors.filter_value}
            field_name="filter_value"
            label="Value"
            register={register}
            validation={{ required: false }}
          />
          <button
            type="submit"
            className="bg-namaku-blue-200 block w-min whitespace-nowrap rounded py-2 px-6 text-xs text-white"
          >
            Filter
          </button>
        </form>
      )}
      <DataTable
        title={title}
        columns={columns}
        data={data}
        progressPending={loading}
        pagination
        paginationServer
        paginationTotalRows={total_row}
        paginationDefaultPage={1}
        paginationPerPage={per_page}
        onChangeRowsPerPage={handlePerRowsChange}
        onChangePage={handlePageChange}
      />
    </div>
  )
}

export default BasicDataTable
