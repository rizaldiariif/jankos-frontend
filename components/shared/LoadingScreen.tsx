import Link from 'next/link'
import React from 'react'

type Props = {}

const LoadingIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      style={{
        margin: 'auto',
        background: 'none',
        display: 'block',
        shapeRendering: 'auto',
      }}
      width="96px"
      height="96px"
      className="h-full w-auto"
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
    >
      <circle cx="84" cy="50" r="10" fill="#cc676a">
        <animate
          attributeName="r"
          repeatCount="indefinite"
          dur="0.8333333333333334s"
          calcMode="spline"
          keyTimes="0;1"
          values="10;0"
          keySplines="0 0.5 0.5 1"
          begin="0s"
        ></animate>
        <animate
          attributeName="fill"
          repeatCount="indefinite"
          dur="3.3333333333333335s"
          calcMode="discrete"
          keyTimes="0;0.25;0.5;0.75;1"
          values="#cc676a;#d0d0d0;#fef8f8;#dfa2a4;#cc676a"
          begin="0s"
        ></animate>
      </circle>
      <circle cx="16" cy="50" r="10" fill="#cc676a">
        <animate
          attributeName="r"
          repeatCount="indefinite"
          dur="3.3333333333333335s"
          calcMode="spline"
          keyTimes="0;0.25;0.5;0.75;1"
          values="0;0;10;10;10"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1"
          begin="0s"
        ></animate>
        <animate
          attributeName="cx"
          repeatCount="indefinite"
          dur="3.3333333333333335s"
          calcMode="spline"
          keyTimes="0;0.25;0.5;0.75;1"
          values="16;16;16;50;84"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1"
          begin="0s"
        ></animate>
      </circle>
      <circle cx="50" cy="50" r="10" fill="#dfa2a4">
        <animate
          attributeName="r"
          repeatCount="indefinite"
          dur="3.3333333333333335s"
          calcMode="spline"
          keyTimes="0;0.25;0.5;0.75;1"
          values="0;0;10;10;10"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1"
          begin="-0.8333333333333334s"
        ></animate>
        <animate
          attributeName="cx"
          repeatCount="indefinite"
          dur="3.3333333333333335s"
          calcMode="spline"
          keyTimes="0;0.25;0.5;0.75;1"
          values="16;16;16;50;84"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1"
          begin="-0.8333333333333334s"
        ></animate>
      </circle>
      <circle cx="84" cy="50" r="10" fill="#ACACAC">
        <animate
          attributeName="r"
          repeatCount="indefinite"
          dur="3.3333333333333335s"
          calcMode="spline"
          keyTimes="0;0.25;0.5;0.75;1"
          values="0;0;10;10;10"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1"
          begin="-1.6666666666666667s"
        ></animate>
        <animate
          attributeName="cx"
          repeatCount="indefinite"
          dur="3.3333333333333335s"
          calcMode="spline"
          keyTimes="0;0.25;0.5;0.75;1"
          values="16;16;16;50;84"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1"
          begin="-1.6666666666666667s"
        ></animate>
      </circle>
      <circle cx="16" cy="50" r="10" fill="#d0d0d0">
        <animate
          attributeName="r"
          repeatCount="indefinite"
          dur="3.3333333333333335s"
          calcMode="spline"
          keyTimes="0;0.25;0.5;0.75;1"
          values="0;0;10;10;10"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1"
          begin="-2.5s"
        ></animate>
        <animate
          attributeName="cx"
          repeatCount="indefinite"
          dur="3.3333333333333335s"
          calcMode="spline"
          keyTimes="0;0.25;0.5;0.75;1"
          values="16;16;16;50;84"
          keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1"
          begin="-2.5s"
        ></animate>
      </circle>
    </svg>
  )
}

const LoadingScreen = (props: Props) => {
  return (
    <div className="fixed top-0 left-0 flex h-screen w-screen flex-col">
      {/* <div className="flex-shrink bg-white py-4">
        <div className="container mx-auto grid grid-cols-3 items-center">
          <Link href={'/'}>
            <a className="w-min whitespace-nowrap rounded-lg border border-jankos-pink-300 py-3 px-8 text-sm font-semibold text-jankos-pink-300">
              Kembali ke Beranda
            </a>
          </Link>
          <div className="text-center">
            <img
              className="mx-auto h-full w-auto"
              src="/images/shared/logo.png"
              alt=""
            />
          </div>
        </div>
      </div> */}
      <div className="flex flex-grow flex-col items-center justify-center bg-jankos-pink-100">
        <div className="h-24 w-auto">
          <LoadingIcon />
        </div>
        <p className="text-xl font-bold">LOADING</p>
        <p className="text-xs font-semibold">Mohon menunggu, ya!</p>
      </div>
    </div>
  )
}

export default LoadingScreen
