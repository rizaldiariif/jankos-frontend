import { format } from 'date-fns'
import React from 'react'

export type ItemDetailProps = {
  key: string
  type: 'text' | 'currency' | 'image' | 'date' | 'wysiwyig' | 'url'
  label: string
}

type Props = {
  item: any
  item_details: ItemDetailProps[]
}

const renderContent = (type: string, value: string) => {
  switch (type) {
    case 'text':
      return <p>{value}</p>
    case 'currency':
      return <p>{`Rp${value}`}</p>
    case 'date':
      return <p>{format(new Date(value), 'dd-MM-yyyy')}</p>
    case 'image':
      return (
        <div className="h-24">
          <img className="h-full w-auto rounded border p-1" src={value} />
        </div>
      )
    case 'wysiwyig':
      return (
        <div
          className="wysiwyig-container"
          dangerouslySetInnerHTML={{ __html: value }}
        />
      )
    case 'url':
      return (
        <a
          className="w-min whitespace-nowrap rounded-md bg-jankos-pink-300 py-1 px-6 text-xs font-semibold text-white"
          target={'_blank'}
          href={value}
        >
          URL
        </a>
      )
    default:
      return null
  }
}

const BasicDetail = (props: Props) => {
  const { item, item_details } = props

  return (
    <div className="grid grid-cols-3 gap-6">
      {item_details.map((item_detail) => (
        <div className="flex flex-col">
          <h3 className="mb-1 text-sm font-semibold">{item_detail.label}</h3>
          {renderContent(item_detail.type, item[item_detail.key])}
        </div>
      ))}
    </div>
  )
}

export default BasicDetail
