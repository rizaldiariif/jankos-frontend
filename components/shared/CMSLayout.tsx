import { removeCookies } from 'cookies-next'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { apiAdmin } from '../../helper/api-admin'
import { useCurrentAdmin } from '../../helper/useCurrentAdmin'
import LoadingScreen from './LoadingScreen'

type Props = {}

const CMSLayout: React.FC<Props> = ({ children }) => {
  const { admin } = useCurrentAdmin()
  const { pathname } = useRouter()

  const [phone_number, setPhoneNumber] = useState('')

  useEffect(() => {
    const fetchPhoneNumber = async () => {
      const {
        data: { data },
      } = await apiAdmin.get('/api/page-contents?key.eq=footer_phone')
      if (data.length > 0) {
        setPhoneNumber(data[0].content)
      }
    }
    fetchPhoneNumber()
  }, [])

  if (!admin) {
    return <LoadingScreen />
  }

  const onSignOut = () => {
    if (!process.env.NEXT_PUBLIC_APP_COOKIE_NAME_ADMIN) {
      return
    }

    if (window.confirm('Are you sure?')) {
      removeCookies(process.env.NEXT_PUBLIC_APP_COOKIE_NAME_ADMIN)
      window.location.href = '/cms/auth/signin'
    }
  }

  return (
    <div className="relative grid h-screen w-screen overflow-x-hidden lg:grid-cols-6">
      <div className="hidden h-full w-full bg-jankos-pink-300 p-6 lg:block">
        <img
          src="/images/shared/logo-white.png"
          alt=""
          className="mb-8 h-auto w-full"
        />
        <ul>
          {admin.admin_level.hierarchy_number >= 2 && (
            <>
              <li className="mb-4 text-sm">
                <Link href="/cms/order">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'order'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="mr-4 h-5 w-5"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
                      />
                    </svg>
                    Pesanan
                  </a>
                </Link>
              </li>
            </>
          )}
          {admin.admin_level.hierarchy_number >= 5 && (
            <li className="mb-4 text-sm">
              <Link href="/cms/product">
                <a
                  className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                    pathname.split('/')[2] == 'product'
                      ? 'font-semibold opacity-100'
                      : 'opacity-75'
                  }`}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="mr-4 h-5 w-5"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10"
                    />
                  </svg>
                  Produk
                </a>
              </Link>
            </li>
          )}
          {admin.admin_level.hierarchy_number >= 100 && (
            <>
              <li className="mb-4 text-sm">
                <Link href="/cms/marketing-material">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'marketing-material'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="mr-4 h-5 w-5"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                      />
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                    </svg>
                    Bahan Marketing
                  </a>
                </Link>
              </li>
              <li className="mb-4 text-sm">
                <Link href="/cms/member">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'member'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="mr-4 h-5 w-5"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"
                      />
                    </svg>
                    Member
                  </a>
                </Link>
              </li>
              <li className="mb-4 text-sm">
                <Link href="/cms/member-level">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'member-level'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="mr-4 h-5 w-5"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"
                      />
                    </svg>
                    Jenjang Member
                  </a>
                </Link>
              </li>
              <li className="mb-4 text-sm">
                <Link href="/cms/article">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'article'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 512 512"
                      xmlSpace="preserve"
                      className="mr-4 h-5 w-5"
                    >
                      <path
                        d="M469.3 192H362.7V85.3c0-35.3-28.7-64-64-64H85.3c-35.3 0-64 28.7-64 64V384c0 58.9 47.8 106.7 106.7 106.7h277.3c47.1 0 85.3-38.2 85.3-85.3v-192c.1-11.8-9.5-21.4-21.3-21.4zM64 384V85.3C64 73.6 73.6 64 85.3 64h213.3c11.8 0 21.3 9.6 21.3 21.3v320c.1 15 4.2 29.8 11.9 42.7H128c-35.3 0-64-28.7-64-64zm384 21.3c0 23.6-19.1 42.7-42.7 42.7s-42.7-19.1-42.7-42.7V234.7H448v170.6zM106.7 192h170.7v42.7H106.7V192zm0 85.3h170.7V320H106.7v-42.7z"
                        style={{
                          fill: '#fef8f8',
                        }}
                      />
                    </svg>{' '}
                    Artikel
                  </a>
                </Link>
              </li>
              <li className="mb-4 text-sm">
                <Link href="/cms/testimony">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'testimony'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 512 512"
                      xmlSpace="preserve"
                      className="mr-4 h-5 w-5"
                    >
                      <path
                        d="M151.4 469.5c-23.7-.1-42.9-19.4-42.8-43.2 0-2.3.2-4.5.6-6.7l16.5-101.7-70.8-73.1c-16.5-17-16.1-44.2.9-60.8 6.4-6.2 14.5-10.3 23.3-11.6l95.9-14.7 42.3-90.3C227.7 46 253.5 37.1 275 47.5c8.7 4.2 15.7 11.2 19.9 19.9l42.1 90.2 96 14.8c23.4 3.6 39.5 25.6 35.8 49.1-1.4 8.8-5.4 16.9-11.6 23.3l-70.7 72.8 16.4 102c3.8 23.4-12 45.4-35.3 49.2-9.5 1.6-19.3-.1-27.7-4.8l-83.8-46.5-83.9 46.5c-6.4 3.6-13.5 5.5-20.8 5.5zM256 374.8c7.3 0 14.4 1.9 20.7 5.4l83.8 46.5-16.3-102c-2.1-13.4 2.1-27.1 11.6-36.8l70.7-72.8-96-15.2c-14.2-2.2-26.3-11.3-32.3-24.3l-42.1-90.1c0-.1-.1-.1-.1-.2l-42.3 90.4c-6 13-18.1 22.1-32.3 24.2l-95.9 14.7 70.7 73.3c9.4 9.8 13.7 23.4 11.6 36.8l-16.5 101.7 84-46.2c6.3-3.5 13.4-5.4 20.7-5.4z"
                        style={{
                          fill: '#fef8f8',
                        }}
                      />
                    </svg>{' '}
                    Testimoni
                  </a>
                </Link>
              </li>
              <li className="mb-4 text-sm">
                <Link href="/cms/reason-item">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'reason-item'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 512 512"
                      xmlSpace="preserve"
                      className="mr-4 h-5 w-5"
                    >
                      <path
                        d="M100 113c0-11 9-20 20-20h272c11 0 20 9 20 20s-9 20-20 20H120c-11 0-20-9-20-20zm312 80c0-11-9-20-20-20H120c-11 0-20 9-20 20s9 20 20 20h272c11 0 20-9 20-20zm-292 60c-11 0-20 9-20 20s9 20 20 20h92c11 0 20-9 20-20s-9-20-20-20h-92zm374.4 156.6c23.4 23.4 23.4 61.4 0 84.8-11.7 11.7-27 17.6-42.4 17.6s-30.7-5.9-42.4-17.5L300.2 384.9c-2.4-2.4-4.2-5.5-5.1-8.8l-22.4-80.7c-1.9-7 .1-14.6 5.3-19.7s12.8-6.9 19.8-4.8l78.7 23.9c3.2 1 6 2.7 8.3 5l109.6 109.8zm-162.2-49.3 73.5 73.7 28.3-28.3-73.9-74.1-39.1-11.9 11.2 40.6zm133.9 77.6-3.9-3.9-28.2 28.3 3.9 3.9c7.8 7.8 20.5 7.8 28.3 0 7.7-7.9 7.7-20.5-.1-28.3zM432 0H80C35.9 0 0 35.9 0 80v352c0 44.1 35.9 80 80 80h245c11 0 20-9 20-20s-9-20-20-20H80c-22.1 0-40-17.9-40-40V80c0-22.1 17.9-40 40-40h352c22.1 0 40 17.9 40 40v246c0 11 9 20 20 20s20-9 20-20V80c0-44.1-35.9-80-80-80z"
                        style={{
                          fill: '#fef8f8',
                        }}
                      />
                    </svg>{' '}
                    Gambar Kenapa Jankos
                  </a>
                </Link>
              </li>
              <li className="mb-4 text-sm">
                <Link href="/cms/admin-level">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'admin-level'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="mr-4 h-5 w-5"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"
                      />
                    </svg>
                    Jabatan Admin
                  </a>
                </Link>
              </li>
              <li className="mb-4 text-sm">
                <Link href="/cms/admin">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'admin'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="mr-4 h-5 w-5"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                    Admin
                  </a>
                </Link>
              </li>
              <li className="mb-4 text-sm">
                <Link href="/cms/page-content">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'page-content'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 512 512"
                      xmlSpace="preserve"
                      className="mr-4 h-5 w-5"
                    >
                      <path
                        d="M100 113c0-11 9-20 20-20h272c11 0 20 9 20 20s-9 20-20 20H120c-11 0-20-9-20-20zm312 80c0-11-9-20-20-20H120c-11 0-20 9-20 20s9 20 20 20h272c11 0 20-9 20-20zm-292 60c-11 0-20 9-20 20s9 20 20 20h92c11 0 20-9 20-20s-9-20-20-20h-92zm374.4 156.6c23.4 23.4 23.4 61.4 0 84.8-11.7 11.7-27 17.6-42.4 17.6s-30.7-5.9-42.4-17.5L300.2 384.9c-2.4-2.4-4.2-5.5-5.1-8.8l-22.4-80.7c-1.9-7 .1-14.6 5.3-19.7s12.8-6.9 19.8-4.8l78.7 23.9c3.2 1 6 2.7 8.3 5l109.6 109.8zm-162.2-49.3 73.5 73.7 28.3-28.3-73.9-74.1-39.1-11.9 11.2 40.6zm133.9 77.6-3.9-3.9-28.2 28.3 3.9 3.9c7.8 7.8 20.5 7.8 28.3 0 7.7-7.9 7.7-20.5-.1-28.3zM432 0H80C35.9 0 0 35.9 0 80v352c0 44.1 35.9 80 80 80h245c11 0 20-9 20-20s-9-20-20-20H80c-22.1 0-40-17.9-40-40V80c0-22.1 17.9-40 40-40h352c22.1 0 40 17.9 40 40v246c0 11 9 20 20 20s20-9 20-20V80c0-44.1-35.9-80-80-80z"
                        style={{
                          fill: '#fef8f8',
                        }}
                      />
                    </svg>{' '}
                    Konten Halaman
                  </a>
                </Link>
              </li>
              <li className="mb-4 text-sm">
                <Link href="/cms/banner-home">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'banner-home'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      id="Layer_1"
                      xmlns="http://www.w3.org/2000/svg"
                      x={0}
                      y={0}
                      viewBox="0 0 512 512"
                      xmlSpace="preserve"
                      className="mr-4 h-5 w-5"
                    >
                      <style>{'.st0{fill:#fef8f8}'}</style>
                      <g id="Layer_2">
                        <path
                          className="st0"
                          d="M405.3 485.3H128c-32.4 0-58.6-26.3-58.7-58.7V85.3c0-32.4 26.3-58.6 58.7-58.7h195.2c15.9 0 31.1 6.5 42.2 17.9l82.1 84.8c10.6 10.9 16.5 25.6 16.5 40.8v256.6c0 32.4-26.3 58.6-58.7 58.6zM128 58.7c-14.7 0-26.6 11.9-26.7 26.7v341.3c0 14.7 11.9 26.6 26.7 26.7h277.3c14.7 0 26.6-11.9 26.7-26.7V170.1c.1-6.9-2.6-13.6-7.5-18.6l-82.1-84.7c-5.1-5.2-12-8.1-19.2-8.1H128z"
                        />
                        <path
                          className="st0"
                          d="M442.5 165.3H384c-32.4 0-58.6-26.3-58.7-58.7V46.9c0-8.8 7.2-16 16-16s16 7.2 16 16v59.7c0 14.7 11.9 26.6 26.7 26.7h58.5c8.8 0 16 7.2 16 16 0 8.9-7.2 16-16 16z"
                        />
                      </g>
                    </svg>{' '}
                    Banner
                  </a>
                </Link>
              </li>
              <li className="mb-4 text-sm">
                <Link href="/cms/certification">
                  <a
                    className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                      pathname.split('/')[2] == 'certification'
                        ? 'font-semibold opacity-100'
                        : 'opacity-75'
                    }`}
                  >
                    <svg
                      id="Capa_1"
                      xmlns="http://www.w3.org/2000/svg"
                      x={0}
                      y={0}
                      viewBox="0 0 512 512"
                      xmlSpace="preserve"
                      className="mr-4 h-5 w-5"
                    >
                      <style>{'.st0{fill:#fef8f8}'}</style>
                      <path
                        className="st0"
                        d="m446.6 124.4-120-120C323.8 1.6 320 0 316 0H106C81.2 0 61 20.2 61 45v422c0 24.8 20.2 45 45 45h300c24.8 0 45-20.2 45-45V135c0-4.1-1.7-7.9-4.4-10.6zM331 51.2l68.8 68.8H346c-8.3 0-15-6.7-15-15V51.2zM406 482H106c-8.3 0-15-6.7-15-15V45c0-8.3 6.7-15 15-15h195v75c0 24.8 20.2 45 45 45h75v317c0 8.3-6.7 15-15 15z"
                      />
                      <path
                        className="st0"
                        d="M346 212H166c-8.3 0-15 6.7-15 15s6.7 15 15 15h180c8.3 0 15-6.7 15-15s-6.7-15-15-15zM346 272H166c-8.3 0-15 6.7-15 15s6.7 15 15 15h180c8.3 0 15-6.7 15-15s-6.7-15-15-15zM346 332H166c-8.3 0-15 6.7-15 15s6.7 15 15 15h180c8.3 0 15-6.7 15-15s-6.7-15-15-15zM286 392H166c-8.3 0-15 6.7-15 15s6.7 15 15 15h120c8.3 0 15-6.7 15-15s-6.7-15-15-15z"
                      />
                    </svg>{' '}
                    Sertifikasi
                  </a>
                </Link>
              </li>
            </>
          )}
          {/* <li className="mb-4 text-sm">
            <Link href="/cms/commission">
              <a className="flex items-center text-white opacity-75 transition hover:font-semibold hover:opacity-100">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="mr-4 h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z"
                  />
                </svg>
                Komisi Member
              </a>
            </Link>
          </li>
          <li className="mb-4 text-sm">
            <Link href="/cms/withdrawal">
              <a className="flex items-center text-white opacity-75 transition hover:font-semibold hover:opacity-100">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="mr-4 h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z"
                  />
                </svg>
                Penarikan Komisi
              </a>
            </Link>
          </li> */}
        </ul>
      </div>
      <div className="flex flex-col overflow-hidden lg:col-span-5">
        <div className="flex w-full items-center justify-between bg-jankos-pink-100 py-5 px-9">
          <div className="">
            <h3 className="mb-1 text-xl font-bold">Halo {admin.name}!</h3>
            <p>{admin.admin_level.name}</p>
          </div>
          <div className="flex items-center">
            <button onClick={onSignOut}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth={2}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                />
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                />
              </svg>
            </button>
          </div>
        </div>
        <div className="relative w-full flex-grow overflow-hidden overflow-y-scroll bg-white p-6">
          {children}
        </div>
      </div>
      <a
        href={`https://wa.me/${phone_number}`}
        className="floating-animation absolute bottom-8 right-8 flex h-12 w-12 items-center justify-center rounded-full bg-green-500"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-8 w-8 text-white"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
          strokeWidth={2}
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z"
          />
        </svg>
      </a>
    </div>
  )
}

export default CMSLayout
