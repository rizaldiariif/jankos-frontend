import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import IconMain from './IconMain'

type Props = {
  show_login?: boolean
}

const AuthHeader: React.FC<Props> = ({ show_login = true }) => {
  const { pathname } = useRouter()

  return (
    <div className="relative hidden w-screen max-w-full bg-white lg:block">
      <div className="container mx-auto flex items-center justify-between py-4">
        <Link href="/">
          <a className="rounded-lg border border-jankos-pink-300 bg-transparent py-2 px-6 text-base font-semibold text-jankos-pink-300 transition-colors hover:bg-jankos-pink-300 hover:text-white">
            Kembali ke Beranda
          </a>
        </Link>
        <Link href="/">
          <a>
            <IconMain />
            {/* <img
              className="h-full w-auto"
              src="/images/shared/logo.png"
              alt=""
            /> */}
          </a>
        </Link>
        {pathname !== '/auth/signin' && show_login ? (
          <Link href="/auth/signin">
            <a className="hidden text-base font-semibold lg:block">
              Sudah punya akun?{' '}
              <span className="text-jankos-pink-300">Login</span>
            </a>
          </Link>
        ) : (
          <span className="block w-48"></span>
        )}
      </div>
    </div>
  )
}

export default AuthHeader
