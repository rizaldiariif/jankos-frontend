import { NextPage } from 'next'
import Image from 'next/image'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import { api } from '../../helper/api'
import { pageContentFinder } from '../../helper/page-content-finder'
import IconFacebook from './IconFacebook'
import IconInitial from './IconInitial'
import IconInitialExtended from './IconInitialExtended'
import IconInstagram from './IconInstagram'
import IconLazada from './IconLazada'
import IconMain from './IconMain'
import IconShopee from './IconShopee'
import IconTokopedia from './IconTokopedia'

type Props = {
  page_contents: any[]
}

const Footer: NextPage<Props> = (props: Props) => {
  const { page_contents } = props

  const [certifications, setCertifications] = useState([])

  useEffect(() => {
    const fetchCertifications = async () => {
      const {
        data: { data },
      } = await api.get('/api/certifications')
      setCertifications(data)
    }
    fetchCertifications()
  }, [])

  return (
    <>
      <div
        className="relative h-auto w-screen max-w-full overflow-hidden bg-jankos-pink-100"
        id="hubungi-kami"
      >
        <div className="container relative z-10 mx-auto grid gap-16 py-16 lg:grid-cols-2">
          <div className="w-full">
            {/* <h4 className="mb-4 text-xl font-bold">
              {pageContentFinder(page_contents, 'footer_1_title')}
            </h4> */}
            {/* <Image src={'/images/logo-initial.png'} height={54} width={36} /> */}
            <IconInitialExtended />
            <span className="mb-4 block"></span>
            <div
              className="wysiwyig-container mb-4 text-sm md:w-1/2"
              dangerouslySetInnerHTML={{
                __html: pageContentFinder(page_contents, 'footer_1_content'),
              }}
            ></div>
            {/* <p className="">
            </p> */}
            <ul className="mb-8 flex items-center justify-start gap-2">
              {certifications.map((c: any) => (
                <li>
                  <Image
                    src={c.thumbnail}
                    height={40}
                    width={40}
                    quality={100}
                  />
                </li>
              ))}
            </ul>
            <p className="mb-3 text-sm">
              {pageContentFinder(page_contents, 'footer_1_social_media_title')}
            </p>
            <div className="flex items-center justify-start">
              <a
                href={pageContentFinder(page_contents, 'footer_instagram_link')}
                className="mr-4"
              >
                <svg
                  width="44"
                  height="44"
                  viewBox="0 0 44 44"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <circle cx="18" cy="18" r="18" fill="#F3F8EF" />
                  <g clipPath="url(#clip0_1799_3542)">
                    <path
                      d="M21.375 9H14.625C13.1332 9 11.7024 9.59263 10.6475 10.6475C9.59263 11.7024 9 13.1332 9 14.625L9 21.375C9 22.8668 9.59263 24.2976 10.6475 25.3525C11.7024 26.4074 13.1332 27 14.625 27H21.375C22.8668 27 24.2976 26.4074 25.3525 25.3525C26.4074 24.2976 27 22.8668 27 21.375V14.625C27 13.1332 26.4074 11.7024 25.3525 10.6475C24.2976 9.59263 22.8668 9 21.375 9V9ZM25.3125 21.375C25.3125 23.5463 23.5463 25.3125 21.375 25.3125H14.625C12.4538 25.3125 10.6875 23.5463 10.6875 21.375V14.625C10.6875 12.4538 12.4538 10.6875 14.625 10.6875H21.375C23.5463 10.6875 25.3125 12.4538 25.3125 14.625V21.375Z"
                      fill="#7B9A62"
                    />
                    <path
                      d="M18 13.5C16.8065 13.5 15.6619 13.9741 14.818 14.818C13.9741 15.6619 13.5 16.8065 13.5 18C13.5 19.1935 13.9741 20.3381 14.818 21.182C15.6619 22.0259 16.8065 22.5 18 22.5C19.1935 22.5 20.3381 22.0259 21.182 21.182C22.0259 20.3381 22.5 19.1935 22.5 18C22.5 16.8065 22.0259 15.6619 21.182 14.818C20.3381 13.9741 19.1935 13.5 18 13.5ZM18 20.8125C17.2544 20.8116 16.5395 20.515 16.0122 19.9878C15.485 19.4605 15.1884 18.7456 15.1875 18C15.1875 16.4486 16.4497 15.1875 18 15.1875C19.5503 15.1875 20.8125 16.4486 20.8125 18C20.8125 19.5503 19.5503 20.8125 18 20.8125Z"
                      fill="#7B9A62"
                    />
                    <path
                      d="M22.8379 13.7617C23.1691 13.7617 23.4375 13.4933 23.4375 13.1621C23.4375 12.831 23.1691 12.5625 22.8379 12.5625C22.5067 12.5625 22.2383 12.831 22.2383 13.1621C22.2383 13.4933 22.5067 13.7617 22.8379 13.7617Z"
                      fill="#7B9A62"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_1799_3542">
                      <rect
                        width="18"
                        height="18"
                        fill="white"
                        transform="translate(9 9)"
                      />
                    </clipPath>
                  </defs>
                </svg>
              </a>
              <a
                href={pageContentFinder(page_contents, 'footer_facebook_link')}
                className="mr-4"
              >
                <svg
                  width="44"
                  height="44"
                  viewBox="0 0 44 44"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <circle cx="18" cy="18" r="18" fill="#F3F8EF" />
                  <path
                    d="M20.4894 11.0977H22.2911V8.05021C21.4187 7.9595 20.5423 7.91472 19.6652 7.91604C17.0586 7.91604 15.2761 9.50688 15.2761 12.4202V14.931H12.334V18.3427H15.2761V27.0827H18.8027V18.3427H21.7352L22.1761 14.931H18.8027V12.7556C18.8027 11.7494 19.0711 11.0977 20.4894 11.0977Z"
                    fill="#7B9A62"
                  />
                </svg>
              </a>
              <a
                href={pageContentFinder(page_contents, 'footer_youtube_link')}
                className="mr-4"
              >
                <svg
                  width="44"
                  height="44"
                  viewBox="0 0 44 44"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <circle cx="18" cy="18" r="18" fill="#F3F8EF" />
                  <path
                    d="M26.081 13.7824C25.8865 13.0595 25.3166 12.4896 24.5937 12.2949C23.2732 11.9336 17.991 11.9336 17.991 11.9336C17.991 11.9336 12.709 11.9336 11.3885 12.2812C10.6796 12.4757 10.0957 13.0596 9.90122 13.7824C9.55371 15.1028 9.55371 17.8412 9.55371 17.8412C9.55371 17.8412 9.55371 20.5934 9.90122 21.9C10.0959 22.6228 10.6657 23.1927 11.3886 23.3874C12.7229 23.7488 17.9912 23.7488 17.9912 23.7488C17.9912 23.7488 23.2732 23.7488 24.5937 23.4012C25.3167 23.2066 25.8865 22.6367 26.0812 21.9139C26.4286 20.5934 26.4286 17.8551 26.4286 17.8551C26.4286 17.8551 26.4425 15.1028 26.081 13.7824ZM16.3093 20.371V15.3114L20.7017 17.8412L16.3093 20.371Z"
                    fill="#7B9A62"
                  />
                </svg>
              </a>
              <a
                href={pageContentFinder(page_contents, 'footer_tiktok_link')}
                className="mr-4"
              >
                <svg
                  width="44"
                  height="44"
                  viewBox="0 0 44 44"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <circle cx="18" cy="18" r="18" fill="#F3F8EF" />
                  <path
                    d="M25.9481 13.2637C24.9779 13.2637 24.0828 12.9423 23.3639 12.4001C22.5395 11.7785 21.9472 10.8667 21.738 9.81586C21.6862 9.55621 21.6583 9.28826 21.6556 9.01367H18.8842V16.5866L18.8808 20.7347C18.8808 21.8437 18.1587 22.784 17.1576 23.1147C16.8671 23.2107 16.5533 23.2562 16.2266 23.2382C15.8096 23.2153 15.4188 23.0895 15.0791 22.8863C14.3563 22.454 13.8662 21.6697 13.8529 20.7726C13.832 19.3704 14.9655 18.2272 16.3667 18.2272C16.6433 18.2272 16.9089 18.2724 17.1576 18.3544V16.2845V15.5404C16.8953 15.5016 16.6283 15.4813 16.3584 15.4813C14.8247 15.4813 13.3904 16.1188 12.3651 17.2673C11.5901 18.1352 11.1253 19.2426 11.0535 20.4037C10.9596 21.929 11.5177 23.379 12.6001 24.4488C12.7592 24.6059 12.9262 24.7516 13.1008 24.8861C14.0289 25.6003 15.1634 25.9874 16.3584 25.9874C16.6283 25.9874 16.8953 25.9675 17.1576 25.9287C18.2739 25.7633 19.3039 25.2523 20.1167 24.4488C21.1154 23.4617 21.6672 22.1512 21.6732 20.7563L21.6589 14.5619C22.1354 14.9295 22.6564 15.2336 23.2155 15.4697C24.0851 15.8366 25.0072 16.0225 25.9561 16.0222V14.0097V13.263C25.9568 13.2637 25.9488 13.2637 25.9481 13.2637Z"
                    fill="#7B9A62"
                  />
                </svg>
              </a>
            </div>
          </div>
          <div className="grid w-full grid-cols-2 lg:grid-cols-2">
            <div className="w-full">
              {/* <h4 className="mb-4 text-xl font-bold">
                {pageContentFinder(page_contents, 'footer_2_title')}
              </h4> */}
              <ul className="flex flex-col">
                <li className="mb-3">
                  <Link href="">
                    <a className="text-sm">Beranda</a>
                  </Link>
                </li>
                <li className="mb-3">
                  <Link href="/#produk">
                    <a className="text-sm">Produk</a>
                  </Link>
                </li>
                <li className="mb-3">
                  <Link href="/#tentang-kami">
                    <a className="text-sm">Tentang Kami</a>
                  </Link>
                </li>
                <li className="mb-3">
                  <Link href="/term-and-condition">
                    <a className="text-sm">Syarat dan Ketentuan</a>
                  </Link>
                </li>
                <li className="mb-3">
                  <Link href="/#hubungi-kami">
                    <a className="text-sm">Hubungi Kami</a>
                  </Link>
                </li>
              </ul>
            </div>
            <div className="w-full">
              <h4 className="mb-4 text-xl font-bold">
                {pageContentFinder(page_contents, 'footer_3_title')}
              </h4>
              <ul className="mb-4 flex flex-col">
                <li className="mb-4 flex items-center justify-start">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6"
                  >
                    <g clipPath="url(#clip0_1799_3524)">
                      <path
                        d="M17.4 22.0001C13.3173 21.9948 9.40331 20.3706 6.5164 17.4837C3.62949 14.5968 2.00529 10.6828 2 6.60011C2 5.38012 2.48464 4.21009 3.34731 3.34742C4.20998 2.48476 5.38 2.00011 6.6 2.00011C6.85834 1.99815 7.11625 2.02159 7.37 2.07011C7.61531 2.10641 7.85647 2.1667 8.09 2.25011C8.25425 2.30774 8.40061 2.40725 8.51461 2.53879C8.62861 2.67033 8.7063 2.82934 8.74 3.00011L10.11 9.00011C10.1469 9.16298 10.1425 9.33249 10.0971 9.4932C10.0516 9.6539 9.96671 9.80067 9.85 9.92011C9.72 10.0601 9.71 10.0701 8.48 10.7101C9.46499 12.871 11.1932 14.6063 13.35 15.6001C14 14.3601 14.01 14.3501 14.15 14.2201C14.2694 14.1034 14.4162 14.0185 14.5769 13.9731C14.7376 13.9276 14.9071 13.9232 15.07 13.9601L21.07 15.3301C21.2353 15.3685 21.3881 15.4483 21.5141 15.562C21.64 15.6757 21.735 15.8196 21.79 15.9801C21.8744 16.2175 21.938 16.4617 21.98 16.7101C22.0202 16.9614 22.0403 17.2156 22.04 17.4701C22.0216 18.6849 21.5233 19.8431 20.654 20.6918C19.7847 21.5405 18.6149 22.0108 17.4 22.0001Z"
                        fill="#7B9A62"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_1799_3524">
                        <rect width="24" height="24" fill="white" />
                      </clipPath>
                    </defs>
                  </svg>
                  <p className="textext-sm ml-2 mt-1">
                    {pageContentFinder(page_contents, 'footer_phone')}
                  </p>
                </li>
                <li className="mb-4 flex items-center justify-start">
                  <a
                    href={`mailto:${pageContentFinder(
                      page_contents,
                      'footer_email'
                    )}`}
                    className="flex items-center justify-start"
                  >
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 32 32"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6"
                    >
                      <path
                        d="M15.9997 14.81L28.7797 6.6C28.2659 6.21461 27.642 6.0043 26.9997 6H4.99973C4.35746 6.0043 3.73353 6.21461 3.21973 6.6L15.9997 14.81Z"
                        fill="#7B9A62"
                      />
                      <path
                        d="M16.54 16.8398L16.37 16.9198H16.29C16.1979 16.9609 16.1001 16.9879 16 16.9998C15.917 17.0102 15.833 17.0102 15.75 16.9998H15.67L15.5 16.9198L2.1 8.25977C2.03598 8.5013 2.00238 8.7499 2 8.99977V22.9998C2 23.7954 2.31607 24.5585 2.87868 25.1211C3.44129 25.6837 4.20435 25.9998 5 25.9998H27C27.7956 25.9998 28.5587 25.6837 29.1213 25.1211C29.6839 24.5585 30 23.7954 30 22.9998V8.99977C29.9976 8.7499 29.964 8.5013 29.9 8.25977L16.54 16.8398Z"
                        fill="#7B9A62"
                      />
                    </svg>
                    <p className="textext-sm ml-2 mt-1">
                      {pageContentFinder(page_contents, 'footer_email')}
                    </p>
                  </a>
                </li>
              </ul>
              <h4 className="mb-4 text-xl font-bold">
                {pageContentFinder(page_contents, 'footer_marketplace_title')}
              </h4>
              <ul className="flex items-center justify-start">
                <li className="mr-4">
                  <a
                    href={pageContentFinder(
                      page_contents,
                      'footer_tokopedia_link'
                    )}
                  >
                    <IconTokopedia />
                  </a>
                </li>
                <li className="mr-4">
                  <a
                    href={pageContentFinder(
                      page_contents,
                      'footer_shopee_link'
                    )}
                  >
                    <IconShopee />
                  </a>
                </li>
                <li className="mr-4">
                  <a
                    href={pageContentFinder(
                      page_contents,
                      'footer_lazada_link'
                    )}
                  >
                    <IconLazada />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="relative z-10 bg-jankos-pink-300 p-4 text-center text-xs text-white">
          {pageContentFinder(page_contents, 'footer_copyright')}
        </div>
        <span className="absolute top-0 right-8 h-64 w-64 -translate-y-1/2 transform rounded-full bg-jankos-green-200"></span>
        <span className="absolute bottom-0 left-1/2 h-96 w-96 translate-y-2/3 -translate-x-full transform rounded-full bg-jankos-pink-200"></span>
      </div>
      <a
        href={`https://wa.me/${pageContentFinder(
          page_contents,
          'footer_phone'
        )}`}
        className="floating-animation fixed bottom-8 right-8 z-30 flex h-12 w-12 items-center justify-center rounded-full bg-green-500"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-8 w-8 text-white"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
          strokeWidth={2}
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z"
          />
        </svg>
      </a>
    </>
  )
}

export default Footer
