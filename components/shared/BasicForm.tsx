import React from 'react'
import BasicInput, { InputItemOption } from './BasicInput'

export type InputItemProps = {
  field_name: string
  label: string
  placeholder?: string
  type?: string
  default_value?: string
  validation?: Object
  option?: boolean
  options?: InputItemOption[]
  wysiwyig?: boolean
  setValue?: any
  textarea?: any
}

type Props = {
  onSubmit: any
  input_fields: InputItemProps[]
  register: any
  errors: any
}

const BasicForm = (props: Props) => {
  const { onSubmit, input_fields, register, errors } = props

  return (
    <form onSubmit={onSubmit}>
      {input_fields.map((input) => (
        <BasicInput
          field_name={input.field_name}
          label={input.label}
          type={input.type}
          register={register}
          error={errors[input.field_name]}
          default_value={input.default_value}
          option={input.option}
          options={input.options}
          wysiwyig={input.wysiwyig}
          setValue={input.setValue}
          validation={input.validation}
          textarea={input.textarea}
          placeholder={input.placeholder}
        />
      ))}
      <input
        type="submit"
        value="Simpan"
        className="w-full cursor-pointer rounded-md border border-jankos-pink-300 bg-jankos-pink-300 py-3 px-20 text-sm font-semibold uppercase text-white lg:w-auto"
      />
    </form>
  )
}

export default BasicForm
