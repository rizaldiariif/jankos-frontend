import React from 'react'

const IconFacebookBlogShare = () => {
  return (
    <svg
      width="12"
      height="22"
      viewBox="0 0 12 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M11.209 4.23828V0.962891C11.209 0.962891 7.85742 0.933594 7.62891 0.933594C6.11133 0.933594 3.9375 2.65625 3.9375 4.61328V8.12304H0.796875V11.8437H3.89063V21.0781H7.55273V11.7969H10.793L11.2031 8.1582H7.59375C7.59375 8.1582 7.59375 5.92578 7.59375 5.48633C7.59375 4.84765 8.07422 4.21484 8.80078 4.21484C9.28711 4.2207 11.209 4.23828 11.209 4.23828Z"
        fill="black"
      />
    </svg>
  )
}

export default IconFacebookBlogShare
