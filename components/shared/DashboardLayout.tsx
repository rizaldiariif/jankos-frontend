import { removeCookies } from 'cookies-next'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { api } from '../../helper/api'
import { useCart } from '../../helper/useCart'
import { useCurrentMember } from '../../helper/useCurrentMember'
import LoadingScreen from './LoadingScreen'

type Props = {}

const DashboardLayout: React.FC<Props> = ({ children }) => {
  const { member } = useCurrentMember()
  const { cart } = useCart()
  const { pathname } = useRouter()
  const [phone_number, setPhoneNumber] = useState('')

  useEffect(() => {
    const fetchPhoneNumber = async () => {
      const {
        data: { data },
      } = await api.get('/api/page-contents?key.eq=footer_phone')
      if (data.length > 0) {
        setPhoneNumber(data[0].content)
      }
    }
    fetchPhoneNumber()
  }, [])

  if (!member) {
    return <LoadingScreen />
  }

  const onSignOut = () => {
    if (!process.env.NEXT_PUBLIC_APP_COOKIE_NAME) {
      return
    }

    if (window.confirm('Are you sure?')) {
      removeCookies(process.env.NEXT_PUBLIC_APP_COOKIE_NAME)
      window.location.href = '/auth/signin'
    }
  }

  return (
    <div className="relative grid h-screen w-screen overflow-x-hidden lg:grid-cols-6">
      <div className="hidden h-full w-full bg-jankos-pink-300 p-6 lg:block">
        <img
          src="/images/shared/logo-white.png"
          alt=""
          className="mb-8 h-auto w-full"
        />
        <ul>
          <li className="mb-6 text-sm">
            <Link href="/dashboard/overview">
              <a
                className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                  pathname.split('/')[2] == 'overview'
                    ? 'font-semibold opacity-100'
                    : 'opacity-75'
                }`}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="mr-4 h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M16 8v8m-4-5v5m-4-2v2m-2 4h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"
                  />
                </svg>
                Dashboard
              </a>
            </Link>
          </li>
          <li className="mb-6 text-sm">
            <Link href="/dashboard/order">
              <a
                className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                  pathname.split('/')[2] == 'order'
                    ? 'font-semibold opacity-100'
                    : 'opacity-75'
                }`}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="mr-4 h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
                  />
                </svg>
                Daftar Pesanan
              </a>
            </Link>
          </li>
          <li className="mb-6 text-sm">
            <Link href="/dashboard/product">
              <a
                className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                  pathname.split('/')[2] == 'product'
                    ? 'font-semibold opacity-100'
                    : 'opacity-75'
                }`}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="mr-4 h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10"
                  />
                </svg>
                Produk Jankos
              </a>
            </Link>
          </li>
          <li className="mb-6 text-sm">
            <Link href="/dashboard/promotion-material">
              <a
                className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                  pathname.split('/')[2] == 'promotion-material'
                    ? 'font-semibold opacity-100'
                    : 'opacity-75'
                }`}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="mr-4 h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                </svg>{' '}
                Bahan Marketing
              </a>
            </Link>
          </li>
          {/* <li className="mb-6 text-sm">
            <Link href="/dashboard/point">
              <a
                className={`flex items-center text-white transition hover:font-semibold hover:opacity-100 ${
                  pathname.split('/')[2] == 'point'
                    ? 'font-semibold opacity-100'
                    : 'opacity-75'
                }`}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="mr-4 h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M12 8v13m0-13V6a2 2 0 112 2h-2zm0 0V5.5A2.5 2.5 0 109.5 8H12zm-7 4h14M5 12a2 2 0 110-4h14a2 2 0 110 4M5 12v7a2 2 0 002 2h10a2 2 0 002-2v-7"
                  />
                </svg>{' '}
                Poin
              </a>
            </Link>
          </li> */}
          {/* <li className="mb-6 text-sm">
            <Link href="/dashboard/commission">
              <a className="flex items-center text-white opacity-75 transition hover:font-semibold hover:opacity-100">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="mr-4 h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z"
                  />
                </svg>{' '}
                Komisi
              </a>
            </Link>
          </li>
          <li className="mb-6 text-sm">
            <Link href="/dashboard/referral">
              <a className="flex items-center text-white opacity-75 transition hover:font-semibold hover:opacity-100">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="mr-4 h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z"
                  />
                </svg>
                Referral
              </a>
            </Link>
          </li> */}
        </ul>
      </div>
      <div className="flex flex-col overflow-hidden lg:col-span-5">
        <div className="flex w-full items-center justify-between bg-jankos-pink-100 py-5 px-9">
          <div className="">
            <h3 className="mb-1 text-xl font-bold">Hello {member.name}!</h3>
            <p>{member.member_level.name}</p>
          </div>
          <div className="flex items-center">
            <Link href="/dashboard/cart">
              <a className="relative">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="mr-4 h-5 w-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
                  />
                </svg>
                <span className="absolute top-0 right-0 -translate-y-1/2 transform rounded-full bg-jankos-pink-300 px-2 pt-1 text-xs font-semibold text-white">
                  {cart.length}
                </span>
              </a>
            </Link>
            <div className="group relative cursor-pointer">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth={2}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                />
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                />
              </svg>{' '}
              <ul className="absolute bottom-0 right-0 z-10 hidden translate-y-full transform flex-col text-center shadow-md group-hover:flex">
                <li className="border bg-white py-2 px-4">
                  <Link href="/dashboard/setting">
                    <a className="whitespace-nowrap text-sm">Ubah Profil</a>
                  </Link>
                </li>
                <li className="border bg-white py-2 px-4">
                  <Link href="/dashboard/change-password">
                    <a className="whitespace-nowrap text-sm">Ubah Kata Sandi</a>
                  </Link>
                </li>
                <li className="border bg-white py-2 px-4">
                  <Link href="/dashboard/support">
                    <a className="whitespace-nowrap text-sm">Bantuan</a>
                  </Link>
                </li>
                <button
                  className="whitespace-nowrap border bg-white py-2 px-4 text-sm"
                  onClick={onSignOut}
                >
                  Keluar
                </button>
              </ul>
            </div>
            {/* <button onClick={onSignOut}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth={2}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"
                />
              </svg>{' '}
            </button> */}
          </div>
        </div>
        <div className="relative w-full flex-grow overflow-hidden overflow-y-scroll bg-white px-12 py-6">
          {children}
        </div>
      </div>
      <a
        href={`https://wa.me/${phone_number}`}
        className="floating-animation absolute bottom-8 right-8 flex h-12 w-12 items-center justify-center rounded-full bg-green-500"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-8 w-8 text-white"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
          strokeWidth={2}
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z"
          />
        </svg>
      </a>
    </div>
  )
}

export default DashboardLayout
