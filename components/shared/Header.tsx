import { NextPage } from 'next'
import React from 'react'
import Link from 'next/link'
import { pageContentFinder } from '../../helper/page-content-finder'
import IconMain from './IconMain'

type Props = {
  page_contents: any[]
}

const Header: NextPage<Props> = (props: Props) => {
  const { page_contents } = props

  return (
    <div className="relative hidden w-screen max-w-full bg-white lg:block">
      <div className="container mx-auto flex items-center justify-between py-4">
        <Link href="/">
          <a>
            <IconMain />
            {/* <img
              className="h-6 w-auto"
              src={pageContentFinder(page_contents, 'header_logo')}
              alt=""
            /> */}
          </a>
        </Link>
        <Link href="/#tentang-kami">
          <a className="hidden text-base font-semibold lg:block">
            Tentang Kami
          </a>
        </Link>
        <Link href="/#produk">
          <a className="hidden text-base font-semibold lg:block">Produk</a>
        </Link>
        <Link href="/blog">
          <a className="hidden text-base font-semibold lg:block">Blog</a>
        </Link>
        <Link href="#hubungi-kami">
          <a className="hidden text-base font-semibold lg:block">
            Hubungi Kami
          </a>
        </Link>
        <div className="flex items-center justify-center">
          <Link href="/auth/signin">
            <a className="rounded-full border border-jankos-pink-300 bg-transparent py-2 px-6 text-base font-semibold text-jankos-pink-300 transition-colors hover:bg-jankos-pink-300 hover:text-white">
              Login
            </a>
          </Link>
          <Link href="/auth/signup">
            <a className="ml-4 hidden rounded-full border border-jankos-pink-300 bg-jankos-pink-300 py-2 px-6 text-base font-semibold text-white transition-colors hover:bg-transparent hover:text-jankos-pink-300 lg:block">
              Register
            </a>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default Header
