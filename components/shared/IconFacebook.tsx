import React from 'react'

type Props = {}

const IconFacebook = (props: Props) => {
  return (
    <svg
      width="23"
      height="23"
      viewBox="0 0 23 23"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M14.4894 5.0982H16.2911V2.0507C15.4187 1.95999 14.5423 1.91521 13.6652 1.91653C11.0586 1.91653 9.27607 3.50737 9.27607 6.4207V8.93153H6.33398V12.3432H9.27607V21.0832H12.8027V12.3432H15.7352L16.1761 8.93153H12.8027V6.75612C12.8027 5.74987 13.0711 5.0982 14.4894 5.0982Z"
        fill="white"
      />
    </svg>
  )
}

export default IconFacebook
